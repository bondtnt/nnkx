# NNKX - Neural Networks Knowledge eXtraction #

Current repository contains code required for doing knowledge extraction from trained artificial neural networks. Specifically feed forward multi-layered (fully connected) error back-propagation networks. This library is built on top of Torch7 nn module. It ads several layers along with utility functions to perform knowledge extraction. Knowledge is extracted in the form of crisp decision tree. Please refer to src/test.lua file for specifics.


### Binary Classification Decision Tree extraction ###

* Version 0.1 quick summary: Rules being extracted are binary classification decision tree. No other rules types are currently supported. If you want more info about topic google for: Knowledge extraction from neural network.