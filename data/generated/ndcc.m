function [M d] = ndcc(m, n, exp);
%
% function [M d] = ndcc(m, n, exp);
%
% NORMALLY DISTRIBUTED CLUSTERS ON CUBES data generator.
% Generate centers for multivariate normal
% distributions based on the vertices of the 1-norm cube, 1-norm value for
% each class.  Generate points for the distributions around each center.
% By default, the distributions use different covariances and numbers of
% points. These can be changed using the flags sameCov and sameFrac.
% 
% ndcc returns:
% M: m-by-n matrix of points in R^n
% d: m-by-1 vector of +1 and -1 representing the class of each point
%
% exp is the expansion which determines how much to stretch the covariance
% matrix.  The default value is 250.
%
% All values are taken as integers for simplicity.
%
% Copyright (C) 2006 Michael E. Thompson and Olvi L. Mangasarian.
% Version 1.0
%
% based on Normally Distributed Clustered Datasets, 
% Copyright (C) 2000 David R. Musicant and Olvi L. Mangasarian.
%
% This software is free for academic and research use only.
% For commercial use, contact thompson@cs.wisc.edu.

rand('state',91225);
randn('state',19481);

if nargin == 2
    exp = 250;
end

%location of the centers
class1Center = 100;
class2Center = 300;
class3Center = 500;


% Whether each center uses the same covariance and fraction of points.
% 1 for yes, else no
sameCov = 0;
sameFrac = 0;

nCols = n;
nRows = m;

nBufferPoints = 100000;
nExpandFactor = exp;    % How much to stretch the covariance matrix


nCenters = 6*nCols;
classCenters = [class1Center class2Center class3Center];

% Generate the centers based on the 1-norm cube
mCenters = sparse(nCenters,nCols);
for i = 1:3
    for j = 1:nCols
        mCenters((i-1)*2*nCols + 2*j-1,:) = [zeros(1,j-1) classCenters(i) zeros(1,nCols-j)];
        mCenters((i-1)*2*nCols + 2*j,:) = [zeros(1,j-1) -classCenters(i) zeros(1,nCols-j)];
    end
end

if sameCov ~= 1

    % Generate the variances and covariances randomly to create a matrix for
    % each center
    mCovariance = zeros(nCols,nCols);
    cCovariance = cell(nCenters,1);
    for i = 1:nCenters,
      mRootCovariance = nExpandFactor *  ...
          rand(nCols,nCols); %simplified
      cCovariance{i} = mRootCovariance' * mRootCovariance;
    end;

else

    %generate random covariance once
    covar = rand(nCols, nCols);

    mCovariance = zeros(nCols,nCols);
    cCovariance = cell(nCenters,1);
    for i = 1:nCenters,
      mRootCovariance = nExpandFactor *  ...
          covar; %simplified
      cCovariance{i} = mRootCovariance' * mRootCovariance;
    end;
end


if sameFrac ~=1

    % Determine what proportion of points will come from each center, then
    % create a cdf to use in deciding which to generate.
    vPointFraction = rand(nCenters,1);
    vPointFraction = vPointFraction / sum(vPointFraction);
    vPointCdf = zeros(1,nCenters);
    for i = 1:nCenters,
      vPointCdf(i) = sum(vPointFraction(1:i));
    end;
    
    %adjust vPointCdf to reduce points at first center and move to second
    for i = 1:ceil(n/2)
        iChange = floor(6*(i-1) + ceil(5*rand));
        twoPoints = vPointCdf(iChange) + vPointCdf(iChange+1);
        vPointCdf(iChange) = .01*twoPoints;
        vPointCdf(iChange+1) = .99*twoPoints;
    end
    
else

    %have same amount of points with each center
    for i = 1:nCenters
        vPointCdf(i) = i/nCenters;
    end

end    
    
%assign class to centers
vCenterClasses(1:2*nCols) = 1;
vCenterClasses(2*nCols+1:4*nCols) = -1;
vCenterClasses(4*nCols+1:6*nCols) = 1;

vCenterClasses(1) = 1;
currentClass = -1;
for i = 2:2:(2*nCols-1)
    vCenterClasses(i:i+1) = currentClass;
    currentClass = -currentClass;
end
vCenterClasses(2*nCols) = currentClass;

%alternate class of outer points with inner
vCenterClasses((2*nCols+1):4*nCols) = -vCenterClasses(1:2*nCols);
vCenterClasses((4*nCols+1):6*nCols) = vCenterClasses(1:2*nCols);

% Now go through and begin generating random points.
for nDataset = 1:1, 

  M = [];
  
  nRowsLeft = nRows;
  nTotRows = nRows;
  
  nMisclass = 0;
  nTrainingClass1 = 0;
  nTrainingClassm1 = 0;

  while (nRowsLeft > 0)
    disp(sprintf('Rows left = %d',nRowsLeft));
    nRowsNow = min(nBufferPoints,nRowsLeft);
    nRowsLeft = nRowsLeft - nRowsNow;
    mNewPoints = zeros(nRowsNow,nCols);
    vPointCenters = zeros(nRowsNow,1);
    
    % Determine which center each point should belong to
    vRandomNumbers = rand(nRowsNow,1);
    for i = nCenters:-1:1,
      vCenterMatch = (vRandomNumbers <= vPointCdf(i));
      vPointCenters([vCenterMatch]) = i;
    end;
    
    % Create a vector of training classes for each point
    vTrainingClasses = zeros(nRowsNow,1);
    
    % Within each class, generate an appropriate number of random points.
    for i = 1:nCenters,
      vIndices = (vPointCenters==i);
      nPoints = sum(vIndices);
      vTrainingClasses(vIndices) = vCenterClasses(i);
      mNewPoints(vIndices,:) = round( ...
	  mvnrnd(mCenters(i,:),cCovariance{i},nPoints));
      
    end;    %for
    
    % Output the data points
    M = [M; mNewPoints vTrainingClasses];
    
  end;  %while

  d = M(:,n+1);
  M = M(:,1:n);
    
  nTrainingClass1 = size(find(d==1),1);
  nTrainingClassm1 = size(find(d==-1),1);
  
  disp(sprintf('Number class  1 points = %d\n',nTrainingClass1));
  disp(sprintf('Number class -1 points = %d\n',nTrainingClassm1));

end; %for-nDataset
