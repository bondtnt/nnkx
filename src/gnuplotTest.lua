require 'torch'
require 'gnuplot'

x = torch.linspace(-1,1)
xx = torch.Tensor(x:size(1),x:size(1)):zero():addr(1,x,x)
xx = xx*math.pi*6
 
local x1 = torch.randn(40):mul(100)
local y1 = torch.randn(40):mul(100)
local x2 = torch.randn(40):mul(100)
local y2 = torch.randn(40):mul(100)
local x3 = torch.randn(40):mul(100)
local y3 = torch.randn(40):mul(100)
 
gnuplot.figure(1)
 
gnuplot.xlabel('Time ms()')
gnuplot.ylabel('')
gnuplot.title('Gnuplot example')
--
--gnuplot.imagesc(torch.sin(xx),'color') 
 
gnuplot.plot(
   {'Group1' , x1, y1,'+'},
   {'Group2' , x2, y2,'+'},
   {'Group3' , x3, y3,'+'}
)


----labels_tab = {}
----for i=1,20 do
----  table.insert(labels_tab, math.ceil(i/10))
----end
----x = torch.cat(torch.Tensor(10):normal(2, 0.05), torch.Tensor(10):normal(3, 0.05), 1)
----y = torch.cat(torch.Tensor(10):normal(2, 0.05), torch.Tensor(10):normal(3, 0.05), 1)
----
--LondonTemp = torch.Tensor{{9, 10, 12, 15, 18, 21, 23, 23, 20, 16, 12, 9},
--                          {5,  5,  6,  7, 10, 13, 15, 15, 13, 10,  7, 5}}
--gnuplot.plot({'High [°C]',LondonTemp[1]},{'Low [°C]',LondonTemp[2]})
----gnuplot.raw('set xtics ("Jan" 1, "Feb" 2, "Mar" 3, "Apr" 4, "May" 5, "Jun" 6, "Jul" 7, "Aug" 8, "Sep" 9, "Oct" 10, "Nov" 11, "Dec" 12)')
----gnuplot.plotflush()
--gnuplot.imagesc(torch.sin(xx),'color')
----gnuplot.axis{0,13,0,''}
----gnuplot.grid(true)
--gnuplot.title('London average temperature')
--gnuplot.plotflush()