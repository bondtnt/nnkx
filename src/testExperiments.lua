require('mobdebug').start()
plPretty = require('pl.pretty')

torch = require('torch')
optim = require('optim')

nn = require('nn')
csvUtil = require('csvUtil')
dataUtil = require('dataUtil')
tableUtil = require('tableUtil')
tensorUtil = require('tensorUtil')
activationModifierUtil = require('activationModifierUtil')
runningUtil = require('runningUtil')
typeUtil = require('typeUtil')
nngraph = require('nngraph')
mathUtil = require('mathUtil')

require 'NeuronPruning'
require 'ActivationRecorder'
require 'ActivationModifier'
require 'SensitivityPruning'
require 'RulesGenerator'
require 'ActivationsDiscretizer'

require 'graph'

torch.manualSeed(1)

local precision = 1e-5
local expprecision = 1e-4


local filePath = '/home/andrey/works/LuaProjects/nnkx/data/haberman.csv'
local data = csvUtils.loadCsvData(filePath, false, ',')
--local data = csvUtil.loadTest2dData()
local X = data[{ {}, {1,-2} }]
X = tensorUtil.scaleTensor(X, -1, 1) -- we will be using tanh function
--X = tensorUtil.scaleTensor(X, 0, 1) -- we will be using sigm function
local labels = data[{ {}, {-1} }]
local targets, classesNumber, labels2Targets = dataUtil.labelsColumnToMatrix(labels, -1, 1) -- (-1 + 1) for tanh, (0, +1) for sigmoid
local dataset = dataUtil.convertToDataset(X, targets)

local classNames = {}
for i = 1, classesNumber do
  classNames[i] = "Class"..i  
end

-- ANN parameters
local inputsSize = dataset[1][1]:size(1) 
local outputsSize = targets:size(2)
local hidden1Size = 15
local hidden2Size = 15 

--local mlp = nn.Sequential()
--mlp:add( nn.Linear(inputsSize, hidden1Size) ) -- 10 input, 20 hidden units
--mlp:add( nn.Tanh() )
----mlp:add( nn.Sigmoid() )
----mlp:add( nn.NeuronPruningLayer('tanh', hidden1Size) ) -- Input layer pruning
----mlp:add( nn.NeuronPruningLayer('sigmoid', hidden1Size) ) -- Input layer pruning
--mlp:add( nn.Linear(hidden1Size, hidden2Size) ) -- 10 input, 20 hidden units
--mlp:add( nn.Tanh() )
----mlp:add( nn.Sigmoid() )
----mlp:add( nn.NeuronPruningLayer('tanh', hidden2Size) ) -- Hidden layer pruning
----mlp:add( nn.NeuronPruningLayer('sigmoid', hidden2Size) ) -- Input layer pruning
--mlp:add( nn.Linear(hidden2Size, outputsSize) ) -- 1 output
-- 
local mlp = nn.Sequential()
mlp:add( nn.NeuronPruningLayer('tanh', inputsSize) )         -- Input layer pruning
mlp:add( nn.ActivationModifier() )          -- Used to perform activations discretization/clusterization
mlp:add( nn.ActivationRecorder(false) )     -- Used to record activation values
mlp:add( nn.Linear(inputsSize, hidden1Size) ) -- 10 input, 20 hidden units
mlp:add( nn.Tanh() )
mlp:add( nn.NeuronPruningLayer('tanh', hidden1Size) )         -- Input layer pruning
mlp:add( nn.ActivationModifier() )          -- Used to perform activations discretization/clusterization
mlp:add( nn.ActivationRecorder(false) )     -- Used to record activation values
mlp:add( nn.Linear(hidden1Size, outputsSize) ) -- 1 output
--mlp:add( nn.SoftMax() )
mlp:add( nn.Tanh() )
mlp:add( nn.ActivationModifier() )          -- Used to perform activations discretization/clusterization
mlp:add( nn.ActivationRecorder(false) )     -- Used to record activation values

local criterion = nn.MSECriterion()  
--local criterion = nn.CrossEntropyCriterion()
local trainer = nn.StochasticGradient(mlp, criterion)
trainer.learningRate = .05
trainer.learningRateDecay = 0.0025
trainer.maxIteration = 500
trainer.verbose = false

local verbose = true 
local saveModels = false
local i = 1
local results = {}

-- log results to files
--  trainLogger = optim.Logger(paths.concat(opt.save, 'train.log'))
--  testLogger = optim.Logger(paths.concat(opt.save, 'test.log'))

local trainLogger = optim.Logger(paths.concat('logs', 'train.log'))

local xValTrainer = {
  criterion = criterion, 
  classNames = classNames, 
  mlp = mlp, 
  trainer = trainer, 
  saveModels = false, -- if true all trained models will be saved 
  verbose = false     -- will print accuracy if true
}

local pruningConfig = {}
pruningConfig.afterPruningRetrainEpochs = 25
pruningConfig.maxFallbacks = 5
pruningConfig.errorWorsenForFallback = 0.05
pruningConfig.numberOfNeuronsToPrune = 1 -- Number of neurons to prune at single step  
pruningConfig.maxNodesToPrune = hidden1Size + inputsSize
pruningConfig.maxPruningIterations = 20 --20
pruningConfig.classNames = classNames
pruningConfig.retrainIterations = 5 --10

local xvalIteration = 1
local xValLogs = {}
xValLogs.trainConfusion = {}
xValLogs.testConfusion = {}
xValLogs.pruningTrainConfusion = {}
xValLogs.pruiningTestConfusion = {}


xValTrainer.trainTest =
  function(thiz, _mlp, _trainer, _classNames, _trainData, _testData, _labels2Targets)
    -- train network
    _trainer:train(_trainData)
    
    -- save stats
    local trainConf = runningUtil.calculateConfusion2(_mlp, _trainData, classNames, labels2Targets, nil)
    local testConf = runningUtil.calculateConfusion2(_mlp, _testData, classNames, labels2Targets, nil)
    trainConf:updateValids()
    testConf:updateValids()
    xValLogs.trainConfusion[xvalIteration] = trainConf
    xValLogs.testConfusion[xvalIteration] = testConf
    
    -- prune network
    local sensitivityPruning = nn.SensitivityPruning()
    local pruningLog = sensitivityPruning:runNeuronsPruning(pruningConfig, _trainData, _mlp, criterion, _trainer, labels2Targets)
    local confusionTrainAfterPruning = runningUtil.calculateConfusion2(_mlp, _trainData, classNames, labels2Targets, nil)
    
    -- save stats    
    local pruningTrainConf = runningUtil.calculateConfusion2(_mlp, _trainData, classNames, labels2Targets, nil)
    local pruningTestConf = runningUtil.calculateConfusion2(_mlp, _testData, classNames, labels2Targets, nil)
    pruningTrainConf:updateValids()
    pruningTestConf:updateValids()
    xValLogs.pruningTrainConfusion[xvalIteration] = pruningTrainConf
    xValLogs.pruiningTestConfusion[xvalIteration] = pruningTestConf
    
    xvalIteration = xvalIteration + 1
    
    trainLogger:add{['% mean class accuracy (train set)'] = trainConf.totalValid * 100}
    if true then
      trainLogger:style{['% mean class accuracy (train set)'] = '-'}
      trainLogger:plot()
    end
  end 

runningUtil.runXVal(dataset, labels, xValTrainer, labels2Targets, 10, 3)

local trainConfTensor = tensorUtil.buildTensorFromConfusionsTable(xValLogs.trainConfusion, function(confM) return confM.totalValid end)
local testConfTensor = tensorUtil.buildTensorFromConfusionsTable(xValLogs.testConfusion, function(confM) return confM.totalValid end)
local pruningTrainConfTensor = tensorUtil.buildTensorFromConfusionsTable(xValLogs.pruningTrainConfusion, function(confM) return confM.totalValid end)
local pruningTestConfTensor = tensorUtil.buildTensorFromConfusionsTable(xValLogs.pruiningTestConfusion, function(confM) return confM.totalValid end)

local function printTensor(t, str) 
  print(str..' mean='..t:mean()..'; std='..t:std()..';')
end


printTensor(trainConfTensor, 'train')
printTensor(trainConfTensor, 'test')
printTensor(trainConfTensor, 'train after pruning')
printTensor(trainConfTensor, 'test after pruning' )
--print(plPretty.write(results, ''))