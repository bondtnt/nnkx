-- DecisionTreeNodeData is a class which holds attribute index (columnNumber) 
-- and splitValue for that attribute

torch = require('torch')
tensorUtil = require('tensorUtil')
require('DecisionTreeStats')

local DecisionTreeLeaf = torch.class('DecisionTreeLeaf')

function DecisionTreeLeaf:__init(classTensor)
  self.classTensor = classTensor
end

function DecisionTreeLeaf:equals(otherDecisionTreeLeafData)
  return tensorUtil.tensorsEqual(self.classTensor, otherDecisionTreeLeafData.classTensor)
end

function DecisionTreeLeaf:isLeaf()
  return true
end

function DecisionTreeLeaf:treeStats()
  return DecisionTreeStats(1, 1)
end

function DecisionTreeLeaf:asString()
  if (string.find(torch.type(self.classTensor), "Tensor") ~= nil) then
    local t = self.classTensor:clone():squeeze()
    if (string.find(torch.type(t), "Tensor") ~= nil) then
      local tensorStr = "["
      for i=1,t:size(1) do
        tensorStr = tensorStr .. t[i]
      end
      tensorStr = tensorStr .. ']' 
      return '(leaf class='..tensorStr..')'
    else
      -- if we had 1D single valued tensor after squeeze it will be a number
      return '(leaf class='..t..')'
    end
  else 
    return '(leaf class='..self.classTensor..')'
  end
end

function DecisionTreeLeaf:print()
  print(self:asString())
end
