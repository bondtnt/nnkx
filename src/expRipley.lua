-- script for running rules extraction experiment on IRIS dataset
require('mobdebug').start()

csv = require "csv"

require 'graph'
require 'pl'
require 'paths'

plPretty = require('pl.pretty')
torch = require('torch')
optim = require('optim')
nn = require('nn')
csvUtil = require('csvUtil')
dataUtil = require('dataUtil')
tableUtil = require('tableUtil')
tensorUtil = require('tensorUtil')
activationModifierUtil = require('activationModifierUtil')
runningUtil = require('runningUtil')
typeUtil = require('typeUtil')
nngraph = require('nngraph')
mathUtil = require('mathUtil')

require 'NeuronPruning'
require 'NeuronPruningLayer'
require 'ActivationRecorder'
require 'ActivationModifier'
require 'SensitivityPruningLogic'
require 'N2PSPruningLogic'
require 'N2PSSensitivityLayeredPruningLogic'
require 'RulesGenerator'
require 'ActivationsDiscretizer'

torch.manualSeed(1)
local timeExec = sys.clock()

local opt = lapp[[
   -b, --batchSize     (default 10)          batch size
   -o, --optimization  (default "SGD")       optimization: SGD | LBFGS 
   -r, --learningRate  (default 0.05)        learning rate, for SGD only
   -m, --momentum      (default 0)           momentum, for SGD only
   -e, --maxEpochs     (default 10)          maximum nb of epochs per single run (fold)
   -i, --maxIter       (default 3)           maximum nb of iterations per batch, for LBFGS
   --coefL1            (default 0)           L1 penalty on the weights
   --coefL2            (default 0)           L2 penalty on the weights
   -t, --threads       (default 4)           number of threads
   --tZero             (default 1)           start averaging at t0 (ASGD only), in nb of epochs
]]

opt.batchSize = 10
opt.optimization = 'SGD'
opt.learningRate = 0.001
opt.learningRateDecay = 0.0
opt.maxEpochs = 1000
opt.threads = 4
opt.weightDecay = 0.00
opt.momentum = 0.0

-- fix seed
torch.manualSeed(1)

-- threads
torch.setnumthreads(opt.threads)
print('<torch> set nb of threads to ' .. torch.getnumthreads())

-- use floats, for SGD
if opt.optimization == 'SGD' then
  torch.setdefaulttensortype('torch.FloatTensor')
end

-- batch size?
if opt.optimization == 'LBFGS' and opt.batchSize < 100 then
  error('LBFGS should not be used with small mini-batches; 1000 is recommended')
end

--require('profiler')
--profiler = newProfiler()
--profiler:start()

local time = os.date("*t")
local timestamp = ("%02d%02d%02d_%02d-%02d-%02d"):format(time.year, time.month, time.day, time.hour, time.min, time.sec)
local dataSetName = 'ripley-synth-batched' --used for log files naming
print(dataSetName)

local classesNumber = 2
local labels2TargetsTe = {torch.FloatTensor({1,-1}), torch.FloatTensor({-1,1})}
local labels2TargetsTr = {torch.FloatTensor({1,-1}), torch.FloatTensor({-1,1})}

datasetTrain={}
datasetTest={}
sizeTrain=0
sizeTest=0
originalMin = math.huge
originalMax = -1 * math.huge
newMin = -1 
newMax = 1

local f = csv.open("data/ripley-synth-train_my.csv")
for cols in f:lines() do
  local input = torch.Tensor(2)
  local output=torch.Tensor(2)
  for i,v in ipairs(cols) do
    if i>=1 and i<=2 then
      input[i] = v
    end
    if i==3 then
      for j=1,2 do
        output[j] = j==(v+1) and 1 or 0
      end
    end
  end
  output = (output * 2 ) -1 -- targets should be {-1, 1} - for Tanh function
  sizeTrain=sizeTrain+1
  datasetTrain[sizeTrain]={input,output}
  originalMin = math.min(originalMin, torch.min(input))
  originalMax = math.max(originalMax, torch.max(input))
end

function datasetTrain:size() return sizeTrain end


local f = csv.open("data/ripley-synth-test_my.csv")
for cols in f:lines() do
  local input = torch.Tensor(2)
  local output=torch.Tensor(2)
  for i,v in ipairs(cols) do
    if i>=1 and i<=2 then
      input[i] = v
    end
    if i==3 then
      for j=1,2 do
        output[j] = j==(v+1) and 1 or 0
      end
    end
  end
  output = (output * 2 ) -1 -- targets should be {-1, 1} - for Tanh function
  sizeTest=sizeTest+1
  datasetTest[sizeTest]={input,output}
  originalMin = math.min(originalMin, torch.min(input))
  originalMax = math.max(originalMax, torch.max(input))
end

function datasetTest:size() return sizeTest end

local dataTestSetTe = datasetTest
local dataTestSetTr = datasetTrain

classNames = {}
for i = 1, classesNumber do
  classNames[i] = ''..i
end

-- ANN parameters
local inputsSize = dataTestSetTe[1][1]:size(1)
local outputsSize = classesNumber -- For ClassNNLCriterion - there must be classesNumber output neurons.
local hidden1Size = 10
--local hidden2Size = 5

local mlp = nn.Sequential()
mlp:add( nn.NeuronPruningLayer('tanh', inputsSize) )         -- Input layer pruning
mlp:add( nn.ActivationModifier() )          -- Used to perform activations discretization/clusterization
mlp:add( nn.ActivationRecorder(false) )     -- Used to record activation values
mlp:add( nn.Linear(inputsSize, hidden1Size) ) -- 10 input, 20 hidden units
mlp:add( nn.Tanh() )

--mlp:add( nn.NeuronPruningLayer('sigmoid', hidden1Size) )         -- Input layer pruning
--mlp:add( nn.ActivationModifier() )          -- Used to perform activations discretization/clusterization
--mlp:add( nn.ActivationRecorder(false) )     -- Used to record activation values
--mlp:add( nn.Linear(hidden1Size, hidden2Size) ) -- 10 input, 20 hidden units
--mlp:add( nn.Sigmoid() )

mlp:add( nn.NeuronPruningLayer('tanh', hidden1Size) )         -- Input layer pruning
mlp:add( nn.ActivationModifier() )          -- Used to perform activations discretization/clusterization
mlp:add( nn.ActivationRecorder(false) )     -- Used to record activation values
mlp:add( nn.Linear(hidden1Size, outputsSize) ) -- 1 output

--=====================================================
-- retrieve parameters and gradients
--parameters, gradParameters = mlp:getParameters()

----------------------------------------------------------------------
-- loss function: negative log-likelihood
--

mlp:add( nn.LogSoftMax())
mlp:add( nn.ActivationModifier() )          -- Used to perform activations discretization/clusterization
mlp:add( nn.ActivationRecorder(false) )     -- Used to record activation values

-- verbose
print('<> using model:')
print(mlp)

--=====================================================

local criterion = nn.MSECriterion()
--local criterion = nn.CrossEntropyCriterion()
--local criterion = nn.BCECriterion()
--local criterion = nn.MarginCriterion()
--local criterion = nn.SoftMarginCriterion()
--local criterion = nn.ClassNLLCriterion()

local verbose = true
local saveModels = false
local i = 1
local results = {}

-- log results to files
local trainLogger = optim.Logger(paths.concat('logs', dataSetName..'_'..timestamp..'train.log'))
local testLogger = optim.Logger(paths.concat('logs', dataSetName..'_'..timestamp..'test.log'))
local prunningTrainLogger = optim.Logger(paths.concat('logs', dataSetName..'_'..timestamp..'prunedTrain.log'))
local prunningTestLogger = optim.Logger(paths.concat('logs', dataSetName..'_'..timestamp..'prunedTest.log'))
local prunedNeuronsCount = optim.Logger(paths.concat('logs', dataSetName..'_'..timestamp..'prunedNeuronsCount.log'))
local rulesTrainLogger = optim.Logger(paths.concat('logs', dataSetName..'_'..timestamp..'rulesTrain.log'))
local rulesTestLogger = optim.Logger(paths.concat('logs', dataSetName..'_'..timestamp..'rulesTest.log'))
local rulesTestDepthLogger = optim.Logger(paths.concat('logs', dataSetName..'_'..timestamp..'rulesTestDepth.log'))
local rulesTestLeafsLogger = optim.Logger(paths.concat('logs', dataSetName..'_'..timestamp..'rulesTestLeafs.log'))

local trainTestTrainer = {
  criterion = criterion,
  classNames = classNames,
  testData = dataTestSetTe,
  trainData = dataTestSetTr,
  mlp = mlp,
  saveModels = false, -- if true all trained models will be saved
  verbose = false     -- will print accuracy if true
}

local pruningConfig = {}
pruningConfig.afterPruningRetrainEpochs = 50
pruningConfig.maxFallbacks = 5
pruningConfig.errorWorsenForFallback = 0.008
pruningConfig.numberOfNeuronsToPrune = 1 -- Number of neurons to prune at single step
--pruningConfig.maxNodesToPrune = hidden1Size + hidden2Size + inputsSize
pruningConfig.maxNodesToPrune = hidden1Size + inputsSize
--pruningConfig.maxPruningIterations = inputsSize + hidden1Size + hidden2Size + pruningConfig.maxFallbacks * 3 --20
pruningConfig.maxPruningIterations = inputsSize + hidden1Size + pruningConfig.maxFallbacks * 3 --20
pruningConfig.classNames = classNames
--pruningConfig.retrainIterations = 10
pruningConfig.verbose = 1

local xvalIteration = 1
local xValLogs = {}
xValLogs.trainConfusion = {}
xValLogs.testConfusion = {}
xValLogs.pruningTrainConfusion = {}
xValLogs.pruningTestConfusion = {}
xValLogs.rulesTrainConfusion = {}
xValLogs.rulesTestConfusion = {}
xValLogs.pruningLogs = {}

trainTestTrainer.trainTest =
function(thiz, _model, _classNames, _trainData, _testData, _labels2Targets)

  -- calc / save stats
  local trainConf = runningUtil.train(_model, criterion, _trainData, _classNames, opt)
  local testConf = runningUtil.test(_model, _testData, _classNames, opt.batchSize)
  _model:training()
  trainConf:updateValids()
  testConf:updateValids()
  xValLogs.trainConfusion[xvalIteration] = trainConf
  xValLogs.testConfusion[xvalIteration] = testConf
  print('trainConf '.. trainConf:__tostring__())
  print('testConf '.. testConf:__tostring__())

  -- prune network
  local sensPruningLogic = nn.SensitivityPruningLogic()
  local neuronPruning1 = nn.NeuronPruning(sensPruningLogic)
  local sensPruningLog, prunedModel = neuronPruning1:runNeuronsPruning(pruningConfig, dataTestSetTr, _model:clone(), criterion, tableUtil.clone(opt))
  _model = prunedModel

  -- save stats
  local pruningTrainConf = runningUtil.test(_model, _trainData, _classNames, opt.batchSize)
  local pruningTestConf = runningUtil.test(_model, _testData, _classNames, opt.batchSize)
  pruningTrainConf:updateValids()
  pruningTestConf:updateValids()
  print('pruningTrainConf '.. pruningTrainConf:__tostring__())
  print('pruningTestConf '.. pruningTestConf:__tostring__())
  xValLogs.pruningTrainConfusion[xvalIteration] = pruningTrainConf
  xValLogs.pruningTestConfusion[xvalIteration] = pruningTestConf
  xValLogs.pruningLogs[xvalIteration] = sensPruningLog

  -- clusterize activations
  activationModifierUtil.recordActivations(_model, _trainData)

  local confusionCalcFuncTe = function()
    return runningUtil.calculateConfusionMSE(_model, _testData, _classNames, labels2TargetsTe, nil)
  end
  local confusionCalcFuncTr = function()
    return runningUtil.calculateConfusionMSE(_model, _trainData, _classNames, labels2TargetsTr, nil)
  end

  local discretizer = nn.ActivationsDiscretizer()
  local confusionBeforeClusterizationTr = confusionCalcFuncTr()
  discretizer:clusterize(_model, confusionCalcFuncTr, 1.95)
  local confusionClusteredTr = confusionCalcFuncTr()


  -- extract rules
  local maxDepth = 100
  local rulegen = nn.RulesGenerator()
  rulegen:initialize(_model, _trainData)
  local inputClustersWithBoundaries = _model:get(2).clustersTable -- collect clusters with their boundaries for input layer (usually it is layer #2)
  local rootNode = rulegen:generateTree(inputClustersWithBoundaries, maxDepth)
  print('rootNode=>\n')
  rootNode:print()
  local treeStats = rootNode:treeStats()
  print(treeStats)

  local scaledTree = rulegen:scaleTree(rootNode, newMin, newMax, originalMin, originalMax)
  print('scaledTree=>\n')
  scaledTree:print()

  -- save extracted rules stats
  local rulesTrainConf = runningUtil.calculateConfusion3(rulegen, rootNode, _trainData, _classNames, labels2TargetsTr, nil)
  local rulesTestConf = runningUtil.calculateConfusion3(rulegen, rootNode, _testData, _classNames, labels2TargetsTe, nil)
  rulesTrainConf:updateValids()
  rulesTestConf:updateValids()
  print('rulesTrainConf '.. rulesTrainConf:__tostring__())
  print('rulesTestConf '.. rulesTestConf:__tostring__())
  xValLogs.rulesTrainConfusion[xvalIteration] = rulesTrainConf
  xValLogs.rulesTestConfusion[xvalIteration] = rulesTestConf

  xvalIteration = xvalIteration + 1

  trainLogger:add{['% mean class accuracy (train set)'] = trainConf.totalValid * 100}
  testLogger:add{['% mean class accuracy (test set)'] = testConf.totalValid * 100}
  prunningTrainLogger:add{['% mean class accuracy (pruned train set)'] = pruningTrainConf.totalValid * 100}
  prunningTestLogger:add{['% mean class accuracy (pruned test set)'] = pruningTestConf.totalValid * 100}
  prunedNeuronsCount:add{['% pruning Log (total pruned neurons counts)'] = sensPruningLog.prunedNeurons[#sensPruningLog.prunedNeurons].total}
  rulesTrainLogger:add{['% mean class accuracy (rules train set)'] = rulesTrainConf.totalValid * 100}
  rulesTestLogger:add{['% mean class accuracy (rules test set)'] = rulesTestConf.totalValid * 100}
  rulesTestDepthLogger:add{['% rules maxDepth'] = treeStats.maxDepth}
  rulesTestLeafsLogger:add{['% rules count'] = treeStats.maxDepth}

  if true then
    trainLogger:style{['% mean class accuracy (train set)'] = '-'}
    testLogger:style{['% mean class accuracy (test set)'] = '+'}
    prunningTrainLogger:style{['% mean class accuracy (pruned train set)'] = '-'}
    prunningTestLogger:style{['% mean class accuracy (pruned test set)'] = '+'}
    prunedNeuronsCount:style{['% pruning Log (pruned neurons counts)'] = '-' }
    rulesTrainLogger:style{['% mean class accuracy (rules train set)'] = '-'}
    rulesTestLogger:style{['% mean class accuracy (rules test set)'] = '+'}
    rulesTestDepthLogger:style{['% mean class accuracy (rules test set)'] = '-'}
    rulesTestLeafsLogger:style{['% mean class accuracy (rules test set)'] = '-'}

    trainLogger:plot()
    testLogger:plot()
    prunningTrainLogger:plot()
    prunningTestLogger:plot()
    prunedNeuronsCount:plot()
    rulesTrainLogger:plot()
    rulesTestLogger:plot()
    rulesTestDepthLogger:plot()
    rulesTestLeafsLogger:plot()
  end
end

--profiler.start('expiris-profile.out')

runningUtil.runTestTrain(trainTestTrainer, 30)

local trainConfTensor = tensorUtil.buildTensorFromConfusionsTable(xValLogs.trainConfusion, function(confM) return confM.totalValid end)
local testConfTensor = tensorUtil.buildTensorFromConfusionsTable(xValLogs.testConfusion, function(confM) return confM.totalValid end)
local pruningTrainConfTensor = tensorUtil.buildTensorFromConfusionsTable(xValLogs.pruningTrainConfusion, function(confM) return confM.totalValid end)
local pruningTestConfTensor = tensorUtil.buildTensorFromConfusionsTable(xValLogs.pruningTestConfusion, function(confM) return confM.totalValid end)
local treeTrainConfTensor = tensorUtil.buildTensorFromConfusionsTable(xValLogs.rulesTrainConfusion, function(confM) return confM.totalValid end)
local treeTestConfTensor = tensorUtil.buildTensorFromConfusionsTable(xValLogs.rulesTestConfusion, function(confM) return confM.totalValid end)


local function printTensor(t, str)
  print(str..' mean='..t:mean()..'; std='..t:std()..';')
end

printTensor(trainConfTensor, 'mlp train')
printTensor(testConfTensor, 'mlp test')
printTensor(pruningTrainConfTensor, 'pruning train')
printTensor(pruningTestConfTensor, 'pruning test' )
printTensor(treeTrainConfTensor, 'tree train')
printTensor(treeTestConfTensor, 'tree test' )


timeExec = sys.clock() - timeExec
print("Time to execute = " .. (timeExec*1000) .. 'ms')


--profiler:stop()
--local outfile = io.open( "expirisBatch-profile.txt", "w+" )
--profiler:report( outfile )
--outfile:close()
