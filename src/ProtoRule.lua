-- ProtoRules is a class which holds combinations of Clusters (along all neurons in one layer) 
-- they are centers of intervals that will be used for rules generation

torch = require('torch')


local ProtoRule = torch.class('ProtoRule')

function ProtoRule:__init(protoRule, coveredPoints)
  self.protoRule = protoRule:clone()
  self.coveredPoints = coveredPoints
end

function ProtoRule:equals(otherProtoRule)
  return tensorUtil.tensorsEqual(self.protoRule, otherProtoRule)
end
