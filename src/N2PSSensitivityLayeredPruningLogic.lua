--require 'torch'
--require 'nn'

local N2PSSensitivityLayeredPruningLogic = torch.class('nn.N2PSSensitivityLayeredPruningLogic')
local tableUtil = require('tableUtil')
local dataUtil = require('dataUtil')

-- Class implementing NeuronPruning module. It is basically a layer which nullifies (or set's to other mean value)
-- outputs from previous layer.
--------------------------------------------
-- @param zeroVal         - Value to which we should nullify pruned neurons (1's in prunning mask)
--                          It is 0 in case Tanh is used (as Tanh output belongs to [-1, +1]
--                          and it should be assigned to 0.5 in case     
function N2PSSensitivityLayeredPruningLogic:__init()
  return self
end

local function findNextLayer(mlp, indexFrom, layerName)
  for i = indexFrom + 1, mlp:size() do
    local layer = mlp:get(i)
    if (torch.typename(layer) == layerName) then
      return layer
    end  
  end
  return nil
end

-- It is better for user to call pruneNeurons / unpruneNeurons functions.
-- pruneUnpruneFlag - 0 - means not prune, 1 - means prune
function N2PSSensitivityLayeredPruningLogic:pruneUnpruneNeurons(sensitivities, mlp, numberOfNeuronsToPrune, pruneUnpruneFlag)
    local pruneUnpruneCandidatesFunc = function(x, y) if (y == 1) then return pruneUnpruneFlag else return x end end
    for _,row in pairs(sensitivities) do
      local layerIndex = row.layerIndx
      local sensitivitiesTensor = row.sensitivitiesTensor
      
      local layer = mlp:get(layerIndex)
      local pruningMask = layer:getPruningMask()
      local pruningFlatMask = pruningMask:clone():resize(pruningMask:nElement())
      local activeNeuronsCount = pruningFlatMask:eq(0):sum()
      
      local threshold = sensitivitiesTensor:sum() / activeNeuronsCount
      local neuronsToPrune = sensitivitiesTensor:le(threshold)
      
      pruningFlatMask:map(neuronsToPrune, pruneUnpruneCandidatesFunc)
      local newPruningMask = pruningFlatMask:resize(pruningMask:size())
      layer:setPruningMask(newPruningMask)
    end
end

-- Function for calculating sensitivities for given layered network using 
-- provided training criterion and dataset. 
-- The only catch - this function returns highest sensitivities of several neurons in each layer. 
-- Of how many neurons per layer is defined by options (default - 5%)
--
-- The idea is that N2PS is used to calculate neurons significance in each layer.
-- But to find neuron with smallest sensitivity we are using error checking after prunning least significant 
-- neoron in each layer. Only sensetivities for these neurons are returned. 
--------------------------------------------
-- @param mlp         - Layered neural network.
-- @param criterion   - Criterion used to train network. Required to test network to find errors - sensititivites.
-- @param dataset     - Neural Network input data set. Should have rows consisting of two parts input and output.
-- @return sensitivities - table with rows {layerIndx, neuronIndx, sensitivity}
-- 
-- NB! method works only with PrunningLayer/ActivationRecorder/Linear layers combinations      
function N2PSSensitivityLayeredPruningLogic:calcNeuronsSensitivities(mlp, criterion, dataset)
  local sensitivities = {}
  local sIndex = 1
  
  local sigmoidF = function(a) return 1/(1 + math.exp(-1 * a)) end
  local absF = function(a) return math.abs(a) end
  local binaryNegator = function (x) if (x == 0) then return 1 else return 0 end end
  
  for i = 1,mlp:size() do
    local layer = mlp:get(i)
    if (torch.typename(layer) == 'nn.NeuronPruningLayer') then
      local pruningMask = layer:getPruningMask()
      -- do not prune this layer - only one neuron has left (actually this means something is wrong!)
      if (pruningMask[pruningMask:eq(0)]:nElement() ~= 1) then
        local sens = 0 --calcSensitivity(mlp,i,n,dataset)
        local recorderLayer = findNextLayer(mlp, i, 'nn.ActivationRecorder')
        local weightsLayer = findNextLayer(mlp, i, 'nn.Linear')            
        
        local acts = recorderLayer.activations
        local weights = weightsLayer.weight
        local totalNetActivationForNeurons = acts:sum(1)
        local totalNetActivationForNeuronsNormalized = totalNetActivationForNeurons:apply(sigmoidF)
        
        local sensitivitiesTensor = torch.Tensor(totalNetActivationForNeuronsNormalized:size()):fill(0)
        for r = 1, weights:size(1) do
          local w = weights[{{r}}]
          local summ = w:clone():add(totalNetActivationForNeuronsNormalized)
          sensitivitiesTensor:add(summ:apply(absF))
        end
        
        local flatMask = pruningMask:clone():resize(pruningMask:nElement())
        
        local maskOfActiveNeurons = flatMask:clone()
        sensitivitiesTensor = sensitivitiesTensor:view(-1)
        
        local nonPrunedNeuronsSensitivities = torch.Tensor(sensitivitiesTensor:size()):fill(0)
        maskOfActiveNeurons:apply(binaryNegator)
        nonPrunedNeuronsSensitivities:maskedCopy(maskOfActiveNeurons, sensitivitiesTensor)
        
        sensitivities[sIndex] = {layerIndx = i, sensitivitiesTensor = nonPrunedNeuronsSensitivities}
        sIndex = sIndex + 1
      end  
    end
  end
  -- Now our sensitivities holds some 'virtual' n2ps measure.
  -- we would like to take several neurons from each layer and test their error sensitivity 
  -- that resulting error sensitivity for only several (or only one) neurons in each layer
  -- will be returned
  
  local sensitivities2 = {}
  local sIndex = 1
  for i = 1,mlp:size() do
    local layer = mlp:get(i)
    if (torch.typename(layer) == 'nn.NeuronPruningLayer') then
      local pruningMask = layer:getPruningMask()
      -- do not prune this layer - only one neuron has left
      if (pruningMask[pruningMask:eq(0)]:nElement() ~= 1) then
        -- let's find sensitivities for each neuron in this layer
        for n = 1,pruningMask:nElement() do 
          local flatMask = pruningMask:clone():resize(pruningMask:nElement())
          if (flatMask[n] == 0) then
            flatMask[n] = 1
            local newMask = flatMask:resize(pruningMask:size())
            layer:setPruningMask(newMask)
            
            local currentError = calcError(mlp,criterion,dataset)
            -- save stats
            sensitivities[sIndex] = {layerIndx = i, neuronIndx = n, sensitivity = currentError}
            sIndex = sIndex + 1
            --restore pruning layer state
            layer:setPruningMask(pruningMask)
          end
        end
      end  
    end
  end
  
  return sensitivities2
end
