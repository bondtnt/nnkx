mathUtil = {}

local torch = require('torch')
local typeUtil = require('typeUtil')


-- function for returning key (index) of the element in the table.
--------------------------------------------
-- @param x       - number.    
-- @return log2(x)
function mathUtil.log2( x )
  return math.log(x) / math.log(2)
end

-- function for entropy estimation of the given vertical tensor. 
-- we assume that each row of the tensor is separate vector.
--------------------------------------------
-- @param t       - 2D tensor.    
-- @return entropy of the vectors set.

function mathUtil.tensorEntropy(tClasses) 
  typeUtil.assert(tClasses, "Tensor")
  if (_DEBUG == true) then
    if (tClasses:size():size() ~= 2) then
          error({msg="expected 2D tensor, but got " + tClasses:size():size() + "D"})
    end  
  end
  
  local uniqueRowsTensor, frequencies = tensorUtil.rowsFrequencies(tClasses)
  local rowsCount = tClasses:size()[1]
  local entropy = 0
  for k,v in pairs(frequencies) do
    local a = v / rowsCount
    entropy = entropy + (-1) * a * mathUtil.log2(a)     
  end 
  return entropy
end

-- We are making single cut. Thus we will get binary classification trees.
-- Function calculates information gain in case of splittting original 
-- tensor 't' and corresponding classes tensor 'tClasses' over column 'colNum'
-- (column number) using specified value (where cut will be made) colSplitValue
--------------------------------------------
-- @param t       - 2D tensor containing vectors as rows.
-- @param tClasses- 2D tensor containing vectors as rows for classes. Each class is a row having 1 or more elements.
-- @param colNum  - column number to make cut on.
-- @param colSplitValue- value to split over given column number.    
-- @return information gain for defined split of the table.

function mathUtil.tensorInformationGain(t, tClasses, colNum, colSplitValue) 
  typeUtil.assert(t, "Tensor")
--  if (_DEBUG == true) then
--    if (t:size():size() ~= 2) then
--          error({msg="expected 2D tensor, but got " + t:size():size() + "D"})
--    end  
--  end
  
--  local totalRowsCount = tClasses:size()[1]
  local entropyBefore = mathUtil.tensorEntropy(tClasses)
  
  local smallerT, smallerTClasses, largerT, largerTClasses = mathUtil.splitTables(t,tClasses,colNum, colSplitValue)

  local smallerTClassesEntropy = 0
  local smallerRowsCount = 0
  if (smallerT:nElement() ~= 0) then
    smallerRowsCount = smallerTClasses:size()[1]
    smallerTClassesEntropy = mathUtil.tensorEntropy(smallerTClasses)
  end
  
  local largerTClassesEntropy = 0
  local largerRowsCount = 0
  if (largerT:nElement() ~= 0) then
    largerRowsCount = largerTClasses:size()[1]
    largerTClassesEntropy = mathUtil.tensorEntropy(largerTClasses)
  end
  
  local totalRowsCount = t:size()[1]
  local entropyAfter = (smallerRowsCount / totalRowsCount) * smallerTClassesEntropy +  
                  (largerRowsCount / totalRowsCount) * largerTClassesEntropy
                  
  local infoGain = entropyBefore - entropyAfter
                  
  return infoGain
end


-- This function splits t tensor and tClasses tensor based on columnNumber and splitValue for that column (in t tensor)
-- t is being sorted on specified column and then splitted.
function mathUtil.splitTables(t, tClasses, colNum, colSplitValue)
  local totalRowsCount = t:size()[1]
  local column = t[{{},{colNum}}]
  local sortedCol, indx = torch.sort(column, 1)
  local i = 0
  while (i + 1 < totalRowsCount) do
    if (sortedCol[i + 1][1] <= colSplitValue) then
      i = i + 1
    else 
      break
    end
  end
  if (sortedCol[i + 1][1] <= colSplitValue) then
    i = i + 1
  end
  
  -- get table part which holds values smaller than given colSplitValue
  -- extract corresponding tClasses as well
  local smallerIndexes = indx:index(1, torch.range(1, indx:nElement()):long())[sortedCol:le(colSplitValue)]:long()
  local smallerRowsCount = 0
  local smallerT = torch.DoubleTensor()
  local smallerTClasses = torch.Tensor()
  if (smallerIndexes:nElement() ~= 0) then 
    smallerT = t:index(1, smallerIndexes:resize(smallerIndexes:nElement()))
    smallerTClasses = tClasses:index(1, smallerIndexes:resize(smallerIndexes:nElement()))
  end
  
  -- get table part which holds values greater than given colSplitValue
  -- extract corresponding tClasses as well
  local largerIndexes = indx:index(1, torch.range(1, indx:nElement()):long())[sortedCol:gt(colSplitValue)]:long()
  local largerT = torch.Tensor()
  local largerTClasses = torch.Tensor()
  if (largerIndexes:nElement() ~= 0) then
    largerT = t:index(1, largerIndexes:resize(largerIndexes:nElement()))
    largerTClasses = tClasses:index(1, largerIndexes:resize(largerIndexes:nElement()))
  end 

  return smallerT, smallerTClasses, largerT, largerTClasses
end

-- Function accepts two tables, one contains data clusters - each row is 
-- a tensor with cluster centers. And the other is output clusters 
-- (in case we have single neuron there will be single valued tensors) values.
-- Function calculates index of attribute and value between two rows for data split. 
--------------------------------------------
-- @param t       - 2D tensor containing vectors as rows.
-- @param tClasses- 2D tensor containing vectors as rows for classes. Each class is a row having 1 or more elements.
-- @param inputClustersWithBoundaries - table holding 2D tensors that in each row hold clustered value in column #1 and upper cluster boundary (splitValue) in column #2 
-- @return colNum  - column number to make cut on.
-- @return colSplitValue- value to split over given column number.
-- @return calculationsPerformed - true if everything worked, false if no calculations was performed
function mathUtil.findBestSplit(t, tClasses, inputClustersWithBoundaries) 
  typeUtil.assert(t, "Tensor")
--  if (_DEBUG == true) then
--    if (t:size():size() ~= 2) then
--          error({msg="expected 2D tensor, but got " + t:size():size() + "D"})
--    end  
--  end
  local calculationsPerformed = false
  local maxInfoGain = -1 * math.huge
  local columnIndex = 0
  local splitValue = -1 * math.huge
  local columnsCount = t:size()[2]
  local totalRowsCount = t:size()[1]
  
  for colNum = 1,columnsCount do 
     
    local column = t[{{},{colNum}}]:clone()
    local sortedCol, indx = torch.sort(column, 1)
    
    -- adjust tClasses sorting
    local indexes = indx:index(1, torch.range(1, indx:nElement()):long()):long():resize(indx:nElement())
    local resortedTClasses = tClasses:index(1, indexes)
    local resortedT = t:index(1, indexes)
    
    for rowNum = 1,(totalRowsCount-1) do
      local currentRowValue = resortedT[{{rowNum},{colNum}}][1][1]
      local nextRowValue = resortedT[{{rowNum+1},{colNum}}][1][1]
      local currentRowClassValue = resortedTClasses[rowNum][1]
      local nextRowClassValue = resortedTClasses[rowNum+1][1]
      if (currentRowClassValue ~= nextRowClassValue) then
        local clustersForGivenNeuron = inputClustersWithBoundaries[colNum]
        local splitRowNum = tensorUtil.keyForValue(clustersForGivenNeuron[{{},{1}}], currentRowValue)[1]
        local tmpSplitValue = clustersForGivenNeuron[{{splitRowNum},{2}}][1][1]

        local tmpInfoGain = mathUtil.tensorInformationGain(resortedT, resortedTClasses, colNum, tmpSplitValue)
        if (tmpInfoGain > maxInfoGain) then
          maxInfoGain = tmpInfoGain
          columnIndex = colNum
          splitValue = tmpSplitValue  
          calculationsPerformed = true
        end
      end
    end
  end
  
  return columnIndex, splitValue, calculationsPerformed
end

-- Function accepts two tables, one contains data clusters - each row is 
-- a tensor with cluster centers. And the other is output clusters 
-- (in case we have single neuron there will be single valued tensors) values.
-- Function calculates index of attribute and value between two rows for data split. 
--------------------------------------------
-- @param x       - Number.
-- @param min     - Old range minimum
-- @param max     - Old range maximum
-- @param newMin  - New range minimum
-- @param newMax  - New range maximum
-- @return x scaled to new range
function mathUtil.scaleValue(x, oldMin, oldMax, newMin, newMax)
  local scalingFactor = (newMax - newMin) / (oldMax - oldMin)
  return scalingFactor*(x - oldMin) + newMin   
end

return mathUtil