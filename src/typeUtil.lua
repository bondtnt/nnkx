typeUtil = {}

local torch = require('torch')

function typeUtil.assert( obj, expectedType )
  if (_DEBUG == true) then
    local objType = torch.type(obj)
    
    if (string.find(expectedType, "Tensor") ~= nil) then
      if (string.find(objType, expectedType) == nil) then
        error({msg="expected type = " + expectedType + ", but got " + objType})
      end
      
    elseif (string.find(expectedType, "Storage") ~= nil) then
      if (string.find(objType, expectedType) == nil) then
        error({msg="expected type = " + expectedType + ", but got " + objType})
      end   
          
    elseif (objType ~= expectedType) then
      error({msg="expected type = " .. expectedType .. ", but got " .. objType})
    end
  
  end
  return true
end

function typeUtil.isTensor( obj )
  local objType = torch.type(obj)
  if (string.find(objType, "Tensor") ~= nil) then
    return true
  else 
    return false  
  end
end

function typeUtil.isStorage( obj )
  local objType = torch.type(obj)
  if (string.find(objType, "Storage") ~= nil) then
    return true
  else 
    return false  
  end
end

return typeUtil