activationModifierUtil = {}

torch = require('torch')
typeUtil = require('typeUtil')
tensorUtil = require('tensorUtil')

-- This function is called from outside to build intervals tensor from given activations.
-- Intervals tensor will hold 2 columns - begin and end of interval.
-- @param intervals - is basically activations expanded to 2 column vector.
-- @param fromIndx, toIndx - should be 1 and intervals:size()[1].
-- @param intervalsAreAcceptableFunc - function used to test neural network - to decide about merging of intervals
function activationModifierUtil.buildIntervals(intervals, fromIndx, toIndx, intervalsAreAcceptableFunc)
  typeUtil.assert(torch.Tensor(),"Tensor")
  
  if (intervals:size()[1] == 1 or fromIndx == toIndx) then
    return intervals -- nothing to clusterize - there is single interval
  end

  local intervalsBackup = intervals:clone()
  local continueMergingFlag = true
  local mergedIntervals, iIndx, jIndx = nil, nil, nil
  while (continueMergingFlag) do
    mergedIntervals, iIndx, jIndx = activationModifierUtil.mergeIntervals(intervals, fromIndx, toIndx)
    local intervalsAreGood = (intervalsAreAcceptableFunc(mergedIntervals) == true)
    continueMergingFlag = (intervalsAreGood) and (mergedIntervals:size()[1] > 1) and (fromIndx ~= toIndx)
    if (continueMergingFlag) then
      intervals = mergedIntervals
      intervalsBackup = mergedIntervals:clone()
      toIndx = toIndx - 1 -- intervals collection become 1 row shorter
    elseif (intervalsAreGood == false) then
      mergedIntervals = intervalsBackup  
    end  
  end
  
  if (mergedIntervals:size()[1] > 1) then
    -- skip these merged intervals and clusterize to the left and to the right from merge point
    local mergedLeftIntervals = activationModifierUtil.buildIntervals(mergedIntervals, fromIndx, iIndx, intervalsAreAcceptableFunc)
    local mergedLeftAndRightIntervals = activationModifierUtil.buildIntervals(mergedLeftIntervals, jIndx, toIndx, intervalsAreAcceptableFunc)
    return mergedLeftAndRightIntervals
  else 
    -- we have merged everythin to single interval - return it
    return mergedIntervals  
  end
end

function activationModifierUtil.mergeIntervals(intervals, fromIndx, toIndx)
  if (fromIndx == toIndx) then
    return intervals, fromIndx, toIndx
  end
  local dMin, iIndx, jIndx = tensorUtil.findNearestElements(intervals[{{fromIndx, toIndx}, {1}}])
  iIndx = iIndx + fromIndx - 1 -- make these indexes absolute
  jIndx = jIndx + fromIndx - 1 -- make these indexes absolute
  local jValue = intervals[jIndx][2]
  intervals[iIndx][2] = jValue
  intervals = tensorUtil.delete2DTensorRow(intervals, jIndx)
  return intervals, iIndx, jIndx
end

-- @param activations - must ne 2D tensor with 1 column - all activations for given neuron
-- @param epsilon - distance for merging points into single cluster
-- @result - clusters tensor with 1st column being clusterValue (center) and 2nd column being upper cluster boundary
--           these clusters will be used to map nonlinear neuron activations into clusterValues - thus it will do 
--           so called activations discretization   
function activationModifierUtil.buildActClusters(activations, epsilon) --TODO - remove this method
  local initialClusters = activations:clone():cat(activations:clone(), 2)
  for i = 1, initialClusters:size()[1] - 1 do
    initialClusters[i][2] = initialClusters[i][2] + torch.abs(initialClusters[i][2] - initialClusters[i+1][2]) / 2
  end
  return activationModifierUtil.buildActClustersX(initialClusters, 1, initialClusters:size()[1], epsilon)
end

-- entry point for building clusters of activations
function activationModifierUtil.clusterize(initialClusters, epsilon) --TODO - remove this method
  return activationModifierUtil.buildActClustersX(initialClusters, 1, initialClusters:size()[1], epsilon)
end

function activationModifierUtil.buildActClustersX(clusters, fromIndx, toIndx, epsilon)
  typeUtil.assert(clusters, "Tensor")
  
  if (clusters:size()[1] == 1 or fromIndx == toIndx) then
    return clusters:clone() -- nothing to clusterize - there is single interval
  end
  
  local c = clusters[{{}, {1}}]
  if (c == nil) then
    print('nil')
  end  
  local indx = torch.abs(c[{{1,-2},{}}] - c[{{2,-1},{}}]):le(epsilon) -- these are indexes for what we should merge if index i = 1 => means i & i+1 activations are in one cluster
  
  local mergedClusters = torch.Tensor({math.huge * -1})
  
  if (indx:sum() == 0) then
    -- no clusters will be formed
    return clusters:clone()  
  else 
    local initialized = false
    local sum = 0
    local count = 0
    local inCluster = false
    
    for i = 1, indx:size(1) do
      if inCluster == false and indx[i][1] == 1 then
        inCluster = true
        sum = c[i][1] + c[i+1][1]
        count = 2
      elseif inCluster and indx[i][1] == 1 then
        sum = sum + c[i+1][1]
        count = count + 1      
      elseif inCluster and indx[i][1] == 0 then
        local clusterCenter = sum/count
        if (initialized == false) then
          initialized = true
          mergedClusters[1] = clusterCenter  
        else 
          mergedClusters = torch.cat(mergedClusters, torch.Tensor({clusterCenter}), 1)
        end
        inCluster = false
        sum = 0
        count = 0
      elseif inCluster == false and indx[i][1] == 0 then
        -- just add activation value as a standalone cluster
        if (initialized == false) then
          initialized = true
          mergedClusters[1] = c[i][1]  
        else 
          mergedClusters = torch.cat(mergedClusters, torch.Tensor({c[i][1]}), 1)
        end
      end
    end
    if inCluster then
      local clusterCenter = sum/count
      if (initialized == false) then
        initialized = true
        mergedClusters[1] = clusterCenter  
      else 
        mergedClusters = torch.cat(mergedClusters, torch.Tensor({clusterCenter}), 1)
      end 
    end
  end 
  
  
  -- add second column - where boundaries will be held
  mergedClusters = mergedClusters:resize(mergedClusters:size(1), 1) -- make it 2D tensor
  mergedClusters = torch.cat(mergedClusters, torch.Tensor(mergedClusters:size(1), 1):fill(0), 2)
  
  -- now let's rebuild clusters boundaries - second column
  if (mergedClusters:size(1) > 1) then
    for i = 1, mergedClusters:size(1)-1 do
      local mean = (mergedClusters[i][1] + mergedClusters[i+1][1]) / 2
      mergedClusters[i][2] = mean
    end
  end
  mergedClusters[mergedClusters:size(1)][2] = -1 * math.huge
  
  return mergedClusters:clone()
end

-- Function takes clusters tensor and looks for two nearest points.
-- if epsilon parameter is supplied then if points are merged into cluster if distance between them
-- is smaller than epsilon. If epsilon is not provided it is defaulted to math.huge.
-- Function operates only on subtensor rows deteremined by fromIndx and toIndx parameters.
function activationModifierUtil.mergeClusters(clusters, fromIndx, toIndx, epsilon, clusterizeFromEnd)
  if (clusterizeFromEnd == nil) then
    clusterizeFromEnd = false
  end
  if (fromIndx == toIndx) then
    return clusters, fromIndx, toIndx
  end
  if (epsilon == nil) then
    epsilon = math.huge
  end
  local dMin, iIndx, jIndx = tensorUtil.findNearestElements(clusters[{{fromIndx, toIndx}, {1}}], clusterizeFromEnd)
  iIndx = iIndx + fromIndx - 1 -- make these indexes absolute
  jIndx = jIndx + fromIndx - 1 -- make these indexes absolute
  local continue = false
  if (dMin < epsilon) then
    -- adjust cluster value (center) for row iIndx - it should be between iIndx center and jIndex center
    local mean = torch.dist(torch.Tensor({clusters[iIndx][1]}), torch.Tensor({clusters[jIndx][1]})) / 2
    clusters[iIndx][1] = clusters[iIndx][1] - mean 
    clusters = tensorUtil.delete2DTensorRow(clusters, jIndx)
    continue = true
  end
  return clusters, iIndx, jIndx, continue
end

function activationModifierUtil.recordActivations(mlp, dataset)
  -- BEGIN of activations collection
  -- enable activations collection
  runningUtil.applyToFilteredLayers(mlp, 'nn.ActivationRecorder', function(layer) layer.recordFlag = true end)
  -- collect activations
  for i = 1,#dataset do
    local example = dataset[i]
    local input = example[1]
    mlp:forward(input) -- now ActivationRecorder's will collect activations
  end
  -- disable activations collection
  runningUtil.applyToFilteredLayers(mlp, 'nn.ActivationRecorder', function(layer) layer.recordFlag = false end)
  -- END of activations collection
end

return activationModifierUtil