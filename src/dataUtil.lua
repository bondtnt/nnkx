dataUtil = {}

local torch = require('torch')
local tableUtil = require('tableUtil')


-- function for returning indices of folds
--------------------------------------------
-- @param dataset     - standard dataset table. 
--                      Must contain size function and 2 columns data, labels
-- @param k           - parameter for splitting dataset. Double, usually = 10 (folds count)         
function dataUtil.cvpartitionKFold(labels, k)
  local n = #labels
  local uniqueset, j = tableUtil.uniqueTableValues(labels) --todo bug??? seems uniqueTableValues bad, should be  
  local n_per_class = tableUtil.frequencies(labels)
  local n_classes = #uniqueset
  
  local jTensor = torch.Tensor(j)
--  C = {"classes" = , "inds" = {}, "n_classes" = {}, "NumObservations" = {}, "NumTestSets" = {}, "TestSize" = {}, "TrainSize" = {}, "Type" = {}}
  local C = {}
  -- The non-Matlab fields classes, inds, n_classes are only useful for some methods
  local indsTensor = torch.Tensor()
  if (n_classes == 1) then
    indsTensor = torch.range(0, (n-1)):mul(k/n):floor():add(1)
--    inds = floor((0:(n-1))' * (k / n)) + 1;
  else 
--    inds = nan(n, 1);
    indsTensor = torch.Tensor(n):fill(0);
    for i = 1,n_classes do
      if (i % 2) == 0 then -- alternate ordering over classes so that the subsets are more nearly the same size
  --      inds(j == i) = floor((0:(n_per_class(i)-1))' * (k / n_per_class(i))) + 1;
        local range = torch.range(0, n_per_class[uniqueset[i]]-1)
        local value = range:mul(k/n_per_class[uniqueset[i]]):floor():add(1)
        indsTensor[jTensor:eq(i)] = value
      else
  --      inds(j == i) = floor(((n_per_class(i)-1):-1:0)' * (k / n_per_class(i))) + 1;
        local range = torch.range(0, n_per_class[uniqueset[i]]-1)
        local reversedRange, _ = torch.sort(range, 1, true)
        local value = reversedRange:mul(k/n_per_class[uniqueset[i]]):floor():add(1)
        indsTensor[jTensor:eq(i)] = value
--        r = torch.range(0, n_per_class[i]-1)
--        y, _ = torch.sort(r, 1, true)
--        indx = math.floor(y * (k/n_per_class[i])) + 1
--        inds[indx:eq(i)]
      end
    end
  end
  C.inds = indsTensor;
  C.NumTestSets = k;
  local indsTable = indsTensor:totable()
  local _, jj = tableUtil.uniqueTableValues(indsTable)
--  [~, ~, jj] = unique (inds);
  local n_per_subset = tableUtil.frequencies(jj)
--  n_per_subset = accumarray (jj, 1);     
  C.TrainSize = torch.Tensor(n_per_subset):mul(-1):add(n):totable()
  C.TestSize = n_per_subset;
  C.NumObservations = n;
  C.Type = string.lower('KFold');
  
  C.getTrainFold = 
    function(foldNumber) 
      return dataUtil.neq(C.inds, foldNumber)
    end
  C.getTestFold = 
    function(foldNumber) 
      return C.inds:eq(foldNumber)
    end
  
  return C
end

function dataUtil.neq(table, value)
  local tableCopy = torch.Tensor(table):clone() --:totable()
  local indexes = tableCopy:apply(
    function(x) 
      if (x ~= value) then 
        return 1 
      else 
        return 0 
      end 
    end)
    
   return indexes:type('torch.ByteTensor') 
end

-- LABELSCOLUMNTOMATRIX Transforms labels column into Neural Network
--                      output layer targets.
--    [1; 2; 3] is transformed into [1, 0, 0; 0, 1, 0; 0, 0, 1]
--    we are talking about classfication labels, not regression
--------------------------------------------
-- @param labels     - Tensor having labels column - of length 1.
-- @return table holding target vectors for neural network
function dataUtil.labelsColumnToMatrix( labels, minVal, maxVal )
  if (labels:size():size() > 1) then
    assert(labels:size()[2] == 1, 'labels should contain single column')
  end
    
  local labels2Targets = {}
  local classN = 0
  local freq = {}
  local flatLabels = labels:clone():resize(labels:nElement())
  for k, v in pairs(flatLabels:totable()) do
    if (freq[v] == nil) then
      freq[v] = 1
      classN = classN + 1
    else   
      freq[v] = freq[v] + 1
    end   
  end
  
  local classIndexes = {}
  local i = 1
  for k, _ in pairs(freq) do
    classIndexes[k] = i
    i = i + 1
  end 
  
  local targetLabels = torch.Tensor(flatLabels:size()[1], classN):fill(minVal)
  for i = 1,flatLabels:size()[1] do 
    local y = labels[i]
    local col = classIndexes[y[1]]
    targetLabels[i][col] = maxVal       
    if (labels2Targets[y[1]] == nil) then
      labels2Targets[y[1]] = targetLabels[i] 
    end
  end   
  return targetLabels, classN, labels2Targets
end

function dataUtil.convertToDataset(data, labels)
  local dataset = {}
  local s = data:size(1)
  for i=1,s do
    local input = data[i]
    local output = labels[i]
    dataset[i] = {input, output}
  end
  dataset.size = function(self)
    return data:size(1)
  end
  
  return dataset
end

function dataUtil.tensorsFromDataset(dataset)
  local rowsCount = dataset.size()
  local rowLength = dataset[1][1]:size(1)
  local labelLength = dataset[1][2]:size(1)
  local X = torch.Tensor(rowsCount, rowLength):fill(0)
  local y = torch.Tensor(rowsCount, labelLength):fill(0)
  local i = 1
  for _, v in pairs(dataset) do
    if ('function' ~= type(v)) then 
      local input = v[1]
      local output = v[2]
      X[i] = input:view(-1)
      y[i] = output:view(-1)
      i = i + 1
    end
  end
  return X, y
end

return dataUtil