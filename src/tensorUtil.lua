tensorUtil = {}

local torch = require('torch')
require('tableUtil')

-- function for flattening given tensor.
--------------------------------------------
-- @param t       - tensor.
-- @return 1D tensor containg all values from input tensor t.
function tensorUtil.flatten(t)
  typeUtil.assert(t, "Tensor")
  return t:view(t:nElement())
end

-- function for returning key (index) of the element in the tensor.
--------------------------------------------
-- @param t       - tensor.
-- @param value   - value to look for     
-- @return tensor containg indices for values. If t is 2 dimensional, indices will be 2D. 
function tensorUtil.keyForValue( t, value )
  typeUtil.assert(t, "Tensor")
  return torch.linspace(1, t:nElement(), t:nElement())[t:eq(value)]
end

-- function for returning indices of folds
--------------------------------------------
-- @param t       - tensor. 
-- @return uniqueset - tensor with unique t values
-- @return ic           
function tensorUtil.uniqueTensorValues(t)
  typeUtil.assert(t, "Tensor")
  local valsetTable = tensorUtil.frequencies(t)
  -- now convert that valset table keys into normal table with values
  local uniqueset={}
  local n=0
  for k,_ in pairs(valsetTable) do
    n=n+1
    uniqueset[n]=k
  end
  
  local ic = tableUtil.icForUnique(uniqueset, t:totable())
  
  return uniqueset, ic
end

-- function for returning indices of folds
--------------------------------------------
-- @param t       - 2D tensor. 
-- @return uniqueset - tensor with unique t values
-- @return ic           
function tensorUtil.uniqueTensorRows(t)
  typeUtil.assert(t, "Tensor")
  local valsetTable = tensorUtil.frequencies(t)
  -- now convert that valset table keys into normal table with values
  local uniqueset={}
  local n=0
  for k,_ in pairs(valsetTable) do
    n=n+1
    uniqueset[n]=k
  end
  
  local ic = tableUtil.icForUnique(uniqueset, t:totable())
  
  return uniqueset, ic
end

-- function for checking table contains element
--------------------------------------------
-- @param t       - tensor.
-- @param value     - value to check.
-- @return true / false     
function tensorUtil.contains(t, value)
  typeUtil.assert(t, "Tensor")
  
  if 0 == t[t:eq(value)]:nElement() then
    return false
  end
  return true
end

-- function returns table with keys equal to unique values from input tensor
-- and values equal to number of values encountered in input
function tensorUtil.unique_counts(input)
  typeUtil.assert(input, "Tensor")

  size = input:nElement()
  y = input:view(size) --flatten input
  b = {}
  for i=1,size do
    a = y[i]
    if b[a] == nil then 
      b[a] = 1
    else 
      b[a] = b[a] + 1
    end  
  end
  return b
end

-- function for returning frequencies of the elements in the tensor.
--------------------------------------------
-- @param t       - 1D tensor.    
-- @return table containing frequencies.  
function tensorUtil.frequencies(t)
  typeUtil.assert(t, "Tensor")

  local valset = {}
  local tt = t:view(-1)
  local i = 1
  for i = 1,tt:nElement() do
    local v = tt[i]
    if (valset[v] == nil) then
      valset[v] = 1
    else 
      valset[v] = valset[v] + 1
    end  
  end
  return valset
end

-- function for returning frequencies of the rows elements in the table.
--------------------------------------------
-- @param t       - 2D tensor. 
-- @return uniqueTensorRows - tensor with unique rows   
-- @return frequencies - table containing frequencies. Key is row index in uniqueTensorRows tensor.   
function tensorUtil.rowsFrequencies(t)
  typeUtil.assert(t, "Tensor")
  
  local rowLength = t:size()[2]
  local uniqueTensorRows = torch.Tensor()
  local frequencies = {}
  
  for i = 1, t:size()[1] do
    local tensorRow = t[i]:clone():resize(1, rowLength)
    if (uniqueTensorRows:size():size() == 0) then
      uniqueTensorRows = tensorRow
      frequencies[1] = 1
    elseif (false == tensorUtil.tensorContainsRow(tensorRow, uniqueTensorRows)) then 
      uniqueTensorRows = uniqueTensorRows:cat(tensorRow, 1)
      frequencies[#frequencies + 1] = 1
    else 
      local rowIndex = tensorUtil.findRow(tensorRow, uniqueTensorRows)
      frequencies[rowIndex] = frequencies[rowIndex] + 1
    end
  end
  
  return uniqueTensorRows, frequencies
end

-- function for returning index of row in tensor containing given row.
--------------------------------------------
-- @param t       - 2D tensor. Within this tensor we will look for rows equal to given one. 
-- Index of first equal will be returned. If nothing is found -1 is returned.
-- 
-- @return uniqueTensorRows - tensor with unique rows   
-- @return frequencies - table containing frequencies. Key is row index in uniqueTensorRows tensor.   
function tensorUtil.findRow(row, t)
  typeUtil.assert(t, "Tensor")
  
  for i = 1,t:size()[1] do
    if (tensorUtil.tensorsEqual2(row, t[i])) then
      return i
    end
  end
  return -1
   
end
  
-- function for returning minimal distance between 2 elements of tensor.
--------------------------------------------
-- @param t       - must be sorted (!) 1 column tensor with at least 1 element
-- @return minDistance, index1, index2. 
function tensorUtil.findNearestElements(tensor, fromEnd)
  if (tensor:nElement() == 1) then
    return 0, 1, 1
  end
  
  local dMin = math.huge
  local iIndx = -1
  local jIndx = -1
  for i = 1, tensor:nElement()-1 do
    if (fromEnd ~= nil and fromEnd == true) then
      i = tensor:nElement() - i
    end
    
    local dist = math.huge
    if (tensor:size():size() == 2) then
      dist = math.abs(tensor[i][1] - tensor[i+1][1])
    elseif (tensor:size():size() == 1) then
      dist = math.abs(tensor[i] - tensor[i+1])
    else
      local dim = tensor:size():size()
      error({msg="expected 1 column tensor or vector , but got " .. dim .. " dimensions"})
    end
    if (dist < dMin) then
      dMin = dist
      iIndx = i
      jIndx = i+1
    end
  end
  
  return dMin, iIndx, jIndx
end

-- function for deleting 2D tensor row.
--------------------------------------------
-- @param tensor       - must be 2D tensor
-- @param row          - row index to remove
-- @return tensor without specified row 
function tensorUtil.delete2DTensorRow(tensor, row)
  typeUtil.assert(tensor,'Tensor')
  local mask = torch.ByteTensor(tensor:size()):fill(1)
  mask[row]:fill(0)
  local newSize = tensor:size()
  newSize[1] = newSize[1] - 1
  return tensor:maskedSelect(mask):resize(newSize)
end

function tensorUtil.typesEqual(o1, o2)
  return type(o1) == type(o2)  
end

function tensorUtil.storagesEqual(s1, s2)
  typeUtil.assert(s1,'Storage')
  typeUtil.assert(s2,'Storage')
  if (type(s1) == type(s2)) then
    if (s1:size() == s2:size()) then
      for i = 1,s1:size() do
        if (s1[i] ~= s2[i]) then
          return false  
        end 
      end
      return true
    end
  end
  return false
end

-- function returns true if 2 passed tensors are equal (by shape and contents)
function tensorUtil.tensorsEqual(t1, t2)
--  return torch.all(torch.eq(t1,t2)) -- this does not respect dimensionalities
  t1 = t1:clone()
  t2 = t2:clone()
  typeUtil.assert(t1, 'Tensor')
  typeUtil.assert(t2, 'Tensor')
  if (tensorUtil.storagesEqual(t1:size(), t2:size())) then
    local flatT1 = tensorUtil.flatten(t1)
    local flatT2 = tensorUtil.flatten(t2)
    return flatT1:add(flatT2:mul(-1)):eq(0):sum() == flatT1:nElement()
  end
  return false  
end

-- function returns true if 2 passed tensors are equal (by contents!)
function tensorUtil.tensorsEqual2(t1, t2)
  return torch.all(torch.eq(t1,t2)) -- this does not respect dimensionalities
end

-- function returns TRUE if given 1D tensor (row) is contained within given 2D tensor (holding rows)
-- row - is a 1D tensor
-- tensor - is a 2D tensor which will be checked (does it contains row under question) 
function tensorUtil.tensorContainsRow(row, tensor)
  for i = 1, tensor:size()[1] do
    local tensorRow = tensor[i]
    if (tensorUtil.tensorsEqual2(row, tensorRow)) then
      return true
    end
  end
  return false  
end

-- function scales 2D tensor columns into specified range
-- assumes there are no NaN's
-- tensor - is a 2D tensor which will be checked (does it contains row under question) 
function tensorUtil.scaleTensor(t, newMinVal, newMaxVal)
  -- remove nan's rows
  local nans = t:ne(t):max(2)
  local nansIndices = torch.linspace(1, t:size(1), t:size(1)):long()
  local datain = t:index(1, nansIndices[nans:eq(0)]):clone()
  
  local oldMinValue = datain:min()
  local oldMaxValue = datain:max()
  local oldRange = oldMaxValue - oldMinValue
  if (oldRange == 0) then
    return datain:apply(function(x) 
      return oldMinValue 
    end), oldMinValue, oldMaxValue
  else 
    local newRange = newMaxVal - newMinVal
    return datain:apply(function(x) 
      return (((x - oldMinValue) * newRange) / oldRange) + newMinVal 
    end), oldMinValue, oldMaxValue
  end
  
end

-- function assumes there is linear table, it's values will be used to fill tensor
function tensorUtil.buildTensorFromConfusionsTable(table, extractorFunc)
  local size = tableUtil.getSize(table)
  local tensor = torch.Tensor(size)
  for i = 1, size do
    tensor[i] = extractorFunc(table[i])
    i = i + 1
  end
  return tensor
end  

-- Function for writing tensor to csv file. 
-- Should be used like:
-- local out = assert(io.open("./dump.csv", "w"))
-- tensorUtil.writeTensorToCsv(t, out, ",")
-- out:close()
function tensorUtil.writeTensorToCsv(t, out, splitter)
  for i=1,t:size(1) do
    for j=1,t:size(2) do
      out:write(t[i][j])
      if j == t:size(2) then
        out:write("\n")
      else
        out:write(splitter)
      end
    end
  end
end


return tensorUtil