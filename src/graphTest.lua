nngraph = require('nngraph')
require 'graph'

local h1 = nn.Linear(20, 10)()
local h2 = nn.Linear(10, 1)(nn.Tanh()(nn.Linear(10, 10)(nn.Tanh()(h1))))
local mlp = nn.gModule({h1}, {h2})

local x = torch.rand(20)
local dx = torch.rand(1)
mlp:updateOutput(x)
mlp:updateGradInput(x, dx)
mlp:accGradParameters(x, dx)

-- draw graph (the forward graph, '.fg')
--graph.dot(mlp.fg, 'MLP')
--graph.dot(mlp.fg, 'MLP', 'outputBasename2')



require('ProtoRule')
require('DecisionTreeLeaf')
require('DecisionTreeNode')

local g = graph.Graph()

local rootNode = graph.Node("< 0.2")
rootNode.graphNodeName = function() return "Attr#2" end
local consequenceNode1 = graph.Node('')
consequenceNode1.graphNodeName = function() return "Class1" end
local consequenceNode2 = graph.Node('')
consequenceNode2.graphNodeName = function() return "Class2xxx" end
g:add(graph.Edge(rootNode, consequenceNode1))
g:add(graph.Edge(rootNode, consequenceNode2))


require('graph.graphviz')
require('graph.Node')
require('graph.Edge')

graphvizOk, graphviz = pcall(function() return ffi.load('libgvc', true) end)
 if not graphvizOk then
    graphvizOk, graphviz = pcall(function() return ffi.load('libgvc.so.6', true) end)
 end

 cgraphOk, cgraph = pcall(function() return ffi.load('libcgraph', true) end)
 if not cgraphOk then
    cgraphOk, cgraph = pcall(function() return ffi.load('libcgraph.so.6', true) end)
 end
 
--graph.dot(g, 'DT', 'smallGraph')

function todot(graph, title)
   local nodes = self.nodes
   local edges = self.edges
   local str = {}
   table.insert(str,'digraph G {\n')
   if title then
      table.insert(str,'labelloc="t";\nlabel="' .. title .. '";\n')
   end
   table.insert(str,'node [shape = oval]; ')
   local nodelabels = {}
   for i,node in ipairs(nodes) do
      local nodeName
      if node.graphNodeName then
         nodeName = node:graphNodeName()
      else
         nodeName = 'Node' .. node.id
      end
      local l =  graph._dotEscape(nodeName .. '\n' .. node:label())
      nodelabels[node] = 'n' .. node.id
      local graphAttributes = ''
      if node.graphNodeAttributes then
         graphAttributes = makeAttributeString(
         node:graphNodeAttributes())
      end
      table.insert(str,
      '\n' .. nodelabels[node] ..
      '[label=' .. l .. graphAttributes .. '];')
   end
   table.insert(str,'\n')
   for i,edge in ipairs(edges) do
      table.insert(str,nodelabels[edge.from] .. ' -> ' .. nodelabels[edge.to] .. ';\n')
   end
   table.insert(str,'}')
   return table.concat(str,'')
end




function graphvizFile(g, algorithm, fname)
   if not graphvizOk or not cgraphOk then
      error("graphviz library could not be loaded.")
   end
   algorithm = algorithm or 'dot'
   local _,_,rendertype = fname:reverse():find('(%a+)%.%w+')
   rendertype = rendertype:reverse()

   local context = graphviz.gvContext()
   local graphvizGraph = cgraph.agmemread(todot(g))

   assert(0 == graphviz.gvLayout(context, graphvizGraph, algorithm),
          "graphviz layout failed")

   local fhandle = ffi.C.fopen(fname, 'w')
   local ret = graphviz.gvRender(context, graphvizGraph, rendertype, fhandle)
   ffi.C.fclose(fhandle)
   assert(0 == ret, "graphviz render failed")

   graphviz.gvFreeLayout(context, graphvizGraph)
   cgraph.agclose(graphvizGraph)
   graphviz.gvFreeContext(context)
end

--[[
Given a graph, dump an SVG or display it using graphviz.
Args:
* `g` - graph to display
* `title` - Title to display in the graph
* `fname` - [optional] if given it should contain a file name without an extension,
the graph is saved on disk as fname.svg and display is not shown. If not given
the graph is shown on qt display (you need to have qtsvg installed and running qlua)
Returns:
* `qs` - the window handle for the qt display (if fname given) or nil
]]
function dot(g,title,fname)
   local qt_display = fname == nil
   fname = fname or os.tmpname()
   local fnsvg = fname .. '.svg'
   local fndot = fname .. '.dot'
   graphvizFile(g, 'dot', fnsvg)
   graphvizFile(g, 'dot', fndot)
   if qt_display then
      require 'qtsvg'
      local qs = qt.QSvgWidget(fnsvg)
      qs:show()
      os.remove(fnsvg)
      os.remove(fndot)
      return qs
   end
end

dot(g, 'DT', 'smallGraph')
