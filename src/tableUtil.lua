tableUtil = {}

local torch = require('torch')
local typeUtil = require('typeUtil')


-- function for returning key (index) of the element in the table.
--------------------------------------------
-- @param table       - table.    
-- @return firs key for value
function tableUtil.keyForValue( t, value )
  typeUtil.assert(t, "table")
  
  for k,v in pairs(t) do
    if (v == value) then 
      return k
    end
  end
  return nil
end

-- function for returning frequencies of the elements in the table.
-- Used by uniqueTableValues(table)
--------------------------------------------
-- @param table       - table.    
-- @return table wich will assign values - indices from uniqueTable mapping to originalTable 
function tableUtil.icForUnique(uniqueTable, originalTable)
  typeUtil.assert(uniqueTable, "table")
  typeUtil.assert(originalTable, "table")
  
  local ic = {}
  for k,v in pairs(originalTable) do
    ic[k] = tableUtil.keyForValue(uniqueTable, v)
  end
  return ic
end

-- function for returning indices of folds
--------------------------------------------
-- @param table       - table.           
function tableUtil.uniqueTableValues(table)
  typeUtil.assert(table, "table")
  
  local valset = tableUtil.frequencies(table)
  -- now convert that valset table keys into normal table with values
  local uniqueset={}
  local n=0
  for k,_ in pairs(valset) do
    n=n+1
    uniqueset[n]=k
  end
  
  ic = tableUtil.icForUnique(uniqueset, table)
  
  return uniqueset, ic
end

-- function for checking table contains element
--------------------------------------------
-- @param table       - table.
-- @param element     - value to check.
-- @return true / false     
function tableUtil.contains(table, element)
  typeUtil.assert(table, "table")

  for _, value in pairs(table) do
    if value == element then
      return true
    end
  end
  return false
end

function tableUtil.unique_keys(input)
  typeUtil.assert(input, "table")

  local keyset={}
  local n=0
  for k,v in pairs(input) do
    n=n+1
    keyset[n]=k
  end
  return keyset
end

-- function for returning frequencies of the elements in the table.
--------------------------------------------
-- @param table       - table.    
-- @return table where keys are values of original table and values are encountering frequencies
function tableUtil.frequencies(table)
  typeUtil.assert(table, "table")

  local valset = {}
  for _,v in pairs(table) do
    if (valset[v] == nil) then
      valset[v]=1
    else 
      valset[v] = valset[v] + 1
    end  
  end
  return valset
end

-- function for returning frequencies of the elements in the table.
--------------------------------------------
-- @param table       - table.  
-- @param indexes     - torch.ByteTensor({1,1,0,0,1,0,0,0,0,0}) - means result table will hold only rows 1,2 and 5    
-- @return table where keys are values of original table and values are encountering frequencies
function tableUtil.rows(table, indexes)
  typeUtil.assert(table, "table")
  typeUtil.assert(indexes, "ByteTensor")

  local filteredTable = {}
  local i = 1
  for k,v in pairs(table) do
    if (indexes[k] ~= 0) then
      filteredTable[i] = v
      i = i + 1
    end  
  end
  
  -- dataset case - update size function
  if (i-1 > 0 and torch.type(filteredTable[i-1]) == "function") then
    filteredTable[i-1] = nil
    filteredTable.size = function(self)
      return i-2
    end  
  end
  return filteredTable
end

-- function for flatteninng table.
-- for example torch.Tensor({1,2,3,45}):totable() will give {{1},{2},{3},{45}} and
-- tableUtil.flatten({{1},{2},{3},{45}}) will return {1,2,3,45}
--------------------------------------------
-- @param table       - table to be flattened.  
-- @return flattened table
function tableUtil.flatten(arr)
  local result = { }
  
  local function flatten(arr)
    for _, v in ipairs(arr) do
      if type(v) == "table" then
        flatten(v)
      else
        table.insert(result, v)
      end
    end
  end
  
  flatten(arr)
  return result
end

-- function for getting table size (count of keys)
--------------------------------------------
-- @param table - for which we will check size
-- @return count of table keys
function tableUtil.getSize(table)
  local count = 0
  for _ in pairs(table) do 
    count = count + 1 
  end
  return count
end

---- function for shallow copy of tables
----------------------------------------------
---- @param table - which will be copied
---- @param keyToExclude - key which should not be copied
---- @return table shallow copy
--function tableUtil.copyTableWithoutKey(table, keyToExclude)
--  local copy = {}
--  for k, v in pairs(table) do 
--    if (k ~= keyToExclude) then
--      copy[k] = v
--    end
--  end
--  return copy
--end

-- function for tables shallow copying 
--------------------------------------------
-- @param orig - which will be copied
-- @return table shallow copy
function tableUtil.shallowcopy(orig)
    local orig_type = type(orig)
    local copy
    if orig_type == 'table' then
        copy = {}
        for orig_key, orig_value in pairs(orig) do
            copy[orig_key] = orig_value
        end
    else -- number, string, boolean, etc
        copy = orig
    end
    return copy
end


function tableUtil.clone (t) -- deep-copy a table
    if type(t) ~= "table" then return t end
    local meta = getmetatable(t)
    local target = {}
    for k, v in pairs(t) do
        if type(v) == "table" then
            target[k] = tableUtil.clone(v)
        else
            target[k] = v
        end
    end
    setmetatable(target, meta)
    return target
end

function tableUtil.copy3(obj, seen)
  -- Handle non-tables and previously-seen tables.
  if type(obj) ~= 'table' then return obj end
  if seen and seen[obj] then return seen[obj] end

  -- New table; mark it as seen an copy recursively.
  local s = seen or {}
  local res = setmetatable({}, getmetatable(obj))
  s[obj] = res
  for k, v in pairs(obj) do res[copy3(k, s)] = copy3(v, s) end
  return res
end

-- function for deep tables print out 
--------------------------------------------
-- @param orig - which will be copied
-- @return table shallow copy
function tableUtil.printObj(obj, hierarchyLevel) 
  if (hierarchyLevel == nil) then
      hierarchyLevel = 0
  elseif (hierarchyLevel == 4) then
      return 0
  end
  
  local whitespace = ""
  for i=0,hierarchyLevel,1 do
      whitespace = whitespace .. "-"
  end
  io.write(whitespace)
  
  print(obj)
  if (type(obj) == "table") then
      for k,v in pairs(obj) do
          io.write(whitespace .. "-")
          if (type(v) == "table") then
              tableUtil.printObj(v, hierarchyLevel+1)
          else
              print(v)
          end         
      end
  else
      print(obj)
  end
end

return tableUtil