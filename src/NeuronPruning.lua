--require 'torch'
--require 'nn'

modelPingUtil = require('modelPingUtil')

local NeuronPruning, _ = torch.class('nn.NeuronPruning')
local tableUtil = require('tableUtil')

function NeuronPruning:__init(pruningLogic)
  self.pruningLogic = pruningLogic
end

function NeuronPruning:countPrunedNeurons(model)
  if (torch.typename(model):lower() ~= 'nn.sequential') then
    error('Neuron pruning can find pruned neurons count only for Seqential models')
  end  
  local prunedNeuronsCounts = {}
  local totalPrunedNeurons = 0
  for i = 1,model:size() do
    local layer = model:get(i)
    if (torch.typename(layer) == 'nn.NeuronPruningLayer') then
      local pruningMask = layer:getPruningMask()
      prunedNeuronsCounts[i] = pruningMask[pruningMask:eq(1)]:nElement()
      totalPrunedNeurons = totalPrunedNeurons + prunedNeuronsCounts[i] 
    end
  end
  local prunedNeurons = {}
  prunedNeurons.total = totalPrunedNeurons
  prunedNeurons.perLayer = prunedNeuronsCounts
  return prunedNeurons
end

-- It is better for user to call pruneNeurons / unpruneNeurons functions.
-- pruneUnpruneFlag - 0 - means not prune, 1 - means prune
--function NeuronPruning:pruneUnpruneNeurons(sensitivities, mlp, numberOfNeuronsToPrune, pruneUnpruneFlag)
--    error('pruneUnpruneNeurons is not implemented')
--end

-- Function for simple neurons pruning based on  
-- provided training criterion and dataset. 
--------------------------------------------
-- @param sensitivities-Table with rows {layerIndx, neuronIndx, sensitivity}
-- @param mlp         - Layered neural network. Where 'nn.NeuronPruningLayer' layers should exist. 
--                      Otherwise nothing will happen.
-- @param numberOfNeuronsToPrune -module:forward(input) how many neurons to prune. Default is 1.
-- @return nothing. mlp will be updated.  
local function pruneNeurons(self, sensitivities, mlp, numberOfNeuronsToPrune)
  self.pruningLogic:pruneUnpruneNeurons(sensitivities, mlp, numberOfNeuronsToPrune, 1)
end

-- Function for simple neurons unpruning based on  
-- provided training criterion and dataset. 
--------------------------------------------
-- @param sensitivities-Table with rows {layerIndx, neuronIndx, sensitivity}
-- @param mlp         - Layered neural network. Where 'nn.NeuronPruningLayer' layers should exist. 
--                      Otherwise nothing will happen.
-- @param numberOfNeuronsToPrune -module:forward(input) how many neurons to prune. Default is 1.
-- @return nothing. mlp will be updated.  
local function unpruneNeurons(self, sensitivities, mlp, numberOfNeuronsToPrune)
  self.pruningLogic:pruneUnpruneNeurons(sensitivities, mlp, numberOfNeuronsToPrune, 0)
end

-- Function for running provided neural network  
-- with provided training criterion on given dataset. 
--------------------------------------------
-- @param mlp         - Neural network
-- @param dataset     - Dataset to be used for classification.
-- @return y - outputs of neural network for provided dataset.  
local function classify(mlp, dataset)
  local y = {}
  for i = 1,#dataset do
    local example = dataset[i]
    local input = example[1]
    y[i] = mlp:forward(input)
  end
  return y
end

-- Function for calculating accuracy   
-- NB!!!! - works with ClassNLLCriterion or CrossEntropyCriterions - due to expected/actual labels calculations
--------------------------------------------
-- @param dataset     - Dataset to be used for classification.
-- @param mlp         - Neural network to be used
-- @param classNames  - Just table with one row of strings for class names
-- @return confusionMatrix, actualLabels table and accuracy 
local function calcConfusionLabelsAccError(dataset, mlp, classNames, criterion, labels2Targets)
  local confusionMat = optim.ConfusionMatrix(classNames)      -- new matrix
  confusionMat:zero()                                              -- reset matrix
  local predictedLabels = {}
  local criterionError = 0
  
  local X, y = dataUtil.tensorsFromDataset(dataset)
  if (y:size(2) == 1) then
    y = y:view(-1)
  end  
  local actualOut = mlp:forward(X)
  local criterionError, _ = criterion:forward(actualOut, y)
  local _, actualLabels = torch.max(actualOut, 2)
  local _, expectedLabels = torch.max(y, 2)
  confusionMat:batchAdd(actualLabels:view(-1), expectedLabels:view(-1))
  confusionMat:updateValids()
  return confusionMat, actualLabels, confusionMat.totalValid, criterionError
end

local function countTotalPrunableNeurons(mlp)
  local totalPrunableNeurons = 0
  for i = 1,mlp:size() do
    local layer = mlp:get(i)
    if (torch.typename(layer) == 'nn.NeuronPruningLayer') then
      local pruningMask = layer:getPruningMask()
      totalPrunableNeurons = totalPrunableNeurons + pruningMask:nElement()
    end
  end
  return totalPrunableNeurons
end

function NeuronPruning:runNeuronsPruning(config, dataset, mlp, criterion, opt)
  local totalPrunableNeurons = countTotalPrunableNeurons(mlp)
  local afterPruningRetrainEpochs = config.afterPruningRetrainEpochs
--  local retrainIterations = config.retrainIterations
  local maxFallbacks = config.maxFallbacks
  local errorWorsenForFallback = config.errorWorsenForFallback
  local numberOfNeuronsToPrune = config.numberOfNeuronsToPrune  
  local maxNodesToPrune = config.maxNodesToPrune
  local maxPruningIterations = config.maxPruningIterations
  local classNames = config.classNames
  
  local originalMaxIteration = opt.maxEpoch
--  opt.maxEpoch = config.retrainIterations
  
--  local _, _, highestAccuracy, minCriterionError = calcConfusionLabelsAccError(dataset, mlp, classNames, criterion, labels2Targets)
  local highestAccuracy = runningUtil.test(mlp, dataset, classNames, opt.batchSize).totalValid
  local mlpBackup = {}      -- this is model version to store
  local fallbackCounter = 0 -- how many times in a row we pruned neuron & then reverted it back due to bad accuracy
  local iter = 1
  local verbose = config.verbose -- can be 0, 1, 2
  local pruningLog = {}
  pruningLog.confusionMat = {}
  pruningLog.accuracy = {}
  pruningLog.criterionError = {}
  pruningLog.prunedNeurons = {}
  pruningLog.confusionMatRestored = {}
  pruningLog.accuracyRestored = {}
  pruningLog.criterionErrorRestored = {}
  while (maxNodesToPrune > NeuronPruning:countPrunedNeurons(mlp).total and iter <= maxPruningIterations) do
    
    -- backup network state
    if (fallbackCounter == 0) then
      mlpBackup = mlp:clone()
    end
    
    -- calculate sensitivities
    local sensitivities = self.pruningLogic:calcNeuronsSensitivities(mlp, criterion, dataset)
    if (#sensitivities == 0) then
      print('Nothing to prune!')
      break
    end  
    
    modelPingUtil.pingModelWriteFile(mlp, width_p, height_p, minX_p, minY_p, maxX_p, maxY_p, "trainedNetwork_beforePruning_"..tostring(iter)..".txt")
    
    -- prune neuron(-s) based on sensitivities
    pruneNeurons(self, sensitivities, mlp, numberOfNeuronsToPrune)
    
    modelPingUtil.pingModelWriteFile(mlp, width_p, height_p, minX_p, minY_p, maxX_p, maxY_p, "trainedNetwork_afterPruning_"..tostring(iter)..".txt")
    
    -- retrain after pruning
    opt.maxEpochs = afterPruningRetrainEpochs
    local parameters, gradParameters = mlp:getParameters()
    local confusion = optim.ConfusionMatrix(classNames)
--    train(mlp, dataset, parameters, gradParameters, confusion, opt)
    local confusionMat = runningUtil.train(mlp, criterion, dataset, classNames, opt)
    local accuracyAfterPruning = confusionMat.totalValid
    -- collect metrics & print them
--    local confusionMat, _, accuracyAfterPruning, criterionErrorAfterPruning = calcConfusionLabelsAccError(dataset, mlp, classNames, criterion, labels2Targets)
    local prunedNeuronsStats = NeuronPruning:countPrunedNeurons(mlp)
    if (verbose >= 2) then
      print(confusionMat)
      print('TotalPrunableNeurons=' ..totalPrunableNeurons.. ' PrunedNeuronsStats='..plPretty.write(prunedNeuronsStats, '') .. ' accuracy='..accuracyAfterPruning)      
    end
    if (verbose >= 1) then
      print('Accuracy beforePruning: '..highestAccuracy..' || after prunning: ' .. accuracyAfterPruning);
      print('Pruning iteration/totalPrunableNeurons/total removed nodes: '..iter..' / '..totalPrunableNeurons.. ' / '..plPretty.write(prunedNeuronsStats, ''));
    end
    
    -- gather some data for history
    pruningLog.confusionMat[iter] = confusionMat
    pruningLog.accuracy[iter] = accuracyAfterPruning
--    pruningLog.criterionError[iter] = criterionError
    pruningLog.prunedNeurons[iter] = prunedNeuronsStats
    pruningLog.confusionMatRestored[iter] = nil
    pruningLog.accuracyRestored[iter] = nil
    pruningLog.criterionErrorRestored[iter] = nil
    
    -- revert pruned neuron if performance becomes too bad
    if (accuracyAfterPruning ~= highestAccuracy and accuracyAfterPruning/highestAccuracy < 1 - errorWorsenForFallback ) then -- we allow some degradadion in accuracy/error
      print('Error becomes larger, reverting last removed neuron(-s) ...');
      unpruneNeurons(self, sensitivities, mlp, numberOfNeuronsToPrune)
      fallbackCounter = fallbackCounter + 1
    elseif (#sensitivities == 0) then
      print('Nothing to prune! Exiting.')
      break
    else 
      fallbackCounter = 0;
    end
            
    -- if too many unsuccessfull attempts were made to prune, restore last known good network state and exit.        
    if (fallbackCounter >= maxFallbacks) then
      print('Tried to prune without luck too many times. Restoring last good known network state...')
      mlp = mlpBackup:clone();
--      local confusionMat, _, accuracyAfterPruning, criterionErrorAfterPruning = calcConfusionLabelsAccError(dataset, mlp, classNames, criterion, labels2Targets)
      local accuracyAfterPruning = runningUtil.test(mlp, dataset, classNames, opt.batchSize).totalValid
      if (verbose >= 1) then
        local prunedNeuronsStats = NeuronPruning:countPrunedNeurons(mlp)
        print('Restored net Accuracy : ' ..accuracyAfterPruning ..' prunedNeurons='..plPretty.write(prunedNeuronsStats, ''));
      end
      -- gather some data for history
      pruningLog.confusionMatRestored[iter] = confusionMat
      pruningLog.accuracyRestored[iter] = accuracyAfterPruning
--      pruningLog.criterionErrorRestored[iter] = criterionErrorAfterPruning
      break
    end
    
--    -- update best known error/accuracy
--    if (minCriterionError > criterionErrorAfterPruning) then 
--        minCriterionError = criterionErrorAfterPruning;
--    end
    if (highestAccuracy < accuracyAfterPruning) then 
        highestAccuracy = accuracyAfterPruning;
    end
    
    print('Iteration '..iter..' finished.')
    iter = iter + 1
  end
  
  return pruningLog, mlp
end