--require 'torch'
--require 'nn'

local SensitivityPruningLogic = torch.class('nn.SensitivityPruningLogic')
local tableUtil = require('tableUtil')
local dataUtil = require('dataUtil')

-- Class implementing NeuronPruning module. It is basically a layer which nullifies (or set's to other mean value)
-- outputs from previous layer.
--------------------------------------------
-- @param zeroVal         - Value to which we should nullify pruned neurons (1's in prunning mask)
--                          It is 0 in case Tanh is used (as Tanh output belongs to [-1, +1]
--                          and it should be assigned to 0.5 in case     
function SensitivityPruningLogic:__init()
  return self
end

-- Function is small wrapper to calculate error on given data using provided criterion 
--------------------------------------------
-- @param mlp         - Layered neural network.
-- @param criterion   - Criterion used to train network. Required to test network to find errors - sensititivites.
-- @param dataset     - Neural Network input data set. Should have rows consisting of two parts input and output.
-- @return error - overall error over give dataset      
local function calcError(mlp, criterion, dataset)
  local X, y = dataUtil.tensorsFromDataset(dataset)
  local actualOut = mlp:forward(X)
  local criterionError, _ = criterion:forward(actualOut, y:view(-1))
  return criterionError
end

-- It is better for user to call pruneNeurons / unpruneNeurons functions.
-- pruneUnpruneFlag - 0 - means not prune, 1 - means prune
function SensitivityPruningLogic:pruneUnpruneNeurons(sensitivities, mlp, numberOfNeuronsToPrune, pruneUnpruneFlag)
  table.sort(sensitivities, function(a,b) return a.sensitivity < b.sensitivity end)
  for i = 1,numberOfNeuronsToPrune do
    if (i <= #sensitivities) then
      local sensitivity = sensitivities[i]
      local layerIndex = sensitivity.layerIndx
      local neuronIndex = sensitivity.neuronIndx
      
      local layer = mlp:get(layerIndex)
      local pruningMask = layer:getPruningMask()
      
      local flatMask = pruningMask:clone():resize(pruningMask:nElement())
      flatMask[neuronIndex] = pruneUnpruneFlag
      local newMask = flatMask:resize(pruningMask:size())
      layer:setPruningMask(newMask)
    else
      break
    end    
  end
end


-- Function for calculating sensitivities for given layered network using 
-- provided training criterion and dataset. 
--------------------------------------------
-- @param mlp         - Layered neural network.
-- @param criterion   - Criterion used to train network. Required to test network to find errors - sensititivites.
-- @param dataset     - Neural Network input data set. Should have rows consisting of two parts input and output.
-- @return sensitivities - table with rows {layerIndx, neuronIndx, sensitivity}      
function SensitivityPruningLogic:calcNeuronsSensitivities(mlp, criterion, dataset)
  local sensitivities = {}
  local sIndex = 1
  for i = 1,mlp:size() do
    local layer = mlp:get(i)
    if (torch.typename(layer) == 'nn.NeuronPruningLayer') then
      local pruningMask = layer:getPruningMask():byte()
      -- do not prune this layer - only one neuron has left
      if (pruningMask[pruningMask:eq(0)]:nElement() ~= 1) then
        -- let's find sensitivities for each neuron in this layer
        for n = 1,pruningMask:nElement() do 
          local flatMask = pruningMask:clone():resize(pruningMask:nElement())
          if (flatMask[n] == 0) then
            flatMask[n] = 1
            local newMask = flatMask:resize(pruningMask:size()):byte()
            layer:setPruningMask(newMask)
            
            local currentError = calcError(mlp,criterion,dataset)
            -- save stats
            sensitivities[sIndex] = {layerIndx = i, neuronIndx = n, sensitivity = currentError}
            sIndex = sIndex + 1
            --restore pruning layer state
            layer:setPruningMask(pruningMask)
          end
        end
      end  
    end
  end
  return sensitivities
end
