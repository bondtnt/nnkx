modelPingUtil = {}

local torch = require('torch')
require('tableUtil')

-- function for assessing model that accepts 2D inputs, produces 2D output.
-- So in the end linear 2D space is created and fed into model, output acquired, index of minimum class is acquired.
-- textual file is printed with lines containing: X,Y,modelMinOutIndex
--------------------------------------------
-- @param model                   - model.
-- @param width, height           - amount of points on X, Y axes.
-- @param minX, minY, maxX, maxY  - parameters for generating linear space.
-- @param fileName                - to print output to. If file exists it will be appended!
function modelPingUtil.pingModelWriteFile(model, width, height, minX, minY, maxX, maxY, fileName)
  local batchInputs = modelPingUtil.prepareBatchInputs(width, height, minX, minY, maxX, maxY)
  
  local preds = model:forward(batchInputs)
  local _, classIndx = torch.min(preds, 2)
  classIndx = classIndx - 1
  
  local file = io.open(fileName,"a")
  for row = 1, batchInputs:size(1) do
    file:write(batchInputs[row][1] .. ',' .. batchInputs[row][2] .. ',' .. tostring(classIndx[row][1]) .. '\n')
  end
  file:close()
end

-- function for assessing model that accepts 2D inputs, produces 2D output.
-- So in the end linear 2D space is created and fed into model, output acquired, index of minimum class is acquired.
-- textual file is printed with lines containing: X,Y,modelMinOutIndex
--------------------------------------------
-- @param rulegen, treeRoot       - rulesGenerator and tree it will run.
-- @param width, height           - amount of points on X, Y axes.
-- @param minX, minY, maxX, maxY  - parameters for generating linear space.
-- @param fileName                - to print output to. If file exists it will be appended!
function modelPingUtil.pingTreeWriteFile(rulegen, treeRoot, width, height, minX, minY, maxX, maxY, fileName)

  local batchInputs = modelPingUtil.prepareBatchInputs(width, height, minX, minY, maxX, maxY)
  
  print("PingTreeWriteFile:")
  local file = io.open(fileName,"a")
  for row = 1,batchInputs:size(1) do
    local input = batchInputs[row]
    print(input)
    local actualOutput = rulegen:runTree(treeRoot, input)
    actualOutput = actualOutput - 1
    actualOutput = actualOutput[1]
--    print(actualOutput)
    file:write(batchInputs[row][1] .. ',' .. batchInputs[row][2] .. ',' .. tostring(actualOutput) .. '\n')
  end
  file:close()
end

function modelPingUtil.prepareBatchInputs(width, height, minX, minY, maxX, maxY)
  local totalDX = maxX - minX
  local totalDY = maxY - minY
  local xStep = totalDX / width
  local yStep = totalDY / height
  local batchInputs = torch.Tensor(width * height, 2):zero()
  local row = 1
  for i = 1, width do
    for j = 1, height do
      local x = minX + xStep * i
      local y = minY + yStep * j
      batchInputs[row][1] = x
      batchInputs[row][2] = y
      row = row + 1  
    end
  end
  return batchInputs
end

return modelPingUtil