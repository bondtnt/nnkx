require 'torch'
require 'nn'
require 'nngraph'

local function main()
   
  x1 = nn.Linear(20,10)()
  mout = nn.Linear(10,1)(nn.Tanh()(nn.Linear(10,10)(nn.Tanh()(x1))))
  mlp = nn.gModule({x1},{mout})
  
  x = torch.rand(20)
  dx = torch.rand(1)
  mlp:updateOutput(x)
  mlp:updateGradInput(x,dx)
  mlp:accGradParameters(x,dx)
  
  -- draw graph (the forward graph, '.fg')
--  graph.dot(mlp.fg,'MLP') -- does not work without qt module
  graph.dot(mlp.fg, 'MLP', 'nngraphtest.svg')

end

main()

