-- script for running rules extraction experiment on IRIS dataset
require('mobdebug').start()

require 'graph'
require 'pl'
require 'paths'

plPretty = require('pl.pretty')
torch = require('torch')
optim = require('optim')
nn = require('nn')
csvUtil = require('csvUtil')
dataUtil = require('dataUtil')
tableUtil = require('tableUtil')
tensorUtil = require('tensorUtil')
activationModifierUtil = require('activationModifierUtil')
runningUtil = require('runningUtil')
typeUtil = require('typeUtil')
nngraph = require('nngraph')
mathUtil = require('mathUtil')

require 'NeuronPruning'
require 'NeuronPruningLayer'
require 'ActivationRecorder'
require 'ActivationModifier'
require 'SensitivityPruningLogic'
require 'N2PSPruningLogic'
require 'N2PSSensitivityLayeredPruningLogic'
require 'RulesGenerator'
require 'ActivationsDiscretizer'

torch.manualSeed(1)
local timeExec = sys.clock()

local opt = lapp[[
   -b, --batchSize     (default 10)          batch size
   -o, --optimization  (default "SGD")       optimization: SGD | LBFGS 
   -r, --learningRate  (default 0.05)        learning rate, for SGD only
   -m, --momentum      (default 0)           momentum, for SGD only
   -e, --maxEpochs     (default 10)          maximum nb of epochs per single run (fold)
   -i, --maxIter       (default 3)           maximum nb of iterations per batch, for LBFGS
   --coefL1            (default 0)           L1 penalty on the weights
   --coefL2            (default 0)           L2 penalty on the weights
   -t, --threads       (default 4)           number of threads
   --tZero             (default 1)           start averaging at t0 (ASGD only), in nb of epochs
]]

opt.batchSize = 10
opt.optimization = 'SGD'
opt.learningRate = 0.1
opt.maxEpochs = 100
opt.threads = 4
opt.momentum = 0.7 --0.01
--opt.coefL2 = 0.001

-- fix seed
torch.manualSeed(1)

-- threads
torch.setnumthreads(opt.threads)
print('<torch> set nb of threads to ' .. torch.getnumthreads())

-- use floats, for SGD
if opt.optimization == 'SGD' then
  torch.setdefaulttensortype('torch.FloatTensor')
end

-- batch size?
if opt.optimization == 'LBFGS' and opt.batchSize < 100 then
  error('LBFGS should not be used with small mini-batches; 1000 is recommended')
end

--require('profiler')
--profiler = newProfiler()
--profiler:start()

local time = os.date("*t")
local timestamp = ("%02d%02d%02d_%02d-%02d-%02d"):format(time.year, time.month, time.day, time.hour, time.min, time.sec)
local dataSetName = 'haberman-batched' --used for log files naming
print(dataSetName)
local filePath = '/home/andrey/works/LuaProjects/nnkx/data/haberman.csv'
local data = csvUtils.loadCsvData(filePath, false, ',', true)
--shuffle data tensor
local shuffleIndexes = torch.randperm(data:size(1)):long()
data = data:index(1, shuffleIndexes)


--  local X = data[{ {}, {1,-2} }]
--  local labels = data[{ {}, {-1} }]
--  local targetLabels, classesNumber = dataUtil.labelsColumnToMatrix(labels, -1, 1) -- (-1 + 1) for tanh, (0, +1) for sigmoid
--  local dataset = dataUtil.convertToDataset(X, targetLabels)


--local data = csvUtil.loadTest2dData()
local X, originalMin, originalMax = tensorUtil.scaleTensor(data[{ {}, {1,-2} }], -1, 1) -- we will be using tanh function
local XX, newMin, newMax = tensorUtil.scaleTensor(X, originalMin, originalMax) -- we will be using tanh function
--X = tensorUtil.scaleTensor(X, 0, 1) -- we will be using sigm function
local labels = data[{ {}, {-1} }]
labels = tensorUtil.scaleTensor(labels, 1, 2) -- we know there are 2 classes - mark them as 1 and 2
local targets, classesNumber, labels2Targets = dataUtil.labelsColumnToMatrix(labels, -1, 1) -- (-1 + 1) for tanh, (0, +1) for sigmoid
local labels2Targets = {}
for i = 1, labels:size(1) do
  labels2Targets[labels[i][1]] = targets[i]
end

---- in case of CrossEntropyCriterion we should use labels
--targets = labels
--for k, v in pairs(labels2Targets) do
--  labels2Targets[k] = k
--end
--local targets, classesNumber, labels2Targets = dataUtil.labelsColumnToMatrix(labels, -1, 1) -- (-1 + 1) for tanh, (0, +1) for sigmoid
targets = labels
--if (classesNumber == 2) then -- we will use single neuron in case of 2 classes
--  targets = targets[{ {}, {1} }]
--  for k, v in pairs(labels2Targets) do
--    labels2Targets[k] = v[1]
--  end
--end

-- convert class colummn into one hot matrix
local targets, classesNumber, labels2Targets = dataUtil.labelsColumnToMatrix(targets, -1, 1)
local dataset = dataUtil.convertToDataset(X, targets)

--for k, v in pairs(dataset) do -- here targets for CrossEntropyCriterion are 1D tensors holding 1 value. It should be not tensor but just number.
--  local row = v
--  if ('function' ~= type(row)) then
--    local trgt = row[2]
--    row[2] = trgt[1]
--  end
--end

classNames = {}
for i = 1, classesNumber do
  classNames[i] = ''..i
end

-- ANN parameters
local inputsSize = dataset[1][1]:size(1)
local outputsSize = classesNumber -- For ClassNNLCriterion - there must be classesNumber output neurons.
local hidden1Size = 10
--local hidden2Size = 15

local mlp = nn.Sequential()
mlp:add( nn.NeuronPruningLayer('tanh', inputsSize) )         -- Input layer pruning
mlp:add( nn.ActivationModifier() )          -- Used to perform activations discretization/clusterization
mlp:add( nn.ActivationRecorder(false) )     -- Used to record activation values
mlp:add( nn.Linear(inputsSize, hidden1Size) ) -- 10 input, 20 hidden units
mlp:add( nn.Tanh() )

--mlp:add( nn.NeuronPruningLayer('tanh', hidden1Size) )         -- Input layer pruning
--mlp:add( nn.ActivationModifier() )          -- Used to perform activations discretization/clusterization
--mlp:add( nn.ActivationRecorder(false) )     -- Used to record activation values
--mlp:add( nn.Linear(hidden1Size, hidden2Size) ) -- 10 input, 20 hidden units
--mlp:add( nn.Tanh() )

mlp:add( nn.NeuronPruningLayer('tanh', hidden1Size) )         -- Input layer pruning
mlp:add( nn.ActivationModifier() )          -- Used to perform activations discretization/clusterization
mlp:add( nn.ActivationRecorder(false) )     -- Used to record activation values
mlp:add( nn.Linear(hidden1Size, outputsSize) ) -- 1 output

--=====================================================
-- retrieve parameters and gradients
--parameters, gradParameters = mlp:getParameters()

-- verbose
print('<> using model:')
print(mlp)

----------------------------------------------------------------------
-- loss function: negative log-likelihood
--
mlp:add(nn.LogSoftMax())
mlp:add( nn.ActivationModifier() )          -- Used to perform activations discretization/clusterization
mlp:add( nn.ActivationRecorder(false) )     -- Used to record activation values

--=====================================================

local criterion = nn.MSECriterion()
--local criterion = nn.CrossEntropyCriterion()
--local criterion = nn.ClassNLLCriterion()

local verbose = true
local saveModels = false
local i = 1
local results = {}

-- log results to files
local trainLogger = optim.Logger(paths.concat('logs', dataSetName..'_'..timestamp..'train.log'))
local testLogger = optim.Logger(paths.concat('logs', dataSetName..'_'..timestamp..'test.log'))
local prunningTrainLogger = optim.Logger(paths.concat('logs', dataSetName..'_'..timestamp..'prunedTrain.log'))
local prunningTestLogger = optim.Logger(paths.concat('logs', dataSetName..'_'..timestamp..'prunedTest.log'))
local prunedNeuronsCount = optim.Logger(paths.concat('logs', dataSetName..'_'..timestamp..'prunedNeuronsCount.log'))
local rulesTrainLogger = optim.Logger(paths.concat('logs', dataSetName..'_'..timestamp..'rulesTrain.log'))
local rulesTestLogger = optim.Logger(paths.concat('logs', dataSetName..'_'..timestamp..'rulesTest.log'))
local rulesTestDepthLogger = optim.Logger(paths.concat('logs', dataSetName..'_'..timestamp..'rulesTestDepth.log'))
local rulesTestLeafsLogger = optim.Logger(paths.concat('logs', dataSetName..'_'..timestamp..'rulesTestLeafs.log'))

local xValTrainer = {
  criterion = criterion,
  classNames = classNames,
  mlp = mlp,
  saveModels = false, -- if true all trained models will be saved
  verbose = false     -- will print accuracy if true
}

local pruningConfig = {}
pruningConfig.afterPruningRetrainEpochs = 50
pruningConfig.maxFallbacks = 5
pruningConfig.errorWorsenForFallback = 0.05
pruningConfig.numberOfNeuronsToPrune = 1 -- Number of neurons to prune at single step
pruningConfig.maxNodesToPrune = hidden1Size +  inputsSize --hidden1Size + hidden2Size + inputsSize
pruningConfig.maxPruningIterations = inputsSize + hidden1Size + pruningConfig.maxFallbacks * 3 --inputsSize + hidden1Size + hidden2Size + pruningConfig.maxFallbacks * 3 --20
pruningConfig.classNames = classNames
--pruningConfig.retrainIterations = 10
pruningConfig.verbose = 1

local xvalIteration = 1
local xValLogs = {}
xValLogs.trainConfusion = {}
xValLogs.testConfusion = {}
xValLogs.pruningTrainConfusion = {}
xValLogs.pruningTestConfusion = {}
xValLogs.rulesTrainConfusion = {}
xValLogs.rulesTestConfusion = {}
xValLogs.pruningLogs = {}

xValTrainer.trainTest =
function(thiz, _model, _classNames, _trainData, _testData, _labels2Targets)

  -- calc / save stats
  local trainConf = runningUtil.train(_model, criterion, _trainData, _classNames, opt)
  local testConf = runningUtil.test(_model, _testData, _classNames, opt.batchSize)
  _model:training()
  trainConf:updateValids()
  testConf:updateValids()
  xValLogs.trainConfusion[xvalIteration] = trainConf
  xValLogs.testConfusion[xvalIteration] = testConf
  print('trainConf '.. trainConf:__tostring__())
  print('testConf '.. testConf:__tostring__())

  -- prune network
  local sensPruningLogic = nn.SensitivityPruningLogic()
  local neuronPruning1 = nn.NeuronPruning(sensPruningLogic)
  local sensPruningLog, prunedModel = neuronPruning1:runNeuronsPruning(pruningConfig, dataset, _model:clone(), criterion, tableUtil.clone(opt))
  _model = prunedModel

  -- save stats
  local pruningTrainConf = runningUtil.test(_model, _trainData, _classNames, opt.batchSize)
  local pruningTestConf = runningUtil.test(_model, _testData, _classNames, opt.batchSize)
  pruningTrainConf:updateValids()
  pruningTestConf:updateValids()
  print('pruningTrainConf '.. pruningTrainConf:__tostring__())
  print('pruningTestConf '.. pruningTestConf:__tostring__())
  xValLogs.pruningTrainConfusion[xvalIteration] = pruningTrainConf
  xValLogs.pruningTestConfusion[xvalIteration] = pruningTestConf
  xValLogs.pruningLogs[xvalIteration] = sensPruningLog

  -- clusterize activations
  activationModifierUtil.recordActivations(_model, _trainData)

  local confusionCalcFunc = function()
    return runningUtil.calculateConfusionMSE(_model, _trainData, _classNames, labels2Targets, nil)
  end

  local discretizer = nn.ActivationsDiscretizer()
  local confusionBeforeClusterization = confusionCalcFunc()
  discretizer:clusterize(_model, confusionCalcFunc, 1.95)
  local confusionClustered = confusionCalcFunc()


  -- extract rules
  local maxDepth = 100
  local rulegen = nn.RulesGenerator()
  rulegen:initialize(_model, _trainData)
  local inputClustersWithBoundaries = _model:get(2).clustersTable -- collect clusters with their boundaries for input layer (usually it is layer #2)
  local rootNode = rulegen:generateTree(inputClustersWithBoundaries, maxDepth)
  print('rootNode=>\n')
  rootNode:print()
  local treeStats = rootNode:treeStats()
  print(treeStats)

  local scaledTree = rulegen:scaleTree(rootNode, newMin, newMax, originalMin, originalMax)
  print('scaledTree=>\n')
  scaledTree:print()

  -- save extracted rules stats
  local rulesTrainConf = runningUtil.calculateConfusion3(rulegen, rootNode, _trainData, _classNames, labels2Targets, nil)
  local rulesTestConf = runningUtil.calculateConfusion3(rulegen, rootNode, _testData, _classNames, labels2Targets, nil)
  rulesTrainConf:updateValids()
  rulesTestConf:updateValids()
  print('rulesTrainConf '.. rulesTrainConf:__tostring__())
  print('rulesTestConf '.. rulesTestConf:__tostring__())
  xValLogs.rulesTrainConfusion[xvalIteration] = rulesTrainConf
  xValLogs.rulesTestConfusion[xvalIteration] = rulesTestConf

  xvalIteration = xvalIteration + 1

  trainLogger:add{['% mean class accuracy (train set)'] = trainConf.totalValid * 100}
  testLogger:add{['% mean class accuracy (test set)'] = testConf.totalValid * 100}
  prunningTrainLogger:add{['% mean class accuracy (pruned train set)'] = pruningTrainConf.totalValid * 100}
  prunningTestLogger:add{['% mean class accuracy (pruned test set)'] = pruningTestConf.totalValid * 100}
  prunedNeuronsCount:add{['% pruning Log (total pruned neurons counts)'] = sensPruningLog.prunedNeurons[#sensPruningLog.prunedNeurons].total}
  rulesTrainLogger:add{['% mean class accuracy (rules train set)'] = rulesTrainConf.totalValid * 100}
  rulesTestLogger:add{['% mean class accuracy (rules test set)'] = rulesTestConf.totalValid * 100}
  rulesTestDepthLogger:add{['% rules maxDepth'] = treeStats.maxDepth}
  rulesTestLeafsLogger:add{['% rules count'] = treeStats.maxDepth}

  if true then
    trainLogger:style{['% mean class accuracy (train set)'] = '-'}
    testLogger:style{['% mean class accuracy (test set)'] = '+'}
    prunningTrainLogger:style{['% mean class accuracy (pruned train set)'] = '-'}
    prunningTestLogger:style{['% mean class accuracy (pruned test set)'] = '+'}
    prunedNeuronsCount:style{['% pruning Log (pruned neurons counts)'] = '-' }
    rulesTrainLogger:style{['% mean class accuracy (rules train set)'] = '-'}
    rulesTestLogger:style{['% mean class accuracy (rules test set)'] = '+'}
    rulesTestDepthLogger:style{['% mean class accuracy (rules test set)'] = '-'}
    rulesTestLeafsLogger:style{['% mean class accuracy (rules test set)'] = '-'}

    trainLogger:plot()
    testLogger:plot()
    prunningTrainLogger:plot()
    prunningTestLogger:plot()
    prunedNeuronsCount:plot()
    rulesTrainLogger:plot()
    rulesTestLogger:plot()
    rulesTestDepthLogger:plot()
    rulesTestLeafsLogger:plot()
  end
end

--profiler.start('expiris-profile.out')

runningUtil.runXVal(dataset, labels, xValTrainer, 10, 3)

local trainConfTensor = tensorUtil.buildTensorFromConfusionsTable(xValLogs.trainConfusion, function(confM) return confM.totalValid end)
local testConfTensor = tensorUtil.buildTensorFromConfusionsTable(xValLogs.testConfusion, function(confM) return confM.totalValid end)
local pruningTrainConfTensor = tensorUtil.buildTensorFromConfusionsTable(xValLogs.pruningTrainConfusion, function(confM) return confM.totalValid end)
local pruningTestConfTensor = tensorUtil.buildTensorFromConfusionsTable(xValLogs.pruningTestConfusion, function(confM) return confM.totalValid end)
local treeTrainConfTensor = tensorUtil.buildTensorFromConfusionsTable(xValLogs.rulesTrainConfusion, function(confM) return confM.totalValid end)
local treeTestConfTensor = tensorUtil.buildTensorFromConfusionsTable(xValLogs.rulesTestConfusion, function(confM) return confM.totalValid end)


local function printTensor(t, str)
  print(str..' mean='..t:mean()..'; std='..t:std()..';')
end

printTensor(trainConfTensor, 'mlp train')
printTensor(testConfTensor, 'mlp test')
printTensor(pruningTrainConfTensor, 'pruning train')
printTensor(pruningTestConfTensor, 'pruning test' )
printTensor(treeTrainConfTensor, 'tree train')
printTensor(treeTestConfTensor, 'tree test' )


timeExec = sys.clock() - timeExec
print("Time to execute = " .. (timeExec*1000) .. 'ms')


--profiler:stop()
--local outfile = io.open( "expirisBatch-profile.txt", "w+" )
--profiler:report( outfile )
--outfile:close()
