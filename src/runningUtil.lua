runningUtil = {}

local torch = require('torch')
plPretty = require('pl.pretty')
optim = require('optim')
--local tableUtil = require('tableUtil')


-- function for returning key (index) of the element in the table.
--------------------------------------------
-- @param dataset - data matrix - Torch2D.tensor. It must hold in last column labels!
-- @param labels  - just a convenience matter - labels vector (table) is used to split data into kFolds 
-- @param xValRunner - object that have methods :train(), :test(), :collectStatistics() and :clone().
-- @param foldsN  - number of folds - default is 10.
-- @param foldsRepeats - usually 3 so that 10 folds x 3 repeats give 30 experiments.    
-- @return array of results with cloned trainers each holding trained model and additional fields with statistics.
function runningUtil.runXVal( dataset, y, xValRunner, foldsN, foldsRepeats)
  local results = {}
  local i = 0
  local mlp = xValRunner.mlp
  local classNames = xValRunner.classNames
  local saveModels = xValRunner.saveModels
  local verbose = xValRunner.verbose
  
  local totalRuns = foldsN * foldsRepeats
  for fR = 1,foldsRepeats do
    local C = dataUtil.cvpartitionKFold(tableUtil.flatten(y:totable()), foldsN)
    for fN = 1,foldsN do
      i = i + 1
      local trainIndexes = C.getTrainFold(fN)
      local testIndexes = C.getTestFold(fN)
      local trainData = tableUtil.rows(dataset, trainIndexes) -- C.getTrainFold(fN) -gives torch.Tensor(0)
      local testData = tableUtil.rows(dataset, testIndexes)
      
      local mlpCopy = mlp:clone()
      xValRunner:trainTest(mlpCopy, classNames, trainData, testData)
      
      local completePercent = ((fR-1) * foldsN + fN) * 100 / totalRuns
      local completePercentStr = string.format("%i", completePercent)
      print('Xval '..completePercentStr..'%  finished repeat# '..fR..' of '..foldsRepeats..' fold#'..fN..' of '..foldsN)
    end  
  end
  return results
end

-- function for returning key (index) of the element in the table.
--------------------------------------------
-- @param testTrainRunner - object that have methods :train(), :test()
-- @param totalRuns - number, usually >= 30 (it is # of experiments)    
-- @return array of results with cloned trainers each holding trained model and additional fields with statistics.
--function runningUtil.runTestTrain(testTrainRunner, totalRuns)
function runningUtil.runTestTrain(trainTestRunner, totalRuns)

  local results = {}
  local mlp = trainTestRunner.mlp
  local classNames = trainTestRunner.classNames
  local testData = trainTestRunner.testData
  local trainData = trainTestRunner.trainData
  local saveModels = trainTestRunner.saveModels
  local verbose = trainTestRunner.verbose
  
  for i = 1,totalRuns do
    local mlpCopy = mlp:clone()
    
    trainTestRunner:trainTest(mlpCopy, classNames, trainData, testData)
    
    local completePercent = i * 100 / totalRuns
    local completePercentStr = string.format("%i", completePercent)
    print('TestTrain '..completePercentStr..'%  finished repeat# '..i..' of '..totalRuns)
    
  end
  return results
end

-- function for copying table.
--------------------------------------------
-- @param table       - table.    
-- @return deep copy of given table
function runningUtil.deepcopy(obj)
  return runningUtil.deepcopyX(obj, nil)
end

-- function for copying table.
--------------------------------------------
-- @param table       - table.
-- @param seen        - have obj been seen    
-- @return deep copy of given table
function runningUtil.deepcopyX(obj, seen)
  -- Handle non-tables and previously-seen tables.
  if type(obj) ~= 'table' then 
    local objType = torch.type(obj)
    if (string.find(objType, "Tensor") ~= nil) then
      return obj:clone()
    end
    return obj 
  end
  if seen and seen[obj] then 
    return seen[obj] 
  end

  -- New table; mark it as seen an copy recursively.
  local s = seen or {}
  local res = setmetatable({}, getmetatable(obj))
  s[obj] = res
  for k, v in pairs(obj) do
    local keyCopy = runningUtil.deepcopyX(k, s) 
    res[keyCopy] = runningUtil.deepcopyX(v, s) 
  end
  return res
end

-- function for applying function to specific mlp layers.
--------------------------------------------
-- @param container       - mlp.
-- @param layerType       - string containing full name of layer (like "nn.ActivationsModifier")    
-- @param f               - function to be applied to specified layers types
function runningUtil.applyToFilteredLayers(container, layerType, f) 
  for i = 1,container:size() do
    if (torch.type(container:get(i)) == layerType) then
      f(container:get(i))
    end
  end
end

-- function for calculation of confusion matrix.
--function runningUtil.calculateConfusion(mlp, dataset, classNames)
--  local confusion = optim.ConfusionMatrix(classNames)
--  for i = 1,#dataset do
--    local example = dataset[i]
--    local input = example[1]
--    local target = example[2]
--    local output = mlp:forward(input)
--    confusion:add(output, target)
--  end
--  confusion:updateValids()
--  return confusion
--end

-- function that returns predicted class label. Inputs are predicted target tensor and map of labels 2 target tensors.
function runningUtil.findClosestLabelTarget(t, labels2Targets)

  local min = labels2Targets[1]:min()
  local max = labels2Targets[1]:max()

  local label = nil
  local target = nil
  local minDistance = math.huge
  
  for k, v in pairs(labels2Targets) do
    local tmpTarget = labels2Targets[k]
    if type(tmpTarget) == 'number' then
      tmpTarget = torch.Tensor({tmpTarget})
    end
    if (t:size(1) == 1 and tmpTarget:size(1) > 1) then -- we have ClassNLLCriterion or similar - let's transform this 1D 1x1 vector into one hot vector
      local indx = torch.LongTensor{t:squeeze()}:view(-1,1)
      local oneHot = torch.Tensor(1,tmpTarget:size(1)):fill(min)
      oneHot:scatter(2, indx, max)
      t = oneHot  
      if (tmpTarget:size():size() == 1) then
        t = t:squeeze()
      end  
    end
    local tmpDistance = t:dist(tmpTarget)
    if tmpDistance < minDistance then
      minDistance = tmpDistance
      label = k
      target = tmpTarget
    end    
  end
  
  return label, target
end

-- function calculates confusionMatrix 
-- NB!!!! - works with ClassNLLCriterion or CrossEntropyCriterions - due to expected/actual labels calculations
-- @param optim - optimizer
-- @param mlp - basically nn.Sequential container - we call it mlp - MultiLayeredPerceptron
-- @param data - data set table with tables as rows holding input and targetOutput data
-- @param classNames - just to initialize confusionMatrix
-- @param labels2Targets - table holding labels (numeric) as keys and target vectors as corresponding targets (used to find labels) 
-- @param confusionMat - in case it is initialized it will be updated and returned. If it is nil - new one created.
function runningUtil.calculateConfusion2(mlp, data, classNames, labels2Targets, confusionMat)
  if confusionMat == nil then
    confusionMat = optim.ConfusionMatrix(classNames)
  end
  local X, y = dataUtil.tensorsFromDataset(data)
  local actualOut = mlp:forward(X)
  local _, actualLabel = torch.max(actualOut, 2)
  local expectedLabel = y
  confusionMat:batchAdd(actualLabel:view(-1), expectedLabel:view(-1))
  confusionMat:updateValids()
  return confusionMat
end

-- function calculates confusionMatrix 
-- NB!!!! - works with ClassNLLCriterion or CrossEntropyCriterions - due to expected/actual labels calculations
-- @param optim - optimizer
-- @param mlp - basically nn.Sequential container - we call it mlp - MultiLayeredPerceptron
-- @param data - data set table with tables as rows holding input and targetOutput data
-- @param classNames - just to initialize confusionMatrix
-- @param labels2Targets - table holding labels (numeric) as keys and target vectors as corresponding targets (used to find labels) 
-- @param confusionMat - in case it is initialized it will be updated and returned. If it is nil - new one created.
function runningUtil.calculateConfusionMSE(mlp, data, classNames, labels2Targets, confusionMat)
  if confusionMat == nil then
    confusionMat = optim.ConfusionMatrix(classNames)
  end
  local X, y = dataUtil.tensorsFromDataset(data)
  local _, expectedLabel = torch.max(y, 2)
  local actualOut = mlp:forward(X)
  local _, actualLabel = torch.max(actualOut, 2)
  confusionMat:batchAdd(actualLabel, expectedLabel)
  confusionMat:updateValids()
  return confusionMat
end

-- function calculates confusionMatrix
-- @param optim - optimizer
-- @param mlp - basically nn.Sequential container - we call it mlp - MultiLayeredPerceptron
-- @param data - data set table with tables as rows holding input and targetOutput data
-- @param classNames - just to initialize confusionMatrix
-- @param labels2Targets - table holding labels (numeric) as keys and target vectors as corresponding targets (used to find labels) 
-- @param confusionMat - in case it is initialized it will be updated and returned. If it is nil - new one created.
function runningUtil.calculateConfusion3(rulegen, treeRoot, data, classNames, labels2Targets, confusionMat)
  if confusionMat == nil then
    confusionMat = optim.ConfusionMatrix(classNames)
  end
  for i = 1,#data do
    local example = data[i]
    local input = example[1]
    local expectedOutput = example[2]
    local actualOutput = rulegen:runTree(treeRoot, input)
    local actualLabel, _ = runningUtil.findClosestLabelTarget(actualOutput, labels2Targets)
    local expectedLabel = runningUtil.findClosestLabelTarget(expectedOutput, labels2Targets)
    confusionMat:add(actualLabel, expectedLabel)
--    print('confusion... expectedOutput='..expectedOutput[1]..','..expectedOutput[2]..' actualOutput='..actualOutput[1]..','..actualOutput[2]..' expectedLabel='..expectedLabel..' actualLabel='..actualLabel) 
  end
  return confusionMat
end

function createBatch(dataset, batchSize, datasetIndex, inputSize, targetSize)
  -- create mini batch
    local inputs = torch.Tensor(batchSize, inputSize)
    local targets = torch.Tensor(batchSize, targetSize)
    if (targetSize == 1) then
      targets = targets:squeeze() -- if we have 1xbatchSize 2D tensor - ClassNNLCriterion heavily complains, thus we convert it to 1D tensor 
    end
    local k = 1
    for i = datasetIndex, math.min(datasetIndex + batchSize-1, dataset:size()) do
      -- load new sample
      local input = dataset[i][1]:clone()
      local target = dataset[i][2]:clone()
      if (targetSize == 1) then 
        target = target:squeeze() 
      end
      inputs[k] = input
      targets[k] = target
      k = k + 1
    end
      
    if (k-1 < batchSize) then -- trunc input/target tensors
      inputs = inputs:narrow(1, 1, k-1)
      targets = targets:narrow(1, 1, k-1)
    end
    return inputs, targets
end

local function shuffleTable( t )
    local rand = math.random 
    assert( t, "shuffleTable() expected a table, got nil" )
    local iterations = #t
    local j
    
    for i = iterations, 2, -1 do
        j = rand(i)
        t[i], t[j] = t[j], t[i]
    end
end

--train function
function runningUtil.train(_model, criterion, _trainData, classNames, opt)
  _model:training()
--  local parameters, gradParameters = _model:getParameters()
  local confusion = optim.ConfusionMatrix(classNames)
  local inputSize = _trainData[1][1]:size(1)
  local targetSize = _trainData[1][2]:size(1)
  local sgdState = nil
  local lbfgsState = nil
  local asgdState = nil
  local adamState = nil
--  local time = sys.clock()

  for epoch = 1, opt.maxEpochs do
    local parameters, gradParameters = _model:getParameters()
    shuffleTable(_trainData)
    local loss = 0
    -- do one epoch
--      print('<trainer> on training set:')
--      print("<trainer> online epoch # " .. epoch .. ' [batchSize = ' .. opt.batchSize .. ']')
    for t = 1,_trainData:size(),opt.batchSize do
      -- create mini batch
      local batchInputs, batchLabels = createBatch(_trainData, opt.batchSize, t, inputSize, targetSize)

      -- create closure to evaluate f(X) and df/dX
      local feval = function(x)
--         -- just in case:
--         collectgarbage()

         -- get new parameters
--         if x ~= parameters then
--            parameters:copy(x)
--         end

         -- reset gradients
         gradParameters:zero()

         -- evaluate function for complete mini batch
         local outputs = _model:forward(batchInputs)
         local loss = criterion:forward(outputs, batchLabels)
         -- estimate df/dW
         local df_do = criterion:backward(outputs, batchLabels)
         _model:backward(batchInputs, df_do)

         -- penalties (L1 and L2):
         if opt.coefL1 ~= 0 or opt.coefL2 ~= 0 then
            -- locals:
            local norm,sign= torch.norm,torch.sign

            -- Loss:
            loss = loss + opt.coefL1 * norm(parameters,1)
            loss = loss + opt.coefL2 * norm(parameters,2)^2/2

            -- Gradients:
            gradParameters:add( sign(parameters):mul(opt.coefL1) + parameters:clone():mul(opt.coefL2) )
         end

--         -- update confusion
--         for i = 1,targets:size(1) do
--            confusion:add(outputs[i], targets[i]:squeeze())
--         end

         -- return f and df/dX
         return loss,gradParameters
      end
      
      -- optimize on current mini-batch
      if opt.optimization == 'LBFGS' then

         -- Perform LBFGS step:
         lbfgsState = lbfgsState or {
            maxIter = opt.maxIter,
            lineSearch = optim.lswolfe
         }
         local _,fs = optim.lbfgs(feval, parameters, lbfgsState)
         loss = loss + fs[1]
       
         -- disp report:
         print('LBFGS step')
         print(' - progress in batch: ' .. t .. '/' .. dataset:size())
         print(' - nb of iterations: ' .. lbfgsState.nIter)
         print(' - nb of function evalutions: ' .. lbfgsState.funcEval)

      elseif opt.optimization == 'SGD' then

         -- Perform SGD step:
         sgdState = sgdState or {
            learningRate = opt.learningRate,
            momentum = opt.momentum,
            learningRateDecay = opt.learningRateDecay,
            weightDecay = opt.weightDecay
         }
         local _, fs = optim.sgd(feval, parameters, sgdState)
         loss = loss + fs[1]
         -- disp progress
--           xlua.progress(t, _trainData:size())
      elseif opt.optimization == 'ASGD' then

         -- Perform ASGD step:
         asgdState = asgdState or {
            eta0 = opt.learningRate,
            t0 = _trainData:size() * opt.tZero
         }
         local _, fs, avg= optim.asgd(feval, parameters, asgdState)
         loss = loss + fs[1]
         -- disp progress
--           xlua.progress(t, _trainData:size())
      elseif opt.optimization == 'ADAM' then

         -- Perform ADAM step:
         adamState = adamState or {
            learningRate = opt.learningRate,
            learningRateDecay = opt.learningRateDecay,
            beta1 = opt.adamBeta1,
            beta2 = opt.adamBeta2,
            epsilon = opt.adamEpsilon
         }
         local _, fs, avg= optim.adam(feval, parameters, asgdState)
         loss = loss + fs[1]
         -- disp progress
--           xlua.progress(t, _trainData:size())
      else
         error('unknown optimization method')
      end
--      print('Epoch='..epoch..' loss='..loss)
    end
    loss = loss / _trainData:size()  
--    print('Epoch='..epoch..' final loss='..loss ..' dataSize=' .. _trainData:size() )
  end
  
  --time taken
--  time = sys.clock() - time
--  time = time / dataset:size()
--  print("<trainer> time to learn 1 sample = " .. (time*1000)
  return runningUtil.test(_model, _trainData, classNames, opt.batchSize)
--  return confusion
end


-- test function
function runningUtil.test(model, testData, classNames, batchSize)
   model:evaluate()
   -- local vars
   local time = sys.clock()
   local confusion = optim.ConfusionMatrix(classNames)
   local inputSize = testData[1][1]:size(1)
   local targetSize = testData[1][2]:size(1) 

   -- test over given dataset
--   print('<trainer> on testing Set:')
   for t = 1,testData:size(),batchSize do
      -- disp progress
--      xlua.progress(t, testData:size())

      -- create mini batch
      local batchInputsX, batchLabelsX = createBatch(testData, batchSize, t, inputSize, targetSize)
      local batchInputs = torch.Tensor(batchSize, inputSize)
      local batchLabels = torch.Tensor(batchSize, targetSize)
      local k = 1
      for i = t, math.min(t+batchSize-1,testData:size()) do
        -- load new sample
        local input = testData[i][1]:clone()
        local target = testData[i][2]:clone()
        if (targetSize == 1) then 
          target = target:squeeze() 
        end
        batchInputs[k] = input
        batchLabels[k] = target
        k = k + 1
      end
      
      if (k-1 < batchSize) then -- trunc input/target tensors
        batchInputs = batchInputs:narrow(1, 1, k-1)
        batchLabels = batchLabels:narrow(1, 1, k-1)
      end
      
      
      -- test samples
      local preds = model:forward(batchInputs)

      if (#classNames == 2 and preds:size(2) == 1) then 
      -- Case when for binary problem single output neuron is used.
         -- Let's transform targets and preds to 2 column tensors.
         -- no let's make a shaky assumption and find min/max
         -- it might be that single batch will not contain all labels 
         local min = batchLabels:min() 
         local max = batchLabels:max()
         batchLabels, _, _ = tensorUtil.scaleTensor(batchLabels, 1, 2)
         preds:apply(function(x) 
            if (x > 1) then 
              return 1 
            elseif (x < 0) then 
              return 0 
            else 
              return x 
            end 
          end)
         preds, _, _ = tensorUtil.scaleTensor(preds:round(), 1, 2)
         
         batchLabels, _, _ = dataUtil.labelsColumnToMatrix(batchLabels, min, max)
         preds, _, _ = dataUtil.labelsColumnToMatrix(preds, min, max)
      end
      -- confusion:
      for i = 1,preds:size(1) do
         confusion:add(preds[i], batchLabels[i]:squeeze())
      end
   end

--   -- timing
--   time = sys.clock() - time
--   time = time / testData:size()
--   print("<trainer> time to test 1 sample = " .. (time*1000) .. 'ms')

   -- print confusion matrix
--   print(confusion)
--   testLogger:add{['% mean class accuracy (test set)'] = confusion.totalValid * 100}
--   confusion:zero()
  confusion:updateValids()
  return confusion;
end

return runningUtil