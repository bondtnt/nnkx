torch = require('torch')
tensorUtil = require('tensorUtil')
local precision = 1e-5

local ActivationModifier, parent = torch.class('nn.ActivationModifier', 'nn.Module')
--local tableUtil = require('tableUtil')

-- Class extends nn module - this is essentially a layer which allows to modify outputs on
-- forward pass. Backward pass is not modified.
-- This layer is used to modify activations for discretization and clusterization of activation values.
-- These are essential steps for rules extraction.
-- Activations modifications is performed by injected modifier functions. 
-- Thus activations modification strategy can be easily changed.    
--------------------------------------------
function ActivationModifier:__init(recordFlag)
  parent.__init(self)
  self.modifierFunction = ActivationModifier.emptyModifierFunction
  return self
end

--------------------------------------------
function ActivationModifier:reset()
  self.modifierFunction = ActivationModifier.emptyModifierFunction
end

function ActivationModifier:updateOutput(input)
  local modifiedInput = self.modifierFunction(input)
  self.output:resizeAs(modifiedInput):copy(modifiedInput)
  return self.output
end

function ActivationModifier:updateGradInput(input, gradOutput)
  self.gradInput:resizeAs(gradOutput):copy(gradOutput)
  return self.gradInput
end

function ActivationModifier.emptyModifierFunction(input)
  return input
end

-- function just rounds input matrix - doing discretization of continuous inputs
function ActivationModifier.afterComaDigitsDiscretizerFunc(afterComaDigits) 
  return function(input)
    local inputCopy = input:clone() 
    if (afterComaDigits == 0) then
      return inputCopy:round()
    else  
      inputCopy:mul(10^afterComaDigits):round():div(10^afterComaDigits)
      return inputCopy
    end
  end
end

-- function which modifies input vector according to given tableOfIntervals.
-- For each neuron (for each input value in input vector) it uses appropriate intervals table and finds
-- matching interval (which holds given input (activation) or to which given input is located closer), then function 
-- replaces activation value with cluster center (cluster formed by interval - cluster center is interval mean) 
-- @param tableOfIntervals - table where for each neuron (indexed from 1 to totalNumberOfNeuronsInLayer 
-- (in case you have 2D or 3D input tensor - it is flattened))
-- @result tableOfClusters. Where each cluster is 2D tensor where first column is cluster center and second column is cluster upper boundary.
function ActivationModifier.clusterizerFunc(tableOfIntervals, min, max)
  local tableOfClusters = ActivationModifier.buildClustersTable(tableOfIntervals, min, max)
  return function(input)
    local flatInput = input:view(input:nElement())
    local modifiedFlatInput = flatInput:clone()
    local dim = modifiedFlatInput:size():size()
    for i = 1,input:nElement() do
      local clusters = tableOfClusters[i]
      if (dim == 1) then
        modifiedFlatInput[i] = ActivationModifier.getClusterCenter(flatInput[i], clusters)
      elseif (dim == 2) then
        modifiedFlatInput[i][1] = ActivationModifier.getClusterCenter(flatInput[i][1], clusters)
      else
        error('Flattened tensor should be either vector or 2D tensor having single column! Instead it have dim = ' .. dim)
      end  
      
    end
    return modifiedFlatInput:resizeAs(input)     
  end
end

-- function which modifies input vector according to given tableOfClusters.
-- For each neuron (for each input value in input vector) it uses appropriate clusters table and finds
-- matching cluster, then function replaces activation value with cluster center 
-- (1st column in cluster 2 column tensor, 2nd column is cluster upper bound) 
-- @param tableOfClusters - table where for each neuron (indexed from 1 to totalNumberOfNeuronsInLayer 
-- (in case you have 2D or 3D input tensor - it is flattened))
-- @result function which will modify input tensor values according to tableOfClusters.
function ActivationModifier.clusterizerFuncFromClusters(tableOfClusters, min, max)
  return function(input)
    local modifiedInput = input:clone()
    local dim = modifiedInput:size():size()
    
    if (dim == 1) then
      modifiedInput:resize(1, modifiedInput:size(1)) -- make this 1D tensor a 2D tensor with single row
    elseif (dim < 1 or dim > 2) then
      error('Input tensor should be either 1D vector or 2D tensor! Instead it have dimensionality = ' .. dim)
    end
    
--    if (totalTime == nil) then
--      totalTime = 0
--    end
--    local timeExec = sys.clock()
    
    for neuronNum = 1, modifiedInput:size(2) do
      local neuronClusters = tableOfClusters[neuronNum]
      for rowNum = 1, modifiedInput:size(1) do
        modifiedInput[rowNum][neuronNum] = ActivationModifier.getClusterCenter(modifiedInput[rowNum][neuronNum], neuronClusters)
      end
    end
    
--    totalTime = totalTime + sys.clock() - timeExec
--    print("ActivationModifier.clusterizerFuncFromClusters -centers: totalTime = " .. (totalTime*1000) .. 'ms')
    if (dim == 1) then
      modifiedInput:resizeAs(input)
    end
    return modifiedInput     
  end
end

function ActivationModifier.getClusterCenter(value, clusters)
  
  local lowerBound = math.huge * -1
  for i = 1, clusters:size()[1] do
    local upperBound = clusters[i][2] 
    if (value > lowerBound and value <= upperBound) then
      return clusters[i][1]
    end 
    lowerBound = upperBound 
  end
  return clusters[clusters:size(1)][1]

--  local lowerBound = math.huge * -1
--  for i = 1, clusters:size()[1] do
--    local upperBound = clusters[i][1] 
--    if (value > lowerBound and value <= upperBound) then
--      return clusters[i][2]
--    end 
--    lowerBound = upperBound 
--  end
--  return clusters[clusters:size(1)][2]

--  local clustersBoundaries = clusters[{{},{1}}]
--  if (value <= clusters[1][1]) then
--    return clusters[1][2]
--  elseif (value > clusters[clusters:size(1)][1]) then
--    return clusters[clusters:size(1)][2]
--  else
--  print('value = '..value)
--  print('clusters = '.. clusters)
--    local r = torch.range(1, clusters:size(1))
--    local indexesOfLargerBounds = clustersBoundaries:gt(value)
--    local upperBoundIndex = r[indexesOfLargerBounds][1]
--    return clusters[upperBoundIndex-1][2]
--  end
--  error('Was unable to find clusters center using value='..value..' and clusters='..clusters)
end

-- function transforms intervals table (containing intervals tensors for each neuron)
-- @param tableOfIntervals: 
-- can be seen as: 
-- local intervalsForNeuron1 = torch.Tensor({{0.0, 0.1}, {0.4, 0.6}, {0.9, 1.0}}) -- here we see 3 intervals found during known activations merging
-- local intervalsForNeuron2 = ...
-- local tableOfIntervals = {}
-- tableOfIntervals[1] = intervalsForNeuron1
-- tableOfIntervals[2] = intervalsForNeuron2
-- this is structure of input - tableOfIntervals;
---------------------------------------------------
-- @result - same as above, but tensorts are a bit different
-- column 1 - denotes center of cluster, and column 2 denotes upper bound of cluster.
-- Such data strcuture allows to find cluster to which belongs input (using bounds of clusters)
-- and replace it with clsuter center value (corresponding value in first column)
function ActivationModifier.buildClustersTable(tableOfIntervals, min, max)
  local clustersTable = {}
  for neuronIndex, intervals in pairs(tableOfIntervals) do
    local clusters = ActivationModifier.buildClustersFromIntervals(intervals, min, max)
    clustersTable[neuronIndex] = clusters
  end
  return clustersTable;
end

-- @param intervals - 2D tensor with 2 columns denoting beginnning and end of interval
-- @result - 2D tensor, where 1st column is cluster center, and 2nd column is cluster upper boundary 
function ActivationModifier.buildClustersFromIntervals(intervals, min, max)
  local clusters = torch.DoubleTensor(intervals:size()):fill(0)
  local size = intervals:size()[1]
  local lowerClusterBoundary = min
  for i=1,size-1 do
    local upperClusterBoundary = intervals[i][2] + torch.abs((intervals[i][2] - intervals[i+1][1]) / 2)
    local clusterCenter = lowerClusterBoundary + torch.abs((lowerClusterBoundary - upperClusterBoundary) / 2)
    clusters[i][1] = clusterCenter;
    clusters[i][2] = upperClusterBoundary;
    lowerClusterBoundary = upperClusterBoundary
  end
  local clusterCenter = lowerClusterBoundary + torch.abs((lowerClusterBoundary - max) / 2)
  clusters[size][1] = clusterCenter;
  clusters[size][2] = max
  return clusters;
end

function ActivationModifier:buildAndSetClusterizerModifierFunction(activations)
  typeUtil.assert(activations, "Tensor")
  
  local min = activations:min()
  local max = activations:max()
  local clustersTable = {}
  for neuronIndx = 1,activations:size()[2] do
    local singleNeuronActivations = activations[{{}, {neuronIndx}}]
    singleNeuronActivations, _ = singleNeuronActivations:sort(1)
    local initialClusters = singleNeuronActivations:clone():cat(singleNeuronActivations:clone(), 2)
    for i = 1, initialClusters:size()[1] - 1 do
      initialClusters[i][2] = initialClusters[i][2] + torch.abs(initialClusters[i][2] - initialClusters[i+1][2]) / 2
    end
    initialClusters[initialClusters:size()[1]][2] = math.huge -- last cluster is not bounded. (first one is not bounded either)
    clustersTable[neuronIndx] = initialClusters
  end
  self.clustersTable = clustersTable
  self.modifierFunction = ActivationModifier.clusterizerFuncFromClusters(clustersTable, min, max)
end
