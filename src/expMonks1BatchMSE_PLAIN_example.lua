-- loarocks install csv
csv = require "csv"
nn = require "nn"
optim = require "optim"

-- prepare data set

torch.manualSeed(1)

datasetTrain={}
datasetTest={}
sizeTrain=0
sizeTest=0

local f = csv.open("data/monks-original/monks-1.train.csv")
for cols in f:lines() do
  local input = torch.Tensor(6)
  local output=torch.Tensor(2)
  for i,v in ipairs(cols) do
    if i>=2 and i<=7 then
      input[i-1] = v
    end
    if i==1 then
      for j=1,2 do
        output[j] = j==(v+1) and 1 or 0
      end
    end
--    print (input,output)
  end
  sizeTrain=sizeTrain+1
  datasetTrain[sizeTrain]={input,output}
end

function datasetTrain:size() return sizeTrain end


local f = csv.open("data/monks-original/monks-1.test.csv")
for cols in f:lines() do
  local input = torch.Tensor(6)
  local output=torch.Tensor(2)
  for i,v in ipairs(cols) do
    if i>=2 and i<=7 then
      input[i-1] = v
    end
    if i==1 then
      for j=1,2 do
        output[j] = j==(v+1) and 1 or 0
      end
    end
--    print (input,output)
  end
  sizeTest=sizeTest+1
  datasetTest[sizeTest]={input,output}
end

function datasetTest:size() return sizeTest end

-- make model

nhidden = 10

model = nn.Sequential();
model:add(nn.Linear(6,nhidden))
model:add(nn.Tanh())
model:add(nn.Linear(nhidden,2))
--model:add(nn.SoftMax())
model:add(nn.LogSoftMax())


print (model)
--criterion = nn.CrossEntropyCriterion()
criterion = nn.MSECriterion()
--trainer = nn.StochasticGradient(model, criterion)
--trainer.learningRate = 0.01
--trainer.maxIteration = 100
--trainer:train(datasetTrain)

function createBatch(dataset, batchSize, datasetIndex, inputSize, targetSize)
  -- create mini batch
    local inputs = torch.Tensor(batchSize, inputSize)
    local targets = torch.Tensor(batchSize, targetSize)
    if (targetSize == 1) then
      targets = targets:squeeze() -- if we have 1xbatchSize 2D tensor - ClassNNLCriterion heavily complains, thus we convert it to 1D tensor 
    end
    local k = 1
    for i = datasetIndex, math.min(datasetIndex + batchSize-1, dataset:size()) do
      -- load new sample
      local input = dataset[i][1]:clone()
      local target = dataset[i][2]:clone()
      if (targetSize == 1) then 
        target = target:squeeze() 
      end
      inputs[k] = input
      targets[k] = target
      k = k + 1
    end
      
    if (k-1 < batchSize) then -- trunc input/target tensors
      inputs = inputs:narrow(1, 1, k-1)
      targets = targets:narrow(1, 1, k-1)
    end
    return inputs, targets
end

--for i = 1,200 do
--  local inputSize = datasetTrain[1][1]:size(1)
--  local targetSize = datasetTrain[1][2]:size(1)
--  local train = datasetTrain
--  local batchSize = 10
--  for t = 1,datasetTrain:size(),batchSize do
--    local inputs, targets = createBatch(datasetTrain, batchSize, t, inputSize, targetSize)
--    -- feed it to the neural network and the criterion
--    criterion:forward(model:forward(inputs), targets)
--  
--    -- train over this example in 3 steps
--    -- (1) zero the accumulation of the gradients
--    model:zeroGradParameters()
--    -- (2) accumulate gradients
--    model:backward(inputs, criterion:backward(model.output, targets))
--    -- (3) update parameters with a 0.01 learning rate
--    model:updateParameters(0.01)
--  end
--end

function calcConfusion(model, dataset, classNames)
  local confusion = optim.ConfusionMatrix(classNames)
  local inputSize = dataset[1][1]:size(1)
  local targetSize = dataset[1][2]:size(1)
  local batchSize = 10
  for t = 1,dataset:size(),batchSize do
      -- create mini batch
    local batchInputs, batchLabels = createBatch(dataset, batchSize, t, inputSize, targetSize)
    
    -- test samples
    local preds = model:forward(batchInputs)
    -- confusion:
    for i = 1,preds:size(1) do
       confusion:add(preds[i], batchLabels[i]:squeeze())
    end
  end
  
  confusion:updateValids() 
  return confusion
end

classNames = {"1", "2"}

for epoch = 1,1000 do
  local inputSize = datasetTrain[1][1]:size(1)
  local targetSize = datasetTrain[1][2]:size(1)
  local train = datasetTrain
  local batchSize = 10
  
  local params, gradParams = model:getParameters()
  
  for t = 1,datasetTrain:size(),batchSize do
    local batchInputs, batchLabels = createBatch(datasetTrain, batchSize, t, inputSize, targetSize)
    
    sgdState = sgdState or {
      learningRate = 0.005,
      momentum = 0,
      learningRateDecay = 0,
      weightDecay = 0
    }
    
    function feval(params)
      gradParams:zero()

      local outputs = model:forward(batchInputs)
      local loss = criterion:forward(outputs, batchLabels)
      local dloss_doutputs = criterion:backward(outputs, batchLabels)
      model:backward(batchInputs, dloss_doutputs)

      return loss, gradParams
    end
    optim.sgd(feval, params, sgdState)
    
--    -- feed it to the neural network and the criterion
--    criterion:forward(model:forward(inputs), targets)
--  
--    -- train over this example in 3 steps
--    -- (1) zero the accumulation of the gradients
--    model:zeroGradParameters()
--    -- (2) accumulate gradients
--    model:backward(inputs, criterion:backward(model.output, targets))
--    -- (3) update parameters with a 0.01 learning rate
--    model:updateParameters(0.01)
  end
  
  if (epoch % 100 == 0) then
    local confusionTrain = calcConfusion(model, datasetTrain, classNames)
    local confusionTest = calcConfusion(model, datasetTest, classNames)
    print(epoch)
    print(confusionTrain)
    print(confusionTest)
  end
end


local confusionTrain = calcConfusion(model, datasetTrain, classNames)
local confusionTest = calcConfusion(model, datasetTest, classNames)

print(confusionTrain)
print(confusionTest)