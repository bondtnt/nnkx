actClusterizerUtil = {}

local torch = require('torch')

-- function for flattening given tensor.
--------------------------------------------
-- @param t       - tensor.
-- @return 1D tensor containg all values from input tensor t.
function actClusterizerUtil.flatten(t)
  typeUtil.assert(t, "Tensor")
  return t:view(t:nElement())
end

-- function for returning key (index) of the element in the tensor.
--------------------------------------------
-- @param t       - tensor.
-- @param value   - value to look for     
-- @return tensor containg indices for values. If t is 2 dimensional, indices will be 2D. 
function tensorUtil.keyForValue( t, value )
  typeUtil.assert(t, "Tensor")
  return torch.linspace(1, t:nElement(), t:nElement())[t:eq(value)]
end

-- function for returning indices of folds
--------------------------------------------
-- @param t       - tensor. 
-- @return uniqueset - tensor with unique t values
-- @return ic           
function tensorUtil.uniqueTensorValues(t)
  typeUtil.assert(t, "Tensor")
  local valsetTable = tensorUtil.frequencies(t)
  -- now convert that valset table keys into normal table with values
  local uniqueset={}
  local n=0
  for k,_ in pairs(valsetTable) do
    n=n+1
    uniqueset[n]=k
  end
  
  local ic = tableUtil.icForUnique(uniqueset, t:totable())
  
  return uniqueset, ic
end

-- function for checking table contains element
--------------------------------------------
-- @param t       - tensor.
-- @param value     - value to check.
-- @return true / false     
function tensorUtil.contains(t, value)
  typeUtil.assert(t, "Tensor")
  
  if 0 == x[x:eq(value)]:nElement() then
    return false
  end
  return true
end

-- function returns table with keys equal to unique values from input tensor
-- and values equal to number of values encountered in input
function tensorUtil.unique_counts(input)
  typeUtil.assert(input, "Tensor")

  size = input:nElement()
  y = input:view(size) --flatten input
  b = {}
  for i=1,size do
    a = y[i]
    if b[a] == nil then 
      b[a] = 1
    else 
      b[a] = b[a] + 1
    end  
  end
  return b
end

-- function for returning frequencies of the elements in the table.
--------------------------------------------
-- @param t       - tensor.    
-- @return table containing frequencies.  
function tensorUtil.frequencies(t)
  typeUtil.assert(t, "Tensor")

  local valset = {}
  t:view(t:nElement())
  local i = 1
  for v in t[i] do
    if (valset[v] == nil) then
      valset[v] = 1
    else 
      valset[v] = valset[v] + 1
    end  
  end
  return valset
end
  
-- function for returning minimal distance between 2 elements of tensor.
--------------------------------------------
-- @param t       - must be sorted (!) 1 column tensor with at least 1 element
-- @return minDistance, index1, index2. 
function tensorUtil.findNearestElements(tensor)
  if (tensor:nElement() == 1) then
    return 0, 1, 1
  end
  
  local dMin = math.huge
  local iIndx = -1
  local jIndx = -1
  for i = 1, tensor:nElement()-1 do
    local dist = math.abs(tensor[i][1] - tensor[i+1][1])
    if (dist < dMin) then
      dMin = dist
      iIndx = i
      jIndx = i+1
    end
  end
  
  return dMin, iIndx, jIndx
end

-- function for deleting 2D tensor row.
--------------------------------------------
-- @param tensor       - must be 2D tensor
-- @param row          - row index to remove
-- @return tensor without specified row 
function tensorUtil.delete2DTensorRow(tensor, row)
  local mask = torch.ByteTensor(tensor:size()):fill(1)
  mask[row]:fill(0)
  local newSize = tensor:size()
  newSize[1] = newSize[1] - 1
  return tensor:maskedSelect(mask):resize(newSize)
end

return tensorUtil