require('mobdebug').start()

--local csv = require("csv")
--local f = csv.open("data/iris/iris.data")
--for fields in f:lines() do
--  for i, v in ipairs(fields) do print(i, v) end
--end

require 'csvUtil'
tableUtil = require 'tableUtil'
tensorUtil = require 'tensorUtil'

--d = require 'Dataframe'
--df = Dataframe()
--
--df:load_csv{path='data/iris/iris_with_headers.data', header=true}
--df:as_categorical('class')
--
--df:show()

function loadIrisData()
  local filePath = 'data/iris/iris.csv'
  local headerPresent = false
  local splitChar = '\t'
  local cutLastRow = true
  local data = csvUtils.loadCsvData(filePath, headerPresent, splitChar, cutLastRow)
  local x = data[{{},{1,-2}}]
  x = torch.cat(x[{{},{2,4}}], x[{{},{1}}], 2) -- put first column to last place
  local y = data[{{},{-1}}]:contiguous()
  return x, y
end

function loadSeparatedData(n)
  -- generates fully searated 3 classes on 1 dimension
  local gen = torch.Generator()
  local x1 = torch.Tensor(n,1):apply(function() return torch.normal(gen, 0.4, 0.15) end)
  local y1 = torch.Tensor(x1:size(1), 1):fill(1)

  local x2 = torch.Tensor(n,1):apply(function() return torch.normal(gen, 0.5, 0.15) end)
  local y2 = torch.Tensor(x1:size(1), 1):fill(2)
  
  local x3 = torch.Tensor(n,1):apply(function() return torch.normal(gen, 0.6, 0.15) end)
  local y3 = torch.Tensor(x1:size(1), 1):fill(3)
  
  local x = torch.cat(torch.cat(x1, x2, 1), x3, 1)
  local y = torch.cat(torch.cat(y1, y2, 1), y3, 1)
  
  return x, y
end

function loadSurroundedData(n)
  -- generates fully searated 3 classes on 1 dimension
  local gen = torch.Generator()
  local x1 = torch.Tensor(n,1):apply(function() return torch.normal(gen, 0.2, 0.05) end)
  local y1 = torch.Tensor(x1:size(1), 1):fill(1)

  local x2 = torch.Tensor(n, 1):apply(function() return torch.normal(gen, 0.5, 0.05) end)
  local y2 = torch.Tensor(x1:size(1), 1):fill(2)
  
  local x3 = torch.Tensor(n,1):apply(function() return torch.normal(gen, 0.8, 0.05) end)
  local y3 = torch.Tensor(x1:size(1), 1):fill(3)
  
  local x = torch.cat(torch.cat(x1, x2, 1), x3, 1)
  local y = torch.cat(torch.cat(y1, y2, 1), y3, 1)
  
  return x, y
end

--local x,y = loadIrisData()
--local x,y = loadSeparatedData(2000)
local x,y = loadSurroundedData(2000)
local indices = torch.linspace(1,y:size(1),y:size(1)):long()

-- LDA in 5 steps: http://sebastianraschka.com/Articles/2014_python_lda.html#lda-in-5-steps
-- Step 1: Computing the d-dimensional mean vectors
function compMeanVectors(X, y) 
  local classes, _ = tensorUtil.uniqueTensorValues(y:view(-1))
  local mean_vectors = nil
  for _,v in pairs(classes) do
    local indicesForclass = indices[y:eq(v)]
    local xForClass = X:index(1, indicesForclass)
    local meanForClass = torch.mean(xForClass, 1)
    if (mean_vectors == nil) then
      mean_vectors = meanForClass
    else 
      mean_vectors = mean_vectors:cat(meanForClass, 1)
    end     
  end
  return mean_vectors, classes
end

local mean_vectors, classes = compMeanVectors(x, y)
print('Mean vectors for classes/features:')
require 'pl.pretty'.dump(mean_vectors)
print('\n')

local featuresCount = mean_vectors:size(2)

-- Step 2: Computing the Scatter Matrices
-- 2.1 Within-class scatter matrix SW
function scatterWithin(X, y, classes)
  if (classes == nil) then
    classes, _ = tensorUtil.uniqueTensorValues(y:view(-1))
  end
  local S_W = torch.Tensor(featuresCount, featuresCount):zero()
  local i = 1
  for _,cl in pairs(classes) do
    local mv = mean_vectors[{{i},{}}]
    local class_sc_mat = torch.Tensor(featuresCount, featuresCount):zero()
    i = i + 1
    
    local indicesForclass = indices[y:eq(cl)]
    local xForClass = X:index(1, indicesForclass)
    -- iterate over class vectors
    for j = 1,xForClass:size(1) do
      local row = xForClass[{{j},{}}]
      local tmp = (row - mv):t() * (row - mv)
      class_sc_mat:add(tmp)
    end
    S_W:add(class_sc_mat)  
  end
  return S_W
end  
local S_W = scatterWithin(x, y, classes)
print('within-class Scatter Matrix:')
require 'pl.pretty'.dump(S_W)
print('\n')

-- 2.2 Between-class scatter matrix SBSB
function scatterBetween(X, y, classes)
  if (classes == nil) then
    classes, _ = tensorUtil.uniqueTensorValues(y:view(-1))
  end
  local overall_mean = torch.mean(X, 1)
  local i = 1
  local S_B = torch.Tensor(featuresCount, featuresCount):zero()
  for _,cl in pairs(classes) do
    local mv = mean_vectors[{{i},{}}]
    local n = y:eq(cl):sum()
    i = i + 1
    local tmp = ((overall_mean - mv):t()):mul(n) * (overall_mean - mv)
    S_B:add(tmp)
  end
    return S_B
end
local S_B = scatterBetween(x, y, classes)
print('between-class Scatter Matrix:')
require 'pl.pretty'.dump(S_B)
print('\n')

--eig_vals, eig_vecs = np.linalg.eig(np.linalg.inv(S_W).dot(S_B))

local invS_W = torch.inverse(S_W)
local eig_vals, _ = torch.eig(invS_W * S_B, 'N')
eig_vals = eig_vals[{{},{1}}]

require 'pl.pretty'.dump(eig_vals)
--require 'pl.pretty'.dump(eig_vecs)
print('done')


--local indices = torch.linspace(1,y:size(1),y:size(1)):long()
--local indices1class = indices[y:eq(1)]
--local indices2class = indices[y:eq(2)]
--local indices3class = indices[y:eq(3)]
--local x1 = x:index(1,indices1class)
--local x2 = x:index(1,indices2class)
--local x3 = x:index(1,indices3class)
--
--torch.mean(x1, 1)
--
--
--require 'pl.pretty'.dump(data)
--require 'pl.pretty'.dump(x1)
--require 'pl.pretty'.dump(x2)
--require 'pl.pretty'.dump(x3)

-- now let's calculate mean vectors for each class


