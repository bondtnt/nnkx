torch = require('torch')
--require 'nn'

local ActivationRecorder, parent = torch.class('nn.ActivationRecorder', 'nn.Module')
--local tableUtil = require('tableUtil')

-- Class extends nn module - this is essentially a layer which saves all incoming inputs.
-- It should be places behind pruning or linear layers and afterwards network should be
-- fed with full dataset. Afterwards this layer will have all activations of all neurons 
-- from previous layer. These activations are input to next rules extraction step - discretization.  
--------------------------------------------
function ActivationRecorder:__init(recordFlag)
  parent.__init(self)
  self.recordFlag = recordFlag
  self.activations = torch.Tensor()
  return self
end

-- Resetting activations table. 
--------------------------------------------
function ActivationRecorder:reset()
  self.activations = torch.Tensor()
end

function ActivationRecorder:updateOutput(input)
  self.output:resizeAs(input):copy(input)
  if (self.recordFlag == true) then
    local output = self.output:clone()
    local outputTensor = output:resize(output:size()[1],1):t()
    if (self.activations:nDimension() == 0) then
      self.activations = outputTensor
    else
      self.activations = torch.cat(self.activations, outputTensor, 1)
    end
  end
  return self.output
end

function ActivationRecorder:updateGradInput(input, gradOutput)
  self.gradInput:resizeAs(gradOutput):copy(gradOutput)
  return self.gradInput
end