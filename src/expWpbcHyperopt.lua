-- This script is intended to run hypero 


require('mobdebug').start()

require 'graph'
require 'pl'
require 'paths'

plPretty = require('pl.pretty')
torch = require('torch')
optim = require('optim')
nn = require('nn')
csvUtil = require('csvUtil')
dataUtil = require('dataUtil')
tableUtil = require('tableUtil')
tensorUtil = require('tensorUtil')
activationModifierUtil = require('activationModifierUtil')
runningUtil = require('runningUtil')
typeUtil = require('typeUtil')
nngraph = require('nngraph')
mathUtil = require('mathUtil')

require 'NeuronPruning'
require 'NeuronPruningLayer'
require 'ActivationRecorder'
require 'ActivationModifier'
require 'SensitivityPruningLogic'
require 'N2PSPruningLogic'
require 'N2PSSensitivityLayeredPruningLogic'
require 'RulesGenerator'
require 'ActivationsDiscretizer'

torch.manualSeed(1)
local timeExec = sys.clock()


local opt = lapp[[
   -b, --batchSize     (default 10)          batch size
   -o, --optimization  (default "SGD")       optimization: SGD | LBFGS 
   -r, --learningRate  (default 0.05)        learning rate, for SGD only
   -m, --momentum      (default 0)           momentum, for SGD only
   -e, --maxEpochs     (default 10)          maximum nb of epochs per single run (fold)
   -i, --maxIter       (default 3)           maximum nb of iterations per batch, for LBFGS
   --coefL1            (default 0)           L1 penalty on the weights
   --coefL2            (default 0)           L2 penalty on the weights
   -t, --threads       (default 4)           number of threads
   --tZero             (default 1)           start averaging at t0 (ASGD only), in nb of epochs
]]
opt.optimization = 'SGD'
opt.batchSize = 10
opt.learningRate = 0.05
opt.maxEpochs = 1000
opt.threads = 8
--opt.momentum = 0.01
--opt.coefL2 = 0.001

-- fix seed
torch.manualSeed(1)

-- threads
torch.setnumthreads(opt.threads)
print('<torch> set nb of threads to ' .. torch.getnumthreads())

-- use floats, for SGD
if opt.optimization == 'SGD' then
  torch.setdefaulttensortype('torch.FloatTensor')
end

if opt.optimization == 'LBFGS' and opt.batchSize < 100 then
  error('LBFGS should not be used with small mini-batches; 1000 is recommended')
end

require 'hypero'
hopt = {}
hopt.startLR = '{0.001,1}'
hopt.minLR = '{0.001,1}'
hopt.momentum = '{0, 0.01, 1, 2}'
hopt.hiddenSize = '{5, 10, 15, 20, 25}'
hopt.batchSize = '{1, 5, 10, 20}'
hopt.extra = '{1,1,1}'
hopt.maxEpoch = '{250, 500, 750}'
hopt.extra = '{1,1,1}' --batchNorm, dropout

--conn = hypero.connect() --{database='localhost', username='hypero'}
--batName = "Wpbc model single layer - hyperopt"
--verDesc = "Initial trial to find best options"
--bat = conn:battery(batName, verDesc)

--[[dataSet]]--

--local time = os.date("*t")
--local timestamp = ("%02d%02d%02d_%02d-%02d-%02d"):format(time.year, time.month, time.day, time.hour, time.min, time.sec)
local dataSetName = 'wpbc-batched' --used for log files naming
print(dataSetName)
local filePath = '/home/andrey/works/LuaProjects/nnkx/data/wpbc.data.csv'
--local filePath = '/home/andrey/works/LuaProjects/nnkx/data/iris/iris.csv'
local data = csvUtils.loadCsvData(filePath, false, ',', true)
--shuffle data tensor
local shuffleIndexes = torch.randperm(data:size(1)):long()
data = data:index(1, shuffleIndexes)

local X, originalMin, originalMax = tensorUtil.scaleTensor(data[{ {}, {4,-2} }], -1, 1) -- we will be using tanh function
local XX, newMin, newMax = tensorUtil.scaleTensor(X, originalMin, originalMax) -- we will be using tanh function
--X = tensorUtil.scaleTensor(X, 0, 1) -- we will be using sigm function
local labels = data[{ {}, {-1} }]
labels = tensorUtil.scaleTensor(labels, 1, 2) -- we know there are 2 classes - mark them as 1 and 2
local targets, classesNumber, labels2Targets = dataUtil.labelsColumnToMatrix(labels, -1, 1) -- (-1 + 1) for tanh, (0, +1) for sigmoid
local labels2Targets = {}
for i = 1, labels:size(1) do
  labels2Targets[labels[i][1]] = targets[i]
end

local dataset = dataUtil.convertToDataset(X, targets)

classNames = {}
for i = 1, classesNumber do
  classNames[i] = ''..i
end

-- ANN parameters
local inputsSize = dataset[1][1]:size(1)
local outputsSize = classesNumber -- For ClassNNLCriterion - there must be classesNumber output neurons.
local hidden1Size = 40

local opt = {}
opt.hiddenSize = hidden1Size
opt.batchSize = 10
opt.optimization = 'SGD'
opt.learningRate = 0.02
opt.maxEpochs = 500
opt.threads = 4
opt.momentum = 0 --0.5 --0.01
opt.coefL1 = 0 --0.1
opt.coefL2 = 0.001

--function buildExperiment(opt)

  --[[Model]]--
  local model = nn.Sequential()
  model:add( nn.Linear(inputsSize, opt.hiddenSize) )
--  if opt.extra == 'batchNorm' then
     model:add(nn.BatchNormalization(opt.hiddenSize))
--  end
  model:add( nn.ReLU() )
--  if opt.extra == 'dropout' then
     model:add(nn.Dropout())
--  end
  model:add( nn.Linear(opt.hiddenSize, outputsSize) )
  model:add(nn.LogSoftMax())

--  print('<> using model:')
--  print(model)
--end

local criterion = nn.MSECriterion()
--local criterion = nn.CrossEntropyCriterion()
--local criterion = nn.ClassNLLCriterion()

local xvalIteration = 1
local xValLogs = {}
xValLogs.trainConfusion = {}
xValLogs.testConfusion = {}

local C = dataUtil.cvpartitionKFold(tableUtil.flatten(labels:totable()), 10)
local fN = 1
local trainData = tableUtil.rows(dataset, C.getTrainFold(fN))
local testData = tableUtil.rows(dataset, C.getTestFold(fN))

--  model:training()
local trainConf = runningUtil.train(model, criterion, trainData, classNames, opt)
local testConf = runningUtil.test(model, testData, classNames, opt.batchSize)
trainConf:updateValids()
testConf:updateValids()
xValLogs.trainConfusion[xvalIteration] = trainConf
xValLogs.testConfusion[xvalIteration] = testConf
print('trainConf '.. trainConf:__tostring__())
print('testConf '.. testConf:__tostring__())
