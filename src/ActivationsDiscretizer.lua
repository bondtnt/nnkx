--require 'torch'
--require 'nn'
modelPingUtil = require('modelPingUtil')

local ActivationsDiscretizer = torch.class('nn.ActivationsDiscretizer')
local tableUtil = require('tableUtil')
local dataUtil = require('dataUtil')
local runningUtil = require('runningUtil')
require('ActivationModifier')
local activationModifierUtil = require('activationModifierUtil')
nn = require('nn')

-- Class implementing ActivationsDiscretizer module. 
-- It holds logic for dicretizing and clusterizing activation values of 
-- 'nn.ActivationModifier' layers in layered neural network
--------------------------------------------
-- @param zeroVal         - Value to which we should nullify pruned neurons (1's in prunning mask)
--                          It is 0 in case Tanh is used (as Tanh output belongs to [-1, +1]
--                          and it should be assigned to 0.5 in case     
function ActivationsDiscretizer:__init()
--  parent.__init(self)
  return self
end

-- This function travels through massed mlp, looks for ActivationModifier layers and inits them
-- with afterComaDigitsDiscretizerFunc( d ) - where d is number of digits to be left after comma.
-- this basically discretizes all activations across whole network. 
-- Afterwards confusion is calculated (via passed in function - confusionCalculatorFunc)
-- and if it does not drop d is decreased and procedure repeated.
-- Operation continues until minimal d that does not increase error is found 
function ActivationsDiscretizer:discretize(mlp, initialD, confusionCalculatorFunc)
--  local confusionD = calculateConfusion(mlp, dataset, classNames)
  local d = initialD -- let's round activations to d digits after coma. PLease note that 0 digits is Ok with us here.
  local originalConfusion = confusionCalculatorFunc()
  local continuationFlag = true
  while (continuationFlag == true) do
    runningUtil.applyToFilteredLayers(mlp, 'nn.ActivationModifier', function(layer) layer.modifierFunction = nn.ActivationModifier().afterComaDigitsDiscretizerFunc( d ) end)
    
    local newConfusion = confusionCalculatorFunc() --calculateConfusion(mlp, dataset, classNames)
    local isNewConfusionWorse = newConfusion.totalValid < originalConfusion.totalValid
    if (isNewConfusionWorse or d < 0) then
      d = d + 1 -- rollback
      continuationFlag = false
      -- Revert back appropriate discretization level
      runningUtil.applyToFilteredLayers(mlp, 'nn.ActivationModifier', function(layer) layer.modifierFunction = nn.ActivationModifier().afterComaDigitsDiscretizerFunc( d ) end)
    else
      d = d - 1
    end  
  end
  return d
end

-- Function creates clusters from activations.
-- It assumes that activations recorder layers are located after modifier layers.
-- Modifier layers are those who will be fed with found clusters. They will modify continuous activations
-- into discreete values (clusters centers).
-- These clustered values are required to extract rules (or build decision tree)
-- @param mlp - seqential container - 'multilayered perceptron'
-- @param - confusionCalculatorFunc - feedback function to check performance
-- @param initialEpsilon - initial minimal distance between activation values of single neuron to be merged
--                         It should be more than 2 in case tanh is used and we have possible distances between activation values of about 2
--                          We will be decreasing this value untill find appropirate which preserves accuracy. 
function ActivationsDiscretizer:clusterize(mlp, confusionCalculatorFunc, initialEpsilon) 
  print('Clusterize')
  
  local epsilon = initialEpsilon 
  local epsilonDecreaseStep = 0.8 -- in case supplied initialEpsilon is too large it will be decreased using epsilonDecreaseStep. Until accuracy will stay the same.
  
  -- Clusterize
  -- Initialize cluster-modifier-functions for ActivationModifier layers
  local confusionOrig = confusionCalculatorFunc()
  confusionOrig:updateValids()
  for i = 1,mlp:size() do
    if (torch.type(mlp:get(i)) == 'nn.ActivationModifier') then
      local recorderLayer = mlp:get(i+1) --we assume recorder layer is located right after modifier layer
      local activations = recorderLayer.activations
      local modifierLayer = mlp:get(i)
      modifierLayer:buildAndSetClusterizerModifierFunction(activations)
    end
  end
  local iter = 1
  -- for all layers starting from last one
  for i = mlp:size(),1,-1 do
    if (torch.type(mlp:get(i)) == 'nn.ActivationModifier') then
      local modifierLayer = mlp:get(i)
      if modifierLayer.clustersTableBackup == nil then
         -- initially clustersTable contains continuous activation values, they will be merged into clusters (wtih boundaries)
        modifierLayer.clustersTableBackup = tableUtil.clone(modifierLayer.clustersTable)
      else
        modifierLayer.clustersTable = tableUtil.clone(modifierLayer.clustersTableBackup)
      end
      local clustersTable = modifierLayer.clustersTable
      for neuronIndx = 1,#clustersTable do
--        print('====== neuronIndx='..neuronIndx)
        local neuronClusters = clustersTable[neuronIndx]
        local neuronClustersBackup = neuronClusters:clone()
        
        local localEpsilon = epsilon
--        print('1. localEpsilon='..localEpsilon)
        local continuationFlag = true 
--        local decreasingEpsilon = true
        while continuationFlag do
--          print('neuronClusters=>')        
--          print(neuronClusters)
          local mergedClusters = activationModifierUtil.clusterize(neuronClusters, localEpsilon)
--          print('mergedClusters=>')
--          print(mergedClusters)
          clustersTable[neuronIndx] = mergedClusters
          
          local confusionNew = confusionCalculatorFunc()
          confusionNew:updateValids()
--          if (decreasingEpsilon) then
          continuationFlag = (confusionNew.totalValid < confusionOrig.totalValid) and (mergedClusters:size(1) < neuronClustersBackup:size(1)) -- if epsilon is so small that nothing is merged - exit
          
          iter = iter + 1
--          end
--          if (decreasingEpsilon and continuationFlag == false) then
--            decreasingEpsilon = false  
--          end
--          print('continuationFlag='..tostring(continuationFlag)..'; new totalValid='..confusionNew.totalValid..'; orig totalValid='..confusionOrig.totalValid..'; mergedClusters:size(1)='..mergedClusters:size(1)..'; neuronClustersBackup:size(1)='..neuronClustersBackup:size(1))
          if (continuationFlag) then -- decrease
            modelPingUtil.pingModelWriteFile(mlp, width_p, height_p, minX_p, minY_p, maxX_p, maxY_p, "trainedNetwork_discretize_iter" .. tostring(iter) .. "_L"..tostring(i).. "_n" .. tostring(neuronIndx) .. ".txt")
            localEpsilon = localEpsilon * epsilonDecreaseStep
--            print('2. decreased localEpsilon='..localEpsilon)
            clustersTable[neuronIndx] = neuronClustersBackup
          end 
        end
        modelPingUtil.pingModelWriteFile(mlp, width_p, height_p, minX_p, minY_p, maxX_p, maxY_p, "trainedNetwork_discretize_iter" .. tostring(iter) .. "_FinishedNeuron_L"..tostring(i).. "_n" .. tostring(neuronIndx) .. ".txt")     
      end 
    end
  end
  print('Clusterize finished')
end
