package.loadlib("libpython2.7.so", "*")
--require("lua-python")
--python.execute("import string")
--string = python.eval("string")

require("python")
python.execute("import string")
pg = python.globals()
pg.string.lower("Hello world!")
-- As Lua is mainly an embedding language, getting access to the batteries included in Python may be interesting.
re = python.import("re")
pattern = re.compile("^Hel(lo) world!")
match = pattern.match("Hello world!")
-- Just like in the Python example, let's put a local object in Python space.
d = {}
pg.d = d
python.execute("d['key'] = 'value'")
table.foreach(d, print)
-- Again, let's grab back the reference from Python space.
d2 = python.eval("d")
print(d == d2)

--Is the Lua interface available to Python?
python.eval("lua")

--Good. So let's do the nested trick in Lua as well.
t={}
python.eval("lua.eval('python.eval(\"lua.eval(\\'t\\')\")')")

--It means that the Lua interpreter state inside the Python interpreter is the same as the outside Lua interpreter state. Let's show that in a more obvious way.
python.execute("lg = lua.globals()")
python.eval("lg.t")


function notthree(num)
  return (num ~= 3)
end
l = python.eval("[1, 2, 3, 4, 5]")
filter = python.eval("filter")
filter(notthree, l) --[1, 2, 4, 5]

