--[[
Tests code
]]
--require("luacov") -- afterwards in command line execute luacov - to generate report file


require('mobdebug').start()
plPretty = require('pl.pretty')

torch = require('torch')
optim = require('optim')

_DEBUG = true

nn = require('nn')
csvUtil = require('csvUtil')
dataUtil = require('dataUtil')
tableUtil = require('tableUtil')
pruningUtil = require('pruningUtil')
tensorUtil = require('tensorUtil')
activationModifierUtil = require('activationModifierUtil')
runningUtil = require('runningUtil')
typeUtil = require('typeUtil')
nngraph = require('nngraph')
mathUtil = require('mathUtil')

require 'NeuronPruning'
require 'NeuronPruningLayer'
require 'ActivationRecorder'
require 'ActivationModifier'
require 'SensitivityPruningLogic'
require 'N2PSPruningLogic'
require 'N2PSSensitivityLayeredPruningLogic'
require 'RulesGenerator'
require 'ActivationsDiscretizer'

require 'graph'

local mytester = torch.Tester()

torch.manualSeed(1)

local precision = 1e-5
local expprecision = 1e-4

local nnpruntest = torch.TestSuite()

-- threads
torch.setnumthreads(4)
print('<torch> set nb of threads to ' .. torch.getnumthreads())

--===================================================================

function nnpruntest.pruningUtil_calculateMislassErrors()
  local c1 = torch.Tensor({{0, 0}})
  pruningUtil.calculateMislassErrors(c1, nil)
  mytester:assertTensorEq(torch.Tensor({{0.0, 0.0}}), c1, precision)
  
  local c2 = torch.Tensor({{3, 0}})
  pruningUtil.calculateMislassErrors(c2, nil)
  mytester:assertTensorEq(torch.Tensor({{0.0, 0.0}}), c2, precision)
  
  local c3 = torch.Tensor({{0, 3, 0}})
  pruningUtil.calculateMislassErrors(c3, nil)
  mytester:assertTensorEq(torch.Tensor({{0.0, 0.0, 0.0}}), c3, precision)
  
  local c4 = torch.Tensor({{0, 3, 4}})
  pruningUtil.calculateMislassErrors(c4, nil)
  mytester:assertTensorEq(torch.Tensor({{0.0, 0.0, 3.0}}), c4, precision)
  
  local c5 = torch.Tensor({{1, 4, 3}})
  pruningUtil.calculateMislassErrors(c5, nil)
  mytester:assertTensorEq(torch.Tensor({{0.0, 3.0, 0.0}}), c5, precision)
  
  local c6 = torch.Tensor({{1, 4, 4}})
  pruningUtil.calculateMislassErrors(c6, nil)
  mytester:assertTensorEq(torch.Tensor({{0.0, 4.0, 0.0}}), c6, precision) -- just pick first column
end

function nnpruntest.pruningUtil_histClassSeparability()
  -- Class 1:
  -- ^
  -- |          + +  
  -- |      + + + + + + 
  -- ------------------------------------------------->
  -- 0   0.1   0.2   0.3   0.4   0.5   0.6   0.7
  local x1 = torch.Tensor({0.11, 0.15, 0.18, 0.201, 0.21, 0.22, 0.23, 0.25, 0.29, 0.31})
  
  -- Class 2:
  -- ^
  -- |              o o  
  -- |          o o o o o o 
  -- ------------------------------------------------->
  -- 0   0.1   0.2   0.3   0.4   0.5   0.6   0.7
  local x2 = torch.Tensor({0.21, 0.23, 0.27, 0.29, 0.301, 0.305, 0.31, 0.32, 0.36, 0.39})
  
  local X = torch.cat(x1, x2, 1)
  local y = torch.cat(torch.Tensor(10):zero(), torch.Tensor(10):fill(1), 1)

  local H = pruningUtil.histClassSeparability( X, y, 10, 0.0, 1.0 )
  local expectedH = torch.Tensor(10, 2):zero()
  expectedH[3][1] = 4
  expectedH[4][2] = 1
  -- it means we are expecting in 3rd bin to have potentially 4 'B' entries classified as 'A' class,
  -- as well as in 4th bin One 'A' class entry missclassified as 'B' class. 
  mytester:assertTensorEq(H, expectedH, precision)
end

--===================================================================

function nnpruntest.typeUtil_assert()
  -- lets check there are no exceptions
  typeUtil.assert(torch.Tensor(),"Tensor")
  typeUtil.assert({},"table")
end

function nnpruntest.typeUtil_isTensor()
  mytester:assert(true == typeUtil.isTensor(torch.Tensor()))
  mytester:assert(true == typeUtil.isTensor(torch.ByteTensor({0, 0, 1, 1})))
  mytester:assert(true == typeUtil.isTensor(torch.DoubleTensor({{0.7, 0.8}, {0.1, 0.1}})))
  mytester:assert(true == typeUtil.isTensor(torch.IntTensor()))
  mytester:assert(false == typeUtil.isTensor(1))
  mytester:assert(false == typeUtil.isTensor({}))
end

function nnpruntest.typeUtil_isStorage()
  mytester:assert(true == typeUtil.isStorage(torch.Tensor():size()))
  mytester:assert(true == typeUtil.isStorage(torch.ByteTensor({0, 0, 1, 1}):size()))
  mytester:assert(true == typeUtil.isStorage(torch.DoubleTensor({{0.7, 0.8}, {0.1, 0.1}}):size()))
  mytester:assert(true == typeUtil.isStorage(torch.IntTensor():size()))
  mytester:assert(false == typeUtil.isStorage(1))
  mytester:assert(false == typeUtil.isStorage({}))
end

--===================================================================

function nnpruntest.ActivationModifier_afterComaDigitsDiscretizer()
  local d0 = nn.ActivationModifier().afterComaDigitsDiscretizerFunc(0)
  local d1 = nn.ActivationModifier().afterComaDigitsDiscretizerFunc(1)
  local d2 = nn.ActivationModifier().afterComaDigitsDiscretizerFunc(2)
  
  mytester:assertTensorEq(torch.Tensor({2.0, 0.0}), d0(torch.Tensor({1.9, 0.1})), precision)
  mytester:assertTensorEq(torch.Tensor({1.9, 0.1}), d1(torch.Tensor({1.91, 0.11})), precision)
  mytester:assertTensorEq(torch.Tensor({1.91, 0.11}), d2(torch.Tensor({1.911, 0.111})), precision)
end

function intervalsAreAcceptable(intervals)
--  for the sake of simplicity we will accept only intervals that are not wider than 0.1 (check test data)
  local size = intervals:size()[1] 
  for i = 1,size do
    if (torch.abs(intervals[i][1] - intervals[i][2]) > 0.1) then 
      return false
    end  
  end 
  return true
end

function nnpruntest.ActivationModifier_AND_activationModifierUtil__buildIntervals_buildClustersFromIntervals_clusterizerFunc()
  -- Test #1
  -- activations must be sorted
  local activations = torch.Tensor({0.1, 0.3, 0.31, 0.32, 0.33, 0.5, 0.51, 0.7, 0.71, 0.9, 0.91, 0.92, 0.93})
  activations:resize(activations:size()[1], 1) -- make it size x 1 matrix instead of vector 
  local initialIntervals = activations:clone():cat(activations:clone(), 2)
  local size = activations:size()[1]    
  
  local intervals = activationModifierUtil.buildIntervals(initialIntervals, 1, size, intervalsAreAcceptable)
  mytester:assertTensorEq(intervals, torch.Tensor({{0.1, 0.3, 0.5, 0.7, 0.9}, {0.1, 0.33, 0.51, 0.71, 0.93}}):t(), precision)
  
  local clusters = nn.ActivationModifier().buildClustersFromIntervals(intervals, 0.0, 1.0)
  mytester:assertTensorEq(clusters, torch.Tensor({{0.1, 0.2}, {0.3075, 0.415}, {0.51, 0.605}, {0.705, 0.805}, {0.9025, 1}}), precision)
  
  -- Test #2
  local activations2 = torch.Tensor({0.0, 0.01, 0.02, 0.03, 0.3, 0.5, 0.51, 0.9})
  activations2:resize(activations2:size()[1], 1) -- make it size x 1 matrix instead of vector
  local initialIntervals2 = activations2:clone():cat(activations2:clone(), 2)
  local size2 = activations2:size()[1]
  
  local intervals2 = activationModifierUtil.buildIntervals(initialIntervals2, 1, size2, intervalsAreAcceptable)
  mytester:assertTensorEq(intervals2, torch.Tensor({{0.0, 0.3, 0.5, 0.9}, {0.03, 0.3, 0.51, 0.9}}):t(), precision)
  
  local clusters2 = nn.ActivationModifier().buildClustersFromIntervals(intervals2, 0.0, 1.0)
  mytester:assertTensorEq(clusters2, torch.Tensor({{0.0825, 0.165}, {0.2825, 0.4}, {0.5525, 0.705}, {0.8525, 1}}), precision)
    
  -- Test #3 - clusterizerFunc - it replaces continuous activations with cluster centers
  local actIntervalsTable = {[1] = intervals, [2] = intervals2} -- activation intervals for two neurons
  local cFunc = nn.ActivationModifier().clusterizerFunc(actIntervalsTable, 0.0, 1.0)
  mytester:assertTensorEq(torch.Tensor({0.9025, 0.0825}), cFunc(torch.Tensor({1.0, 0.1})), precision)
  mytester:assertTensorEq(torch.Tensor({0.3075, 0.5525}), cFunc(torch.Tensor({0.4, 0.7})), precision)
  mytester:assertTensorEq(torch.Tensor({0.1, 0.8525}), cFunc(torch.Tensor({0.0, 1.0})), precision)
end

-- Test #4 - bug catching
function nnpruntest.ActivationModifierUtil__clusterize()
  local neuronClusters = torch.Tensor(
  {{-1.3835e-01, -1.3216e-01},
  {-1.2597e-01, -1.2597e-01},
  {-1.2596e-01, -1.2352e-01},
  {-1.2107e-01, -1.1794e-01},
  {-1.1480e-01, -1.1291e-01},
  {-1.1101e-01, -1.0775e-01},
  {-1.0450e-01, -1.0356e-01},
  {-1.0262e-01, -1.0096e-01},
  {-9.9296e-02, -9.9296e-02},
  {-9.9296e-02, -9.9076e-02},
  {-9.8855e-02, -9.6902e-02},
  {-9.4950e-02, -9.3684e-02},
  {-9.2418e-02, -9.1326e-02},
  {-9.0234e-02, -8.9791e-02},
  {-8.9347e-02, -8.9300e-02},
  {-8.9252e-02, -8.8407e-02},
  {-8.7561e-02, -8.7212e-02},
  {-8.6863e-02, -8.6426e-02},
  {-8.5989e-02, -8.4519e-02},
  {-8.3049e-02, -8.2549e-02},
  {-8.2049e-02, -8.1537e-02},
  {-8.1026e-02, -8.0631e-02},
  {-8.0235e-02, -7.9836e-02},
  {-7.9437e-02, -7.9110e-02},
  {-7.8782e-02, -7.8645e-02},
  {-7.8507e-02, -7.8235e-02},
  {-7.7962e-02, -7.7836e-02},
  {-7.7710e-02, -7.7303e-02},
  {-7.6897e-02, -7.6776e-02},
  {-7.6654e-02, -7.6633e-02},
  {-7.6613e-02, -7.6564e-02},
  {-7.6515e-02, -7.6355e-02},
  {-7.6195e-02, -7.6187e-02},
  {-7.6180e-02, -7.6180e-02},
  {-7.6180e-02, -7.6180e-02},
  {-7.6180e-02, -7.6180e-02},
  {-7.6180e-02, -7.6180e-02},
  {-7.6180e-02, -7.6180e-02},
  {-7.6180e-02, -7.6180e-02},
  {-7.6180e-02, -7.6177e-02},
  {-7.6173e-02, -7.6067e-02},
  {-7.5961e-02, -7.5957e-02},
  {-7.5954e-02, -7.5908e-02},
  {-7.5862e-02, -7.5638e-02},
  {-7.5413e-02, -7.5337e-02},
  {-7.5260e-02, -7.4886e-02},
  {-7.4512e-02, -7.4420e-02},
  {-7.4329e-02, -7.4134e-02},
  {-7.3939e-02, -7.3856e-02},
  {-7.3774e-02, -7.3711e-02},
  {-7.3649e-02, -7.3281e-02},
  {-7.2914e-02, -7.2806e-02},
  {-7.2699e-02, -7.2695e-02},
  {-7.2691e-02, -7.2686e-02},
  {-7.2681e-02, -7.2658e-02},
  {-7.2634e-02, -7.2443e-02},
  {-7.2251e-02, -7.2162e-02},
  {-7.2074e-02, -7.2045e-02},
  {-7.2015e-02, -7.1829e-02},
  {-7.1642e-02, -7.1637e-02},
  {-7.1632e-02, -7.0864e-02},
  {-7.0095e-02, -7.0019e-02},
  {-6.9944e-02, -6.9795e-02},
  {-6.9647e-02, -6.9514e-02},
  {-6.9381e-02, -6.9374e-02},
  {-6.9367e-02, -6.9291e-02},
  {-6.9215e-02, -6.8831e-02},
  {-6.8448e-02, -6.8338e-02},
  {-6.8228e-02, -6.8153e-02},
  {-6.8079e-02, -6.7997e-02},
  {-6.7915e-02, -6.7580e-02},
  {-6.7245e-02, -6.7212e-02},
  {-6.7178e-02, -6.6639e-02},
  {-6.6101e-02, -6.6075e-02},
  {-6.6049e-02, -6.5999e-02},
  {-6.5950e-02, -6.5833e-02},
  {-6.5716e-02, -6.5531e-02},
  {-6.5346e-02, -6.4799e-02},
  {-6.4253e-02, -6.3899e-02},
  {-6.3545e-02, -6.3434e-02},
  {-6.3323e-02, -6.3076e-02},
  {-6.2830e-02, -6.2821e-02},
  {-6.2812e-02, -6.2672e-02},
  {-6.2531e-02, -6.2488e-02},
  {-6.2445e-02, -6.2284e-02},
  {-6.2122e-02, -6.1498e-02},
  {-6.0874e-02, -6.0553e-02},
  {-6.0232e-02, -6.0210e-02},
  {-6.0187e-02, -5.9970e-02},
  {-5.9752e-02, -5.9516e-02},
  {-5.9280e-02, -5.9269e-02},
  {-5.9259e-02, -5.8188e-02},
  {-5.7118e-02, -5.7050e-02},
  {-5.6982e-02, -5.6775e-02},
  {-5.6568e-02, -5.6473e-02},
  {-5.6379e-02, -5.6128e-02},
  {-5.5876e-02, -5.5703e-02},
  {-5.5530e-02, -5.5077e-02},
  {-5.4624e-02, -5.4327e-02},
  {-5.4030e-02, -5.3518e-02},
  {-5.3007e-02, -5.2689e-02},
  {-5.2371e-02, -5.2115e-02},
  {-5.1859e-02, -5.1045e-02},
  {-5.0230e-02, -5.0059e-02},
  {-4.9889e-02, -4.9789e-02},
  {-4.9689e-02, -4.9671e-02},
  {-4.9653e-02, -4.9304e-02},
  {-4.8954e-02, -4.8008e-02},
  {-4.7062e-02, -4.6935e-02},
  {-4.6807e-02, -4.6807e-02},
  {-4.6807e-02, -4.6060e-02},
  {-4.5313e-02, -4.4475e-02},
  {-4.3637e-02, -4.2930e-02},
  {-4.2223e-02, -4.1767e-02},
  {-4.1310e-02, -4.0122e-02},
  {-3.8933e-02, -3.8926e-02},
  {-3.8919e-02, -3.8505e-02},
  {-3.8092e-02, -3.7961e-02},
  {-3.7831e-02, -3.7036e-02},
  {-3.6241e-02, -3.5911e-02},
  {-3.5580e-02, -3.4536e-02},
  {-3.3491e-02, -3.3424e-02},
  {-3.3356e-02, -3.3070e-02},
  {-3.2784e-02, -3.2746e-02},
  {-3.2708e-02, -3.1932e-02},
  {-3.1157e-02, -3.0946e-02},
  {-3.0734e-02, -3.0608e-02},
  {-3.0482e-02, -3.0466e-02},
  {-3.0449e-02, -3.0174e-02},
  {-2.9899e-02, -2.9590e-02},
  {-2.9282e-02, -2.9077e-02},
  {-2.8871e-02, -2.8605e-02},
  {-2.8338e-02, -2.8181e-02},
  {-2.8025e-02, -2.7786e-02},
  {-2.7546e-02, -2.7390e-02},
  {-2.7234e-02, -2.6874e-02},
  {-2.6515e-02, -2.5612e-02},
  {-2.4709e-02, -2.4588e-02},
  {-2.4467e-02, -2.3847e-02},
  {-2.3227e-02, -2.2970e-02},
  {-2.2713e-02, -2.2394e-02},
  {-2.2074e-02, -2.1896e-02},
  {-2.1718e-02, -2.1583e-02},
  {-2.1448e-02, -2.1335e-02},
  {-2.1222e-02, -2.0469e-02},
  {-1.9716e-02, -1.9672e-02},
  {-1.9627e-02, -1.9571e-02},
  {-1.9515e-02, -1.9481e-02},
  {-1.9447e-02, -1.9246e-02},
  {-1.9046e-02, -1.8724e-02},
  {-1.8402e-02, -1.7865e-02},
  {-1.7329e-02, -1.7151e-02},
  {-1.6974e-02, -1.6919e-02},
  {-1.6865e-02, -1.6788e-02},
  {-1.6711e-02, -1.6386e-02},
  {-1.6061e-02, -1.6061e-02},
  {-1.6061e-02, -1.6061e-02},
  {-1.6061e-02, -1.5739e-02},
  {-1.5418e-02, -1.5379e-02},
  {-1.5340e-02, -1.5250e-02},
  {-1.5160e-02, -1.4820e-02},
  {-1.4481e-02, -1.4457e-02},
  {-1.4434e-02, -1.4277e-02},
  {-1.4120e-02, -1.3983e-02},
  {-1.3845e-02, -1.3742e-02},
  {-1.3639e-02, -1.3496e-02},
  {-1.3354e-02, -1.3144e-02},
  {-1.2934e-02, -1.2540e-02},
  {-1.2145e-02, -1.2019e-02},
  {-1.1893e-02, -1.1699e-02},
  {-1.1506e-02, -1.0978e-02},
  {-1.0450e-02, -1.0133e-02},
  {-9.8158e-03, -9.7125e-03},
  {-9.6092e-03, -9.4671e-03},
  {-9.3250e-03, -9.2860e-03},
  {-9.2470e-03, -9.1870e-03},
  {-9.1271e-03, -8.5389e-03},
  {-7.9507e-03, -7.9092e-03},
  {-7.8676e-03, -7.8577e-03},
  {-7.8478e-03, -7.6750e-03},
  {-7.5021e-03, -7.4672e-03},
  {-7.4324e-03, -7.1014e-03},
  {-6.7704e-03, -6.7324e-03},
  {-6.6945e-03, -6.5932e-03},
  {-6.4918e-03, -6.3875e-03},
  {-6.2831e-03, -6.1199e-03},
  {-5.9568e-03, -5.9414e-03},
  {-5.9261e-03, -5.8075e-03},
  {-5.6890e-03, -5.5283e-03},
  {-5.3675e-03, -5.2920e-03},
  {-5.2166e-03, -4.7274e-03},
  {-4.2382e-03, -4.0421e-03},
  {-3.8460e-03, -3.8211e-03},
  {-3.7961e-03, -3.7400e-03},
  {-3.6839e-03, -3.6115e-03},
  {-3.5390e-03, -3.5325e-03},
  {-3.5259e-03, -3.3951e-03},
  {-3.2642e-03, -3.2051e-03},
  {-3.1460e-03, -3.0987e-03},
  {-3.0513e-03, -2.8845e-03},
  {-2.7178e-03, -2.6932e-03},
  {-2.6686e-03, -2.4631e-03},
  {-2.2577e-03, -2.1541e-03},
  {-2.0505e-03, -1.9842e-03},
  {-1.9179e-03, -1.7981e-03},
  {-1.6783e-03, -1.6272e-03},
  {-1.5761e-03, -1.5627e-03},
  {-1.5494e-03, -1.5242e-03},
  {-1.4991e-03, -1.4666e-03},
  {-1.4341e-03, -1.4203e-03},
  {-1.4066e-03, -1.2876e-03},
  {-1.1686e-03, -1.1432e-03},
  {-1.1178e-03, -8.7755e-04},
  {-6.3727e-04, -5.9218e-04},
  {-5.4709e-04, -5.3435e-04},
  {-5.2160e-04, -5.2160e-04},
  {-5.2160e-04, -2.3384e-04},
  {5.3932e-05, 3.6276e-04},
  {6.7159e-04, 7.1590e-04},
  {7.6022e-04, 1.0820e-03},
  {1.4038e-03, 1.6826e-03},
  {1.9614e-03, 2.0562e-03},
  {2.1510e-03, 2.4379e-03},
  {2.7248e-03, 2.8377e-03},
  {2.9505e-03, 3.0017e-03},
  {3.0530e-03, 3.1474e-03},
  {3.2418e-03, 3.3641e-03},
  {3.4863e-03, 3.5625e-03},
  {3.6386e-03, 3.6388e-03},
  {3.6390e-03, 3.6986e-03},
  {3.7582e-03, 3.8962e-03},
  {4.0342e-03, 4.1440e-03},
  {4.2539e-03, 4.4026e-03},
  {4.5514e-03, 4.5933e-03},
  {4.6353e-03, 4.7394e-03},
  {4.8436e-03, 4.8966e-03},
  {4.9496e-03, 4.9590e-03},
  {4.9683e-03, 5.1629e-03},
  {5.3574e-03, 5.4043e-03},
  {5.4512e-03, 5.5357e-03},
  {5.6203e-03, 5.6477e-03},
  {5.6752e-03, 5.7265e-03},
  {5.7778e-03, 5.9018e-03},
  {6.0257e-03, 6.0554e-03},
  {6.0850e-03, 6.1464e-03},
  {6.2078e-03, 6.3016e-03},
  {6.3954e-03, 6.3990e-03},
  {6.4025e-03, 6.5058e-03},
  {6.6090e-03, 6.7719e-03},
  {6.9347e-03, 6.9971e-03},
  {7.0596e-03, 7.0681e-03},
  {7.0766e-03, 7.6031e-03},
  {8.1297e-03, 8.1820e-03},
  {8.2343e-03, 8.2880e-03},
  {8.3417e-03, 8.3522e-03},
  {8.3627e-03, 8.3975e-03},
  {8.4323e-03, 8.4495e-03},
  {8.4667e-03, 8.5524e-03},
  {8.6380e-03, 8.7902e-03},
  {8.9423e-03, 8.9642e-03},
  {8.9862e-03, 9.0542e-03},
  {9.1223e-03, 9.1235e-03},
  {9.1246e-03, 9.1730e-03},
  {9.2215e-03, 9.3114e-03},
  {9.4012e-03, 9.4025e-03},
  {9.4037e-03, 9.4137e-03},
  {9.4236e-03, 9.4381e-03},
  {9.4526e-03, 9.6589e-03},
  {9.8652e-03, 9.9881e-03},
  {1.0111e-02, 1.0125e-02},
  {1.0139e-02, 1.0162e-02},
  {1.0185e-02, 1.0251e-02},
  {1.0316e-02, 1.0366e-02},
  {1.0416e-02, 1.0499e-02},
  {1.0582e-02, 1.0756e-02},
  {1.0930e-02, 1.0937e-02},
  {1.0944e-02, 1.0997e-02},
  {1.1050e-02, 1.1209e-02},
  {1.1368e-02, 1.1404e-02},
  {1.1441e-02, 1.1452e-02},
  {1.1463e-02, 1.1518e-02},
  {1.1573e-02, 1.1661e-02},
  {1.1749e-02, 1.1833e-02},
  {1.1916e-02, 1.1923e-02},
  {1.1930e-02, 1.1940e-02},
  {1.1949e-02, 1.1998e-02},
  {1.2046e-02, 1.2051e-02},
  {1.2055e-02, 1.2127e-02},
  {1.2199e-02, 1.2213e-02},
  {1.2226e-02, 1.2317e-02},
  {1.2408e-02, 1.2492e-02},
  {1.2576e-02, 1.2669e-02},
  {1.2762e-02, 1.2849e-02},
  {1.2936e-02, 1.3000e-02},
  {1.3063e-02, 1.3063e-02},
  {1.3063e-02, 1.3063e-02},
  {1.3063e-02, 1.3063e-02},
  {1.3063e-02, 1.3063e-02},
  {1.3063e-02, 1.3063e-02},
  {1.3063e-02, 1.3063e-02},
  {1.3063e-02, 1.3063e-02},
  {1.3063e-02, 1.3063e-02},
  {1.3063e-02, 1.3063e-02},
  {1.3063e-02, 1.3063e-02},
  {1.3063e-02, 1.3063e-02},
  {1.3063e-02, 1.3063e-02},
  {1.3063e-02, 1.3063e-02},
  {1.3063e-02, 1.3063e-02},
  {1.3063e-02, 1.3063e-02},
  {1.3063e-02, 1.3063e-02},
  {1.3063e-02, 1.3063e-02},
  {1.3063e-02, 1.3063e-02},
  {1.3063e-02, 1.3063e-02},
  {1.3063e-02, 1.3063e-02},
  {1.3063e-02, math.huge}})

  local mergedClusters = activationModifierUtil.clusterize(neuronClusters, 1.95)
  mytester:asserteq(1, mergedClusters:size(1), 'Wrong merged cluster count')
  mytester:assertalmosteq(-0.028460533073016, mergedClusters[1][1], precision, 'Wrong merged cluster center')
  mytester:asserteq(math.huge * -1, mergedClusters[1][2], 'Wrong merged cluster bound')
  
  mergedClusters = activationModifierUtil.clusterize(neuronClusters, 1.4625)
  mytester:asserteq(1, mergedClusters:size(1), 'Wrong merged cluster count')
  mytester:assertalmosteq(-2.8461e-02, mergedClusters[1][1], precision, 'Wrong merged cluster center')
  
  mergedClusters = activationModifierUtil.clusterize(neuronClusters, 0.82265625)
  mytester:asserteq(1, mergedClusters:size(1), 'Wrong merged cluster count')
  mytester:assertalmosteq(-2.8461e-02, mergedClusters[1][1], precision, 'Wrong merged cluster center')
  
  mergedClusters = activationModifierUtil.clusterize(neuronClusters, 0.0082451510412284)
  mergedClusters[2][2] = 1000 -- assertTensorEq does not likes (math.huge * -1)
  local epxectedMergedClusters = torch.Tensor({{-1.3835e-01,  -8.3230e-02},
                                               {-2.8111e-02,  1000}})
  mytester:assertTensorEq(mergedClusters, epxectedMergedClusters, precision, ' Merged clusters tensor is not matching expected one')
  mytester:asserteq(2, mergedClusters:size(1), 'Wrong merged cluster count')
  
  mergedClusters = activationModifierUtil.clusterize(neuronClusters, 0.0061838632809213)
  mergedClusters[4][2] = 1000 -- assertTensorEq does not likes (math.huge * -1)
  local epxectedMergedClusters = torch.Tensor({{-1.3835e-01,  -1.3134e-01},
                                               {-1.2433e-01,  -1.1862e-01},
                                               {-1.1291e-01,  -6.9766e-02},
                                               {-2.6628e-02,  1000}})
  mytester:assertTensorEq(mergedClusters, epxectedMergedClusters, precision, ' Merged clusters tensor is not matching expected one')
  mytester:asserteq(4, mergedClusters:size(1), 'Wrong merged cluster count')
  
  mergedClusters = activationModifierUtil.clusterize(neuronClusters, 0.004637897460691)
  mergedClusters[5][2] = 1000 -- assertTensorEq does not likes (math.huge * -1)
  local epxectedMergedClusters = torch.Tensor({{-1.3835e-01,  -1.3216e-01},
                                               {-1.2596e-01,  -1.2352e-01},
                                               {-1.2107e-01,  -1.1699e-01},
                                               {-1.1291e-01,  -6.9766e-02},
                                               {-2.6628e-02,  1000}})
  mytester:assertTensorEq(mergedClusters, epxectedMergedClusters, precision, ' Merged clusters tensor is not matching expected one')
  mytester:asserteq(5, mergedClusters:size(1), 'Wrong merged cluster count')
  
  mergedClusters = activationModifierUtil.clusterize(neuronClusters, 0.0034784230955182)
  mergedClusters[7][2] = 1000 -- assertTensorEq does not likes (math.huge * -1)
  local epxectedMergedClusters = torch.Tensor({ {-1.3835e-01,  -1.3216e-01},
                                                {-1.2596e-01,  -1.2352e-01},
                                                {-1.2107e-01,  -1.1793e-01},
                                                {-1.1480e-01,  -1.1291e-01},
                                                {-1.1101e-01,  -1.0596e-01},
                                                {-1.0091e-01,  -6.3160e-02},
                                                {-2.5406e-02,  1000}})
  mytester:assertTensorEq(mergedClusters, epxectedMergedClusters, precision, ' Merged clusters tensor is not matching expected one')
  mytester:asserteq(7, mergedClusters:size(1), 'Wrong merged cluster count')
  
  mergedClusters = activationModifierUtil.clusterize(neuronClusters, 0.0026088173216387)
  mergedClusters[9][2] = 1000 -- assertTensorEq does not likes (math.huge * -1)
  local epxectedMergedClusters = torch.Tensor({ {-1.3835e-01,  -1.3216e-01},
                                                {-1.2596e-01,  -1.2352e-01},
                                                {-1.2107e-01,  -1.1793e-01},
                                                {-1.1480e-01,  -1.1291e-01},
                                                {-1.1101e-01,  -1.0728e-01},
                                                {-1.0356e-01,  -1.0135e-01},
                                                {-9.9149e-02,  -9.4363e-02},
                                                {-8.9577e-02,  -5.6624e-02},
                                                {-2.3671e-02,  1000}})
  mytester:assertTensorEq(mergedClusters, epxectedMergedClusters, precision, ' Merged clusters tensor is not matching expected one')
  mytester:asserteq(9, mergedClusters:size(1), 'Wrong merged cluster count')
  
  mergedClusters = activationModifierUtil.clusterize(neuronClusters, 0.001956612991229)
  mergedClusters[14][2] = 1000 -- assertTensorEq does not likes (math.huge * -1)
  local epxectedMergedClusters = torch.Tensor({ {-1.3835e-01,  -1.3216e-01},
                                                {-1.2596e-01,  -1.2352e-01},
                                                {-1.2107e-01,  -1.1793e-01},
                                                {-1.1480e-01,  -1.1291e-01},
                                                {-1.1101e-01,  -1.0728e-01},
                                                {-1.0356e-01,  -1.0135e-01},
                                                {-9.9149e-02,  -9.7050e-02},
                                                {-9.4950e-02,  -9.3684e-02},
                                                {-9.2418e-02,  -9.0313e-02},
                                                {-8.8208e-02,  -7.9722e-02},
                                                {-7.1237e-02,  -6.0965e-02},
                                                {-5.0692e-02,  -4.4146e-02},
                                                {-3.7599e-02,  -1.9869e-02},
                                                {-2.1387e-03,  1000}})
  mytester:assertTensorEq(mergedClusters, epxectedMergedClusters, precision, ' Merged clusters tensor is not matching expected one')
  mytester:asserteq(14, mergedClusters:size(1), 'Wrong merged cluster count')

end

function nnpruntest.ActivationModifier_getClusterCenter()
  local clusters = torch.Tensor({{0.0825, 0.165}, {0.2825, 0.4}, {0.5525, 0.705}, {0.8525, 1}})
  mytester:asserteq(0.0825, nn.ActivationModifier().getClusterCenter(0.0, clusters, 0.0), 'Wrong cluster center found')
  mytester:asserteq(0.0825, nn.ActivationModifier().getClusterCenter(0.165, clusters, 0.0), 'Wrong cluster center found')
  mytester:asserteq(0.2825, nn.ActivationModifier().getClusterCenter(0.166, clusters, 0.0), 'Wrong cluster center found')
  mytester:asserteq(0.5525, nn.ActivationModifier().getClusterCenter(0.5, clusters, 0.0), 'Wrong cluster center found')
  mytester:asserteq(0.8525, nn.ActivationModifier().getClusterCenter(1, clusters, 0.0), 'Wrong cluster center found')
end

--===================================================================

function nnpruntest.tableUtil_contains()
  t = {1, 2, 3}
  mytester:assert(tableUtil.contains(t,1), ' 1 must be in the list')
  mytester:assert(tableUtil.contains(t,2), ' 2 must be in the list')
  mytester:assert(tableUtil.contains(t,3), ' 3 must be in the list')
  mytester:assert(not tableUtil.contains(t,4), ' 4 must NOT be in the list')
end

function nnpruntest.tableUtil_frequencies()
  t = {1, 1, 1, 2, 2, 3}
  fr = tableUtil.frequencies(t)
  mytester:assert(fr[1] == 3, ' 1 frequency is 3')
  mytester:assert(fr[2] == 2, ' 2 frequency is 2')
  mytester:assert(fr[3] == 1, ' 3 frequency is 1')
  mytester:assert(#fr == 3, ' must be total 3 entries in the table')
end

function nnpruntest.tableUtil_getSize()
  mytester:asserteq(tableUtil.getSize({}), 0)
  mytester:asserteq(tableUtil.getSize({1}), 1)
  mytester:asserteq(tableUtil.getSize({[1]="red", [2]="green", [3]="blue"}), 3)
end

function nnpruntest.tableUtil_uniqueTableValues()
  t = {10, 10, 30, 30, 40, 40, 20, 20}
  unique, ic = tableUtil.uniqueTableValues(t)
  
  mytester:assert(tableUtil.contains(unique,10), ' 10 must be in the list')
  mytester:assert(tableUtil.contains(unique,20), ' 20 must be in the list')
  mytester:assert(tableUtil.contains(unique,30), ' 30 must be in the list')
  mytester:assert(tableUtil.contains(unique,40), ' 40 must be in the list')
  mytester:asserteq(#unique, 4, ' there should be 4 unique values')
  
  for k,v in pairs(ic) do
    mytester:asserteq(unique[v], t[k], ' so ic should contain indexes of unique set mapped to original table') 
  end
end

--===================================================================

function nnpruntest.dataUtil_cvpartitionKFold_y()
  y = {0,0,0,2,2,2,2,2,2,1,1,1,1,1,1,1,1,1,1,1,1} -- 3, 5, 9 => 17
  folds = 4
  C = dataUtil.cvpartitionKFold(y, folds)
  mytester:asserteq(torch.Tensor(y)[C.getTestFold(1)]:size(1), C.TestSize[1], ' test size Fold1 must match')
  mytester:asserteq(torch.Tensor(y)[C.getTrainFold(1)]:size(1), C.TrainSize[1], ' train size Fold1 must match')
  mytester:asserteq(torch.Tensor(y)[C.getTestFold(4)]:size(1), C.TestSize[4], ' test size Fold4 must match')
  mytester:asserteq(torch.Tensor(y)[C.getTrainFold(4)]:size(1), C.TrainSize[4], ' train size Fold4 must match')
  
  mytester:asserteq(#y, torch.Tensor(C.TestSize):sum(), ' all test indexes sets should be equal to full set')
end

--===================================================================

function nnpruntest.mathUtil_tensorEntropy()
  -- tensors should not be 1D 
  -- use torch.Tensor({1,2,3,4}):resize(4,1) instead of torch.Tensor({1,2,3,4})
  
  local e1 = mathUtil.tensorEntropy(torch.Tensor({{1,1},{1,0},{1,0},{1,0},{1,1},{1,1},{1,1},{1,1},{1,1},{1,0},{1,0},{1,1},{1,1},{1,1}}))
  local e2 = mathUtil.tensorEntropy(torch.Tensor({{1,2},{3,4}}))
  local e3 = mathUtil.tensorEntropy(torch.Tensor({{0,0},{0,0}}))
  local e4_1 = mathUtil.tensorEntropy(torch.Tensor({1,2,3,4}):resize(4,1))
  local e4_2 = mathUtil.tensorEntropy(torch.Tensor({{1,2},{3,4},{5,6},{7,8}}))
  
  mytester:assert(0.94028595867062 < e1 and e1 < 0.94028595867064, ' entropy must be 0.94028595867063')
  mytester:asserteq(e2, 1, ' entropy must be one')
  mytester:asserteq(e3, 0, ' entropy must be zero')
  mytester:asserteq(e4_1, 2, ' entropy must be two')
  mytester:asserteq(e4_1, e4_2, ' entropies must be equal')
end

function nnpruntest.mathUtil_tensorInformationGain()
  -- in tensor t class is defined by the value of middle value in each row, first element is always 1, last is always 0 
  local t = torch.Tensor({{1,1,0},{1,0,0},{1,0,0},{1,0,0},{1,1,0},{1,1,0},{1,1,0},{1,1,0},{1,1,0},{1,0,0},{1,0,0},{1,1,0},{1,0,0},{1,0,0}})
  local y = torch.Tensor({{1},    {0},    {0},    {0},    {1},    {1},    {1},    {1},    {1},    {0},    {0},    {1},    {0},    {0}})
  -- y is our tensor holding classes. It can be represented by several columns.
  
  -- Let's measure info gain of middle split (0.5) on columns 1 and 2
  local iGainCol1 = mathUtil.tensorInformationGain(t, y, 1, 0.5)
  local iGainCol2 = mathUtil.tensorInformationGain(t, y, 2, 0.5)
  local iGainCol3 = mathUtil.tensorInformationGain(t, y, 3, 0.5)
  
  mytester:asserteq(iGainCol1, 1, ' information gain must be one')
  mytester:asserteq(iGainCol2, 0, ' information gain must be zero')
  mytester:asserteq(iGainCol3, 1, ' information gain must be one')
end

function nnpruntest.mathUtil_scaleValue()
  mytester:asserteq(mathUtil.scaleValue(10, 0, 10, 0, 1), 1, ' maximum from old range must be transformed to maximum in new range')
  mytester:asserteq(mathUtil.scaleValue(0, 0, 1, 1, 2), 1, ' minimum from old range must be transformed to minimum in new range')
  mytester:asserteq(mathUtil.scaleValue(0.5, 0, 1, 0, 10), 5, ' 0.5 from range [0;1] is 5 in new range [0;10]')
  mytester:asserteq(mathUtil.scaleValue(-2, -3, 7, 0, 1), 0.1, ' -2 from range [-3;+7] is 0.1 in new range [0;1]')
end

--===================================================================

function nnpruntest.tensorUtil_uniqueTensorValues()
  t = {10, 10, 30, 30, 40, 40, 20, 20}
  unique, ic = tensorUtil.uniqueTensorValues(torch.Tensor(t))
  
  mytester:assert(tensorUtil.contains(torch.Tensor(unique),10), ' 10 must be in the list')
  mytester:assert(tensorUtil.contains(torch.Tensor(unique),20), ' 20 must be in the list')
  mytester:assert(tensorUtil.contains(torch.Tensor(unique),30), ' 30 must be in the list')
  mytester:assert(tensorUtil.contains(torch.Tensor(unique),40), ' 40 must be in the list')
  mytester:asserteq(#unique, 4, ' there should be 4 unique values')
  
  for k,v in pairs(ic) do
    mytester:asserteq(unique[v], t[k], ' so ic should contain indexes of unique set mapped to original table') 
  end
end

function nnpruntest.tensorUtil_findNearestElements()
  local t = torch.Tensor({0.1, 0.2, 0.25, 0.4, 0.44, 0.5, 0.9, 1.1})
  local d, i1, i2 = tensorUtil.findNearestElements(t)
  
  mytester:assert(math.abs(0.04 - d) < precision, 'wrong minimal distance found, should be 0.04')
  mytester:asserteq(4, i1, 'wrong index of first element found')
  mytester:asserteq(5, i2, 'wrong index of second element found')
end

function nnpruntest.tensorUtil_findNearestElements_fromEnd()
  local t = torch.Tensor({0.1, 0.2, 0.25, 0.4, 0.445, 0.5, 0.9, 1.4, 1.44})
  local d, i1, i2 = tensorUtil.findNearestElements(t, true)
  
  mytester:assert(math.abs(0.04 - d) < precision, 'wrong minimal distance found, should be 0.04')
  mytester:asserteq(8, i1, 'wrong index of first element found')
  mytester:asserteq(9, i2, 'wrong index of second element found')
end

function nnpruntest.tensorUtil_delete2DTensorRow()
  local t = torch.Tensor({0.1, 0.2, 0.25, 0.4, 0.44, 0.5, 0.9, 1.1})
  local len = t:size()[1]
  local twoDTensor = t:resize(len,1):expand(len,2)
  local t2 = tensorUtil.delete2DTensorRow(twoDTensor, 6);

  mytester:assert(t2:eq(0.5):sum() == 0, '0.5 is not removed')
  mytester:assert(t2:size()[1] == len-1, 'rows number did not changed')
end

function nnpruntest.tensorUtil_tensorsEqual()
  local t = torch.Tensor({0.1, 0.2, 0.25, 0.4, 0.44, 0.5, 0.9, 1.1})
  
  mytester:assert(true == tensorUtil.tensorsEqual(t, t:clone()))
  mytester:assert(false == tensorUtil.tensorsEqual(torch.Tensor({1,2,3,4}), torch.Tensor({{1,2},{3,4}})))
  mytester:assert(true == tensorUtil.tensorsEqual(torch.Tensor({{1,2},{3,4}}), torch.Tensor({{1,2},{3,4}})))
  mytester:assert(false == tensorUtil.tensorsEqual(torch.Tensor({0.8000, -0.8000}), torch.Tensor({-0.8000, 0.8000})))
end

function nnpruntest.tensorUtil_tensorsEqual2()
  local t = torch.Tensor({0.1, 0.2, 0.25, 0.4, 0.44, 0.5, 0.9, 1.1})
  
  mytester:assert(true == tensorUtil.tensorsEqual2(t, t:clone()))
  mytester:assert(true == tensorUtil.tensorsEqual2(torch.Tensor({1,2,3,4}), torch.Tensor({{1,2},{3,4}})))
  mytester:assert(true == tensorUtil.tensorsEqual2(torch.Tensor({{1,2},{3,4}}), torch.Tensor({{1,2},{3,4}})))
  mytester:assert(false == tensorUtil.tensorsEqual2(torch.Tensor({0.8000, -0.8000}), torch.Tensor({-0.8000, 0.8000})))
end

function nnpruntest.tensorUtil_storagesEqual()
  local t = torch.Tensor({0.1, 0.2, 0.25, 0.4, 0.44, 0.5, 0.9, 1.1})
  local t2 = torch.Tensor({0.1, 0.2, 0.25, 0.4, 0.44, 0.5, 0.9})
  
  mytester:assert(true == tensorUtil.storagesEqual(t:size(), t:size()))
  mytester:assert(false == tensorUtil.storagesEqual(t:size(), t2:size()))
  mytester:assert(false == tensorUtil.storagesEqual(torch.Tensor({1,2,3,4}):size(), torch.Tensor({{1,2},{3,4}}):size()))
  mytester:assert(true == tensorUtil.storagesEqual(torch.Tensor({{1,2},{3,4}}):size(), torch.Tensor({{1,2},{3,4}}):size()))
end

function nnpruntest.tensorUtil_tensorContainsRow()
  local t = torch.Tensor({{0.1, 0.2}, {0.25, 0.4}, {0.44, 0.5}})
  
  mytester:assert(true == tensorUtil.tensorContainsRow(torch.Tensor({0.1, 0.2}), t))
  mytester:assert(false == tensorUtil.tensorContainsRow(torch.Tensor({0.1, 0.1}), t))
  mytester:assert(true == tensorUtil.tensorContainsRow(torch.Tensor({0.44, 0.5}), t))
end

function nnpruntest.tensorUtil_uniqueTensorRows()
  local tensor = torch.Tensor({{0,1}, {0,0}, {1,0}, {1,1}, {0,1}, {1,1}, {0,1}, {1,0}})
  local expectedUniqueTensor = torch.Tensor({{0,1}, {0,0}, {1,0}, {1,1}})
  local expectedFrequencies = {[1]=3, [2]=1, [3]=2, [4]=2}
  
  local uniqueRowsTensor, frequencies = tensorUtil.rowsFrequencies(tensor)
  
  mytester:assertTensorEq(uniqueRowsTensor, expectedUniqueTensor, precision, ' - Unique rows tensor should contain only unique rows.')
  for k,v in pairs(frequencies) do
    mytester:asserteq(expectedFrequencies[k], v)
  end
end

function nnpruntest.tensorUtil_scaleTensor()
  local tensor = torch.Tensor({{1,2,3,4,5}, {6,7,8,9,10}})
  local expectedTensor = torch.Tensor({ {0, 0.11111111111111, 0.22222222222222, 0.33333333333333, 0.44444444444444}, {0.55555555555555, 0.6666666666666, 0.7777777777777, 0.88888888888888, 1 } })
  
  local scaledTensor = tensorUtil.scaleTensor(tensor, 0, 1)
  
  mytester:assertTensorEq(expectedTensor, scaledTensor, precision, ' - Expected tensor should be equal to scaled tensor.')
end

--===================================================================

function nnpruntest.runningUtil_kFold_run()
  local data = csvUtil.loadTest2dData()
  local X = data[{ {}, {1,-2} }]
  local labels = data[{ {}, {-1} }]
  local targetLabels, classesNumber = dataUtil.labelsColumnToMatrix(labels, -1, 1) -- (-1 + 1) for tanh, (0, +1) for sigmoid
  local dataset = dataUtil.convertToDataset(X, targetLabels)
  local labels2Targets = {} 
  for i = 1, labels:size(1) do
    labels2Targets[labels[i]] = targetLabels[i]  
  end
  
  local classNames = {}
  for i = 1, classesNumber do
    classNames[i] = "Class"..i  
  end
  
  -- ANN parameters
  local inputs = dataset[1][1]:size(1) 
  local outputs = dataset[1][2]:size(1) 
  local HUs = 5 
  local learnRate = 0.01
  
  local mlp = nn.Sequential()
  mlp:add( nn.NeuronPruningLayer('tanh', inputs) ) -- Input layer pruning
  mlp:add( nn.Linear(inputs, HUs) ) -- 10 input, 20 hidden units
  mlp:add( nn.Tanh() )
  mlp:add( nn.NeuronPruningLayer('tanh', HUs) ) -- Hidden layer pruning
  mlp:add( nn.Linear(HUs, outputs) ) -- 1 output
  
  local criterion = nn.MSECriterion()  

  local opt = {}
  opt.batchSize = 10
  opt.optimization = 'SGD'
  opt.learningRate = 0.05
  opt.momentum = 0.0
  opt.maxEpochs = 25
  opt.learningRate = 0.02
  opt.coefL1 = 0.0
  opt.coefL2 = 0.0
  
  local trainTestTrainer = {
    criterion = criterion, 
    classNames = classNames, 
    mlp = mlp, 
    saveModels = false, -- if true all trained models will be saved 
    verbose = false     -- will print accuracy if true
  }
    
  trainTestTrainer.trainTest =   
    function(thiz, _model, _classNames, _trainData, _testData)
      local trainConf = runningUtil.train(_model, criterion, _trainData, _classNames, opt)
      local testConf = runningUtil.test(_model, _testData, _classNames, opt.batchSize)
      trainConf:updateValids()
      testConf:updateValids()
      print('trainConf '.. trainConf:__tostring__())
      print('testConf '.. testConf:__tostring__())
    end  
  
  runningUtil.runXVal(dataset, labels, trainTestTrainer, 5, 2)
end

function nnpruntest.runningUtil_TestTrain_run()
  local data = csvUtil.loadTest2dData()
  local X = data[{ {}, {1,-2} }]
  local labels = data[{ {}, {-1} }]
  local targetLabels, classesNumber = dataUtil.labelsColumnToMatrix(labels, -1, 1) -- (-1 + 1) for tanh, (0, +1) for sigmoid
  local dataset = dataUtil.convertToDataset(X, targetLabels)
  
  local classNames = {}
  for i = 1, classesNumber do
    classNames[i] = "Class"..i  
  end
  
  -- ANN parameters
  local inputs = dataset[1][1]:size(1) 
  local outputs = dataset[1][2]:size(1) 
  local HUs = 5 
  local learnRate = 0.01
  
  local mlp = nn.Sequential()
  mlp:add( nn.NeuronPruningLayer('tanh', inputs) ) -- Input layer pruning
  mlp:add( nn.Linear(inputs, HUs) ) -- 10 input, 20 hidden units
  mlp:add( nn.Tanh() )
  mlp:add( nn.NeuronPruningLayer('tanh', HUs) ) -- Hidden layer pruning
  mlp:add( nn.Linear(HUs, outputs) ) -- 1 output
  
  local criterion = nn.MSECriterion()  
  
  local opt = {}
  opt.batchSize = 10
  opt.optimization = 'SGD'
  opt.learningRate = 0.05
  opt.momentum = 0.0
  opt.maxEpochs = 25
  opt.learningRate = 0.02
  opt.coefL1 = 0.0
  opt.coefL2 = 0.0
  
  -- log results to files
--  trainLogger = optim.Logger(paths.concat(opt.save, 'train.log'))
--  testLogger = optim.Logger(paths.concat(opt.save, 'test.log'))
  
  -- let's split data into test/train datasets
  local foldsN = 5
  local C = dataUtil.cvpartitionKFold(tableUtil.flatten(labels:totable()), foldsN)
  local fN = 1
  local trainIndexes = C.getTrainFold(fN)
  local testIndexes = C.getTestFold(fN)
  local trainData = tableUtil.rows(dataset, trainIndexes)
  local testData = tableUtil.rows(dataset, testIndexes)

  local trainTestTrainer = {
    criterion = criterion, 
    classNames = classNames, 
    testData = testData,
    trainData = trainData,
    mlp = mlp, 
    saveModels = false, -- if true all trained models will be saved 
    verbose = false     -- will print accuracy if true
  }
  
  trainTestTrainer.trainTest =   
    function(thiz, _model, _classNames, _trainData, _testData)
      local trainConf = runningUtil.train(_model, criterion, _trainData, _classNames, opt)
      local testConf = runningUtil.test(_model, _testData, _classNames, opt.batchSize)
      trainConf:updateValids()
      testConf:updateValids()
      print('trainConf '.. trainConf:__tostring__())
      print('testConf '.. testConf:__tostring__())
    end
  
  runningUtil.runTestTrain(trainTestTrainer, 5)
end

--===================================================================

function nnpruntest.tensor_is_binary()
  mytester:asserteq(tensor_is_binary(torch.Tensor({{1,2}, {7,8}})), false, ' - tensor [1,2;7,8] is not binary')
  mytester:asserteq(tensor_is_binary(torch.Tensor({{0,0}, {-1,0}})), false, ' - tensor [0,0;-1,0] is not binary ')
  mytester:asserteq(tensor_is_binary(torch.Tensor({{0,2}, {1,0}})), false, ' - tensor [0,2;1,0] is not binary ')
  mytester:asserteq(tensor_is_binary(torch.Tensor({{0,1}, {1,0}})), true, ' - tensor [0,1;1,0] is binary')
  mytester:asserteq(tensor_is_binary(torch.Tensor({{1,1}, {1,1}})), true, ' - tensor [1,1;1,1] is binary')
  mytester:asserteq(tensor_is_binary(torch.Tensor({{0,0}, {0,0}})), true, ' - tensor [0,0;0,0] is binary')
end

--===================================================================

function nnpruntest.NeuronPruningLayer_prunningIsWorking()
  --local badMask = torch.Tensor({{1,2}, {7,8}})
  --mytester:assertError(nn.NeuronPruningLayer(badMask), ' - tensor [1,2;7,8] is not binary ') -- tester does not catches error
  
  -- Here we mailnly testing setters / getters. Making sure we are using clones where it is needed. 
  -- So that nobody from outside can influence our state.
  local mask = torch.ByteTensor({0, 1, 1, 0}):resize(1,4)
  local npruning = nn.NeuronPruningLayer('Tanh', 4)
  npruning:setPruningMask(mask)
  local returnedMask = npruning:getPruningMask()
  
  mytester:assert(returnedMask ~= nil, ' - Object returned nil')
  mytester:assertTensorEq(returnedMask:double(), mask:double(), precision, ' - NeuronPruningLayer layer should return same pruning mask')
  
  mask:zero()
  originalMask = torch.ByteTensor({0, 1, 1, 0}):resize(1,4)
  mytester:assertTensorEq(npruning:getPruningMask():double(), originalMask:double(), precision, ' - Seems dropout have returned prunning mask by reference')

  npruning:setPruningMask(mask)
  mytester:assertTensorEq(npruning:getPruningMask():double(), torch.zeros(1, 4), precision, ' - NeuronPruningLayer layer should have zero mask now')
  mask[1][1] = 1
  mytester:assertne(npruning:getPruningMask()[1][1], mask[1][1], ' - Seems we are still referencing old object') -- tester cannot compare tensors for not equality!  
  
  local otherMask = npruning:getPruningMask()
  otherMask[1][1] = 1
  mytester:assertne(npruning:getPruningMask()[1][1], otherMask[1][1], ' - Seems we are still referencing old object') -- tester cannot compare tensors for not equality!
  
  -- Test zeroVal from 'sigmoid' is equal to 0.5
  mytester:asserteq(nn.NeuronPruningLayer('Sigmoid', 4):getZeroVal(), 0.5, ' - Seems Sigmoid was not transformed into 0.5 zero value')
  -- Test zeroVal from 'tanh' is equal to 0.0
  mytester:asserteq(nn.NeuronPruningLayer('Tanh', 4):getZeroVal(), 0, ' - Seems Tanh was not transformed into 0.0 zero value')
  
  -- Test getOutput pruning
  local input = torch.range(1,4):double()
  local zeroVal = 0.5
  npruning = nn.NeuronPruningLayer(zeroVal, 4)
  local output = npruning:updateOutput(input)
  mytester:assertTensorEq(input, output, precision, ' - With default pruning mask (no pruned neurons) input should stay non-modified')
  
  npruning:setPruningMask(torch.ByteTensor({0, 1, 1, 0}):resize(1,4))
--  npruning:updateOutput(torch.range(1,6):double():resize(2,3))
--  mytester:assertError(npruning:updateOutput(input), ' - error should be thrown here as dimensions of input/mask does not match')
  local input = torch.range(1,4):double():resize(1,4)
  local output = npruning:updateOutput(input)
  mytester:assertTensorEq(torch.DoubleTensor({1,zeroVal, zeroVal,4}):resize(1,4), output, precision, ' - Pruning should be applied to output')
  
  -- Test getGradInput pruning
  local input = torch.range(1,4):double():resize(1,4)
  local gradInput = npruning:updateGradInput(input, input)
  mytester:assertTensorEq(torch.DoubleTensor({1, 0, 0,4}):resize(1,4), gradInput, precision, ' - Pruning should be applied to gradInput')
end

function nnpruntest.test_activationModifierUtil_clusterize()
  -- first column cluster centers (or initially just neuron activations), second column - boundaries - mean values between clusters 
  local initialClusters = torch.Tensor({{10,9},{8,4.5},{1,0.5},{0,-0.5},{-1,-4.5}, {-8,-9}, {-10, -1*math.huge}})
  
  local epsilon = 3
  local mergedClusters1 = activationModifierUtil.clusterize(initialClusters, epsilon)
  mytester:asserteq(mergedClusters1:size(1), 3, 'Sizes does not match')
--  mytester:assertTensorEq(torch.DoubleTensor({{9,4.5}, {0,-4.5}, {-9,-1*math.huge}}), mergedClusters1, precision, ' - Pruning should be applied to gradInput')
  
  epsilon = 10
  local mergedClusters2 = activationModifierUtil.clusterize(initialClusters, epsilon)
  mytester:asserteq(mergedClusters2:size(1), 1, 'Sizes does not match')
--  mytester:assertTensorEq(torch.DoubleTensor({{4.25, -2.375}, {-9,-1*math.huge}}), mergedClusters2, precision, ' - Pruning should be applied to gradInput')
  
  epsilon = 0.5
  local mergedClusters3 = activationModifierUtil.clusterize(initialClusters, epsilon)
  mytester:asserteq(mergedClusters3:size(1), initialClusters:size(1), 'Sizes does not match')
--  mytester:assertTensorEq(torch.DoubleTensor({{-2.4375,-1*math.huge}}), mergedClusters3, precision, ' - Pruning should be applied to gradInput')
end

--function nnpruntest.test_activationModifierUtil_mergeClusters()
--  local initialClusters = torch.Tensor({{10,9},{8,4.5},{1,0},{-1,-4.5}, {-8,-9}, {-10, -1*math.huge}})
--  local mergedClusters, iIndx, jIndx, continue = activationModifierUtil.mergeClusters(clusters, fromIndx, toIndx, epsilon, clusterizeFromEnd)
--
--  local clusters = torch.Tensor({{0.0825, 0.165}, {0.2825, 0.4}, {0.5525, 0.705}, {0.8525, 1}})
--  mytester:asserteq(0.0825, nn.ActivationModifier().getClusterCenter(0.0, clusters, 0.0), 'Wrong cluster center found')
--  mytester:asserteq(0.0825, nn.ActivationModifier().getClusterCenter(0.165, clusters, 0.0), 'Wrong cluster center found')
--  mytester:asserteq(0.2825, nn.ActivationModifier().getClusterCenter(0.166, clusters, 0.0), 'Wrong cluster center found')
--  mytester:asserteq(0.5525, nn.ActivationModifier().getClusterCenter(0.5, clusters, 0.0), 'Wrong cluster center found')
--  mytester:asserteq(0.8525, nn.ActivationModifier().getClusterCenter(1, clusters, 0.0), 'Wrong cluster center found')
--end

local function buildTreeNode(column, splitVal, leftNode, rightNode)
  local rootNode = DecisionTreeNode(column, splitVal)
  rootNode:setLeft(leftNode)
  rootNode:setRight(rightNode)
  return rootNode
end

function nnpruntest.test_RulesGenerator_treeStats()
  --tree:
  -- RootNode (0,0)
  --    left1 (1,1):
  --          left11:     class1
  --          right11:    class2
  --    right1(2,2): 
  --          left (3,3):
  --              left:   class1
  --              right:  class2
  --          right:      class1
  --
  -- Expected Stats: depth=4, leafsCount=5
  
  local rootNode = buildTreeNode( 0,0, 
                                  buildTreeNode(1,1,
                                    DecisionTreeLeaf(1), 
                                    DecisionTreeLeaf(2)),
                                  buildTreeNode(2,2,
                                    buildTreeNode(3,3,
                                      DecisionTreeLeaf(1), 
                                      DecisionTreeLeaf(2)),
                                    DecisionTreeLeaf(1))   )

  local stats = rootNode:treeStats()
  mytester:asserteq(4, stats.maxDepth, 'Wrong maxDepth found')
  mytester:asserteq(5, stats.leafsCount, 'Wrong leafsCount found')
  print("rootNode=>")
  rootNode:print()
end

--===================================================================

--function nnpruntest.test_n2ps_prunning()
--  data = csvUtil.generateNoisyXorData(4, -0.8, 0.8, 0.05)
--  X = data[{ {}, {1,-2} }]
--  labels = data[{ {}, {-1} }]
--  if (labels:min() <= 0 ) then
--    labels = labels:add(torch.abs(labels:min()) + 1)
--  end  
--  targetLabels, classNumber = dataUtil.labelsColumnToMatrix(labels, -1, 1) -- (-1 + 1) for tanh, (0, +1) for sigmoid
--  dataset = dataUtil.convertToDataset(X, targetLabels)
--  local labels2Targets = {}
--  for i = 1, labels:size(1) do
--    labels2Targets[labels[i][1]] = targetLabels[i]  
--  end
--  
--  classNames = {}
--  for i = 1, classNumber do
--    classNames[i] = "Class"..i  
--  end
--  
--  -- ANN parameters
--  inputs = dataset[1][1]:size(1) 
--  outputs = dataset[1][2]:size(1) 
----  HUs = 10 
--  HUs = 10
--  learnRate = 0.1
--  
--  -- ActivationRecorder's are placed behind ActivationModifiers, because firstly they are recording non-modified activations
--  -- and then after clustering is performed and clustering modifier functions are in place - they record clustered activations
--  mlp = nn.Sequential()
--  mlp:add( nn.NeuronPruningLayer('tanh') )         -- Input layer pruning
----  mlp:add( nn.ActivationModifier() )          -- Used to perform activations discretization/clusterization
--  mlp:add( nn.ActivationRecorder(false) )     -- Used to record activation values
--  mlp:add( nn.Linear(inputs, HUs) )           -- 2 input, 3 hidden units
--  mlp:add( nn.Tanh() )
--  mlp:add( nn.NeuronPruningLayer('tanh') )         -- Hidden layer pruning
----  mlp:add( nn.ActivationModifier() )          -- Used to perform activations discretization/clusterization
--  mlp:add( nn.ActivationRecorder(false) )     -- Used to record activation values
--  mlp:add( nn.Linear(HUs, outputs) )          -- 1 output
----  mlp:add( nn.ActivationModifier() )          -- Used to perform activations discretization/clusterization
--  mlp:add( nn.ActivationRecorder(false) )     -- Used to record activation values
--  
--  criterion = nn.MSECriterion()  
--  trainer = nn.StochasticGradient(mlp, criterion)
--  trainer.verbose = false
--  trainer.learningRate = learnRate
--  trainer.maxIteration = 120
--  trainer.momentum = 0.5
--  trainer:train(dataset)
--  
--  -- lets prune nodes
--  local config = {}
--  config.afterPruningRetrainEpochs = 50
--  config.maxFallbacks = 5
--  config.errorWorsenForFallback = 0.05
--  config.numberOfNeuronsToPrune = 1 -- Number of neurons to prune at single step  
--  config.maxNodesToPrune = 5
--  config.maxPruningIterations = 20--20
--  config.classNames = classNames
--  config.retrainIterations = 50 --10
--  
--  activationModifierUtil.recordActivations(mlp, dataset)
--  
----  local n2psPruning = nn.N2PSPruning()
----  local pruningLog = n2psPruning:runNeuronsPruning(config, dataset, mlp, criterion, trainer, labels2Targets)
--  -- N2PSPrunning is not working - it fails to prune any neurons.
----  local sensPruningLogic = nn.SensitivityPruningLogic()
----  local neuronPruning1 = nn.NeuronPruning(sensPruningLogic)
----  local sensPruningLog = neuronPruning1:runNeuronsPruning(config, dataset, mlp:clone(), criterion, trainer, labels2Targets)
--  
--  local n2PSSensitivityLayeredPruningLogic = nn.N2PSSensitivityLayeredPruningLogic()
--  local neuronPruning2 = nn.NeuronPruning(n2PSSensitivityLayeredPruningLogic)
--  --local n2psSensLayeredPruningLog = neuronPruning2:runNeuronsPruning(config, dataset, mlp:clone(), criterion, trainer, labels2Targets)
--  
--end

--===================================================================

--function nnpruntest.test_sensitivityPruning_discretizing_clusterizing_decisionTreeBuilding()
--  
----  data = csvUtil.loadTest2dData()
--  data = csvUtil.generateNoisyXorData(4, -0.8, 0.8, 0.05)
--  X = data[{ {}, {1,-2} }]
--  labels = data[{ {}, {-1} }]
--  if (labels:min() <= 0 ) then
--    labels = labels:add(torch.abs(labels:min()) + 1)
--  end  
--  targetLabels, classNumber = dataUtil.labelsColumnToMatrix(labels, -1, 1) -- (-1 + 1) for tanh, (0, +1) for sigmoid
--  dataset = dataUtil.convertToDataset(X, targetLabels)
--  local labels2Targets = {}
--  for i = 1, labels:size(1) do
--    labels2Targets[labels[i][1]] = targetLabels[i]  
--  end
--  
--  classNames = {}
--  for i = 1, classNumber do
--    classNames[i] = "Class"..i  
--  end
--  
--  -- ANN parameters
--  inputs = dataset[1][1]:size(1) 
--  outputs = dataset[1][2]:size(1) 
----  HUs = 10 
--  HUs = 3
--  learnRate = 0.01
--  
--  -- ActivationRecorder's are placed behind ActivationModifiers, because firstly they are recording non-modified activations
--  -- and then after clustering is performed and clustering modifier functions are in place - they record clustered activations
--  mlp = nn.Sequential()
--  mlp:add( nn.NeuronPruningLayer('tanh', inputs) )         -- Input layer pruning
--  mlp:add( nn.ActivationModifier() )          -- Used to perform activations discretization/clusterization
--  mlp:add( nn.ActivationRecorder(false) )     -- Used to record activation values
--  mlp:add( nn.Linear(inputs, HUs) )           -- 10 input, 20 hidden units
--  mlp:add( nn.Tanh() )
--  mlp:add( nn.NeuronPruningLayer('tanh', HUs) )         -- Hidden layer pruning
--  mlp:add( nn.ActivationModifier() )          -- Used to perform activations discretization/clusterization
--  mlp:add( nn.ActivationRecorder(false) )     -- Used to record activation values
--  mlp:add( nn.Linear(HUs, outputs) )          -- 1 output
--  mlp:add( nn.ActivationModifier() )          -- Used to perform activations discretization/clusterization
--  mlp:add( nn.ActivationRecorder(false) )     -- Used to record activation values
--  
--  criterion = nn.MSECriterion()  
--  trainer = nn.StochasticGradient(mlp, criterion)
--  trainer.verbose = false
--  trainer.learningRate = learnRate
--  trainer.maxIteration = 500
--  trainer:train(dataset)
--  
--  -- lets prune nodes
--  local config = {}
--  config.afterPruningRetrainEpochs = 10
--  config.maxFallbacks = 5
--  config.errorWorsenForFallback = 0.05
--  config.numberOfNeuronsToPrune = 1 -- Number of neurons to prune at single step  
--  config.maxNodesToPrune = 5
--  config.maxPruningIterations = 0 --20
--  config.classNames = classNames
--  config.retrainIterations = 5 --10
--  
--  local sensitivityPruning = nn.SensitivityPruning()
--  local pruningLog = sensitivityPruning:runNeuronsPruning(config, dataset, mlp, criterion, trainer, labels2Targets)
----  print(plPretty.write(pruningLog, ''))
--
--	activationModifierUtil.recordActivations(mlp, dataset)
--
--  local confusionCalcFunc = function() 
--    return runningUtil.calculateConfusion2(mlp, dataset, classNames, labels2Targets, nil) 
--  end
--  -- Discretize ( this is optional step, but if you have too many activation values (input vectors) this step can help in reducing this number to ~200 (for tanh) for D=2
--  -- Set discretizer-functions for all ActivationModifiers layers
--  local initialD = 2 -- let's round activations to d digits after coma. PLease note that 0 digits is Ok with us here.
--  local discretizer = nn.ActivationsDiscretizer()
--  local d = discretizer:discretize(mlp, initialD, confusionCalcFunc)
--  
--  -- For current time being discretization results can not be used in clusterization.
--  
--  local confusionBeforeClusterization = confusionCalcFunc()
--  discretizer:clusterize(mlp, confusionCalcFunc, 1.95)
--
--  local confusionClustered = confusionCalcFunc()
--  mytester:asserteq(confusionBeforeClusterization.totalValid, confusionClustered.totalValid, 'Average classification rates are not equal!')
--  
--  -- now perform rules extraction
--  -- What we need as well is to know how each found activation values cluster participates in classification process.
--  -- For this we will collect modified (clustered) activation values on given dataset. 
--  
--  local rulegen = nn.RulesGenerator()
--  rulegen:initialize(mlp, dataset)
--  local inputClustersWithBoundaries = mlp:get(2).clustersTable -- collect clusters with their boundaries for input layer (usually it is layer #2)
--  mytester:asserteq(tableUtil.getSize(inputClustersWithBoundaries), 2)
--  mytester:asserteq(inputClustersWithBoundaries[1]:nElement(), 4)
--  mytester:asserteq(inputClustersWithBoundaries[2]:nElement(), 4)
--  local rootNode = rulegen:generateTree(inputClustersWithBoundaries)
--  
--  local treeConf = runningUtil.calculateConfusion3(rulegen, rootNode, dataset, classNames, labels2Targets, nil)
--  print(treeConf)
--  print(treeConf.totalValid)
--  
--  local y = rulegen:runTree(rootNode, torch.Tensor({-0.8, -0.8}))
--  local actualLabel, _ = runningUtil.findClosestLabelTarget(y, labels2Targets)
--  local expectedLabel = 1
--  mytester:asserteq(actualLabel, expectedLabel)
--  
--  local y = rulegen:runTree(rootNode, torch.Tensor({0.8, 0.8}))
--  local actualLabel, _ = runningUtil.findClosestLabelTarget(y, labels2Targets)
--  local expectedLabel = 1
--  mytester:asserteq(actualLabel, expectedLabel)
--
--  local y = rulegen:runTree(rootNode, torch.Tensor({0.8, -0.8}))
--  local actualLabel, _ = runningUtil.findClosestLabelTarget(y, labels2Targets)
--  local expectedLabel = 2
--  mytester:asserteq(actualLabel, expectedLabel)
--  
--  local y = rulegen:runTree(rootNode, torch.Tensor({-0.8, 0.8}))
--  local actualLabel, _ = runningUtil.findClosestLabelTarget(y, labels2Targets)
--  local expectedLabel = 2
--  mytester:asserteq(actualLabel, expectedLabel)
--end

mytester:add(nnpruntest)
mytester:run()