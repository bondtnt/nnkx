-- DecisionTreeNodeData is a class which holds attribute index (columnNumber) 
-- and splitValue for that attribute

torch = require('torch')


local DecisionTreeNode = torch.class('DecisionTreeNode')

function DecisionTreeNode:__init(columnNumber, splitValue)
  self.columnNumber = columnNumber
  self.splitValue = splitValue
  self.leftChild = nil
  self.rightChild = nil
end

function DecisionTreeNode:setLeft(leftChild)
  self.leftChild = leftChild 
end

function DecisionTreeNode:setRight(rightChild)
  self.rightChild = rightChild 
end

function DecisionTreeNode:isLeaf()
  return false
end

function DecisionTreeNode:treeStats()
  local leftStats = self.leftChild:treeStats()
  local rightStats = self.rightChild:treeStats()
  local depth = math.max(leftStats.maxDepth, rightStats.maxDepth) + 1
  local leafs = leftStats.leafsCount + rightStats.leafsCount
  return DecisionTreeStats(depth, leafs)
end

function DecisionTreeNode:asString()
  return '(col='..self.columnNumber..', splitVal='..self.splitValue..', left='..self.leftChild:asString()..', right='..self.rightChild:asString()..')'
end

--function DecisionTreeNode:__tostring__()
--  return torch.type(self) ..
--    string.format(print())
--end

local function tree_print(self, prefix, isTail)
  function getTailStr(isTail, yesStr, noStr) 
    if (isTail) then
      return yesStr
    else
      return noStr
    end
  end
  
  if (self.rightChild ~= nil) then
    tree_print(self.rightChild, prefix .. getTailStr(isTail,"│    ", "     "), false)  
  end
  
  if (torch.type(self) == 'DecisionTreeNode') then
    local colNum = self.columnNumber or 'n/a'
    local splitVal = self.splitValue or 'n/a'
    print(prefix .. getTailStr(isTail,"└─── ", "┌─── ") .. '(splitFeature='..colNum..', splitValue='..splitVal..')')
  else -- we assume here is DecisionTreeLeaf
    print(prefix .. getTailStr(isTail,"└─── ", "┌─── ") .. self:asString())
  end  
  if (self.leftChild ~= nil) then
    tree_print(self.leftChild, prefix .. getTailStr(isTail, "     ", "│    "), true)    
  end
end

function DecisionTreeNode:print()
  tree_print(self, "", true)
end