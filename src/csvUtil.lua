csvUtils = {}

function csvUtils.loadTest2dData()
  local filePath = '/home/andrey/works/LuaProjects/nnkx/data/non_lin2d.csv'
  return csvUtils.loadCsvData(filePath, true, ',')
end

-- Function returns tensor as read from csv file.
-- @param filePath      - path to csv file
-- @param headerPresent - boolean flag denoting should we skip first line or not
-- @param splitChar     - for csv files usually should be ','
function csvUtils.loadCsvData(filePath, headerPresent, splitChar, cutLastRow)
  if (cutLastRow == nil) then
    cutLastRow = true
  end

  -- Split string
  function string:split(sep)
    local sep, fields = sep, {}
    local pattern = string.format("([^%s]+)", sep)
    self:gsub(pattern, function(substr) fields[#fields + 1] = substr end)
    return fields
  end
  
  -- Count number of rows and columns in file
  local i = 1
  for line in io.lines(filePath) do
    if i == 1 then
      COLS = #line:split(splitChar)
    end
    i = i + 1
  end

  local ROWS = i
  if (headerPresent) then
    ROWS = i - 1  -- Minus 1 because of header
  end
  
  -- Read data from CSV to tensor
  local csvFile = io.open(filePath, 'r')
  if (headerPresent) then
    local header = csvFile:read()
  end

  local data = torch.Tensor(ROWS, COLS)

  local i = 0
  for line in csvFile:lines('*l') do
    i = i + 1
    local l = line:split(splitChar)
    for key, val in ipairs(l) do
      data[i][key] = val
    end
    
  end

  csvFile:close()
  
  if (cutLastRow == true) then
    --last row holds zeros
    return data[{{1,-2},{}}]
  else
    return data
  end
end

-- function generates dataset which models xor problem with slight noise - so that in total we have X vectors per each of 4 XOR problem points 
function csvUtils.generateNoisyXorData(numOfVectorsPerPoint, min, max, stddev)
  
  local t = torch.Tensor(numOfVectorsPerPoint * 4, 3):fill(0)
  -- 0, 0 => 0 case
  for i=1,numOfVectorsPerPoint do 
    if (stddev == 0) then  
      t[i][1] = min
      t[i][2] = min
    else
      t[i][1] = torch.normal(min, stddev)
      t[i][2] = torch.normal(min, stddev)
    end
    t[i][3] = 0
  end
  -- 0, 1 => 1 case
  for i=1,numOfVectorsPerPoint do
    if (stddev == 0) then
      t[i+numOfVectorsPerPoint][1] = min
      t[i+numOfVectorsPerPoint][2] = max
    else 
      t[i+numOfVectorsPerPoint][1] = torch.normal(min, stddev)
      t[i+numOfVectorsPerPoint][2] = torch.normal(max, stddev)
    end  
    t[i+numOfVectorsPerPoint][3] = 1
  end
  -- 1, 0 => 1 case
  for i=1,numOfVectorsPerPoint do 
    if (stddev == 0) then
      t[i+numOfVectorsPerPoint*2][1] = max
      t[i+numOfVectorsPerPoint*2][2] = min
    else
      t[i+numOfVectorsPerPoint*2][1] = torch.normal(max, stddev)
      t[i+numOfVectorsPerPoint*2][2] = torch.normal(min, stddev)
    end
    t[i+numOfVectorsPerPoint*2][3] = 1
  end
  -- 1, 1 => 0 case
  for i=1,numOfVectorsPerPoint do
    if (stddev == 0) then 
      t[i+numOfVectorsPerPoint*3][1] = max
      t[i+numOfVectorsPerPoint*3][2] = max
    else
      t[i+numOfVectorsPerPoint*3][1] = torch.normal(max, stddev)
      t[i+numOfVectorsPerPoint*3][2] = torch.normal(max, stddev)
    end
    t[i+numOfVectorsPerPoint*3][3] = 0
  end

  return t
end

return csvUtils