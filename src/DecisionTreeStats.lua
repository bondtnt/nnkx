-- DecisionTreeNodeData is a class which holds attribute index (columnNumber) 
-- and splitValue for that attribute

torch = require('torch')


local DecisionTreeStats = torch.class('DecisionTreeStats')

function DecisionTreeStats:__init(maxDepth, leafsCount)
  self.maxDepth = maxDepth
  self.leafsCount = leafsCount
end

function DecisionTreeStats:__tostring__()
  return 'DecisionTreeStats(maxDepth=' .. self.maxDepth .. '; leafsCount=' .. self.leafsCount .. ')'
end
