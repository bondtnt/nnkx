pruningUtil = {}

local torch = require('torch')
local typeUtil = require('typeUtil')

-- function which will replace given row(tensor) with missclassification ratios based on histograms. Check tests.
-- Function is public only for tests!!!
--------------------------------------------
-- @param tensor - in our usage case is actually a matrix row 
function pruningUtil.calculateMislassErrors(tensor, i)
  
  local y, j = torch.topk(tensor, 2, 2, true)
  tensor:zero()
  local j1 = j[1][1]
  local j2 = j[1][2]
  if (y[1][1] >= y[1][2] and y[1][2] > 0) then
    local maxIndex = j1
--    local nextToMaxIndex = j2
    tensor[1][maxIndex] = y[1][2]
  elseif (y[1][1] < y[1][2] and y[1][1] > 0) then
    local maxIndex = j2
--    local nextToMaxIndex = j1
    tensor[1][maxIndex] = y[1][1]
  end
end

local function applyToSlices(tensor, dimension, func, ...)
    for i, slice in ipairs(tensor:split(1, dimension)) do
        func(slice, i, ...)
    end
    return tensor
end


-- function for returning key (index) of the element in the table.
--------------------------------------------
-- @param x       - number.    
-- @return log2(x)
function pruningUtil.histClassSeparability( X, y, binCount, min, max )
  local unique = tensorUtil.unique_counts(y)
  local indices = torch.linspace(1,y:size(1),y:size(1)):long()
  local e = 0
  local H = torch.Tensor()
  for u, _ in pairs(unique) do
    local selectedIndx = indices[y:eq(u)]
    local XClass = X:index(1, selectedIndx)
    local h = XClass:histc(binCount, min, max)
    local eClass = 0
    
    if (H:nElement() == 0) then
      H = h
    else
      H = torch.cat(H, h, 2)
    end
  end
  
  local Hbackup = H:clone()  
  -- horizontally process matrix
  -- process each row/bin 
  applyToSlices(H, 1, pruningUtil.calculateMislassErrors, H)
  
  return H
end

return pruningUtil