-- script for running rules extraction experiment on IRIS dataset

require('mobdebug').start()
plPretty = require('pl.pretty')

torch = require('torch')
optim = require('optim')

nn = require('nn')
csvUtil = require('csvUtil')
dataUtil = require('dataUtil')
tableUtil = require('tableUtil')
tensorUtil = require('tensorUtil')
activationModifierUtil = require('activationModifierUtil')
runningUtil = require('runningUtil')
typeUtil = require('typeUtil')
nngraph = require('nngraph')
mathUtil = require('mathUtil')

require 'NeuronPruning'
require 'ActivationRecorder'
require 'ActivationModifier'
require 'SensitivityPruningLogic'
require 'N2PSPruningLogic'
require 'N2PSSensitivityLayeredPruningLogic'
require 'RulesGenerator'
require 'ActivationsDiscretizer'

require 'graph'

torch.manualSeed(1)
torch.setnumthreads(4)


local timeExec = sys.clock()

--require('profiler')
--profiler = newProfiler()
--profiler:start()

local time = os.date("*t")
local timestamp = ("%02d%02d%02d_%02d-%02d-%02d"):format(time.year, time.month, time.day, time.hour, time.min, time.sec)
local dataSetName = 'iris' --used for log files naming
local filePath = '/home/andrey/works/LuaProjects/nnkx/data/iris/iris.csv'
local data = csvUtils.loadCsvData(filePath, false, '\t')
--local data = csvUtil.loadTest2dData()
local X = data[{ {}, {1,-2} }]
X = tensorUtil.scaleTensor(X, 0, 1) -- we will be using tanh function
--X = tensorUtil.scaleTensor(X, 0, 1) -- we will be using sigm function
local labels = data[{ {}, {-1} }]
--labels = tensorUtil.scaleTensor(labels, 1, 2) -- we know there are 2 classes - mark them as 1 and 2
local targets, classesNumber, labels2Targets = dataUtil.labelsColumnToMatrix(labels, -1, 1) -- (-1 + 1) for tanh, (0, +1) for sigmoid
local targets, classesNumber = dataUtil.labelsColumnToMatrix(labels, 0, 1) -- (-1 + 1) for tanh, (0, +1) for sigmoid
local labels2Targets = {}
for i = 1, labels:size(1) do
  labels2Targets[labels[i][1]] = targets[i]  
end

-- in case of CrossEntropyCriterion we should use labels
targets = labels
for k, v in pairs(labels2Targets) do
  labels2Targets[k] = k
end
local targets, classesNumber, labels2Targets = dataUtil.labelsColumnToMatrix(labels, 0, 1) -- (-1 + 1) for tanh, (0, +1) for sigmoid
targets = labels
--if (classesNumber == 2) then -- we will use single neuron in case of 2 classes
--  targets = targets[{ {}, {1} }]
--  for k, v in pairs(labels2Targets) do
--    labels2Targets[k] = v[1]
--  end
--end 

local dataset = dataUtil.convertToDataset(X, targets)

--for k, v in pairs(dataset) do -- here targets for CrossEntropyCriterion are 1D tensors holding 1 value. It should be not tensor but just number.  
--  local row = v
--  if ('function' ~= type(row)) then
--    local trgt = row[2]
--    row[2] = trgt[1] 
--  end
--end

local classNames = {}
for i = 1, classesNumber do
  classNames[i] = "Class"..i  
end

-- ANN parameters
local inputsSize = dataset[1][1]:size(1) 
local outputsSize = 3 --targets:size(2)
local hidden1Size = 5
local hidden2Size = 5 

local mlp = nn.Sequential()
mlp:add( nn.NeuronPruning('sigmoid') )         -- Input layer pruning
mlp:add( nn.ActivationModifier() )          -- Used to perform activations discretization/clusterization
mlp:add( nn.ActivationRecorder(false) )     -- Used to record activation values
mlp:add( nn.Linear(inputsSize, hidden1Size) ) -- 10 input, 20 hidden units
mlp:add( nn.Tanh() )

mlp:add( nn.NeuronPruning('sigmoid') )         -- Input layer pruning
mlp:add( nn.ActivationModifier() )          -- Used to perform activations discretization/clusterization
mlp:add( nn.ActivationRecorder(false) )     -- Used to record activation values
mlp:add( nn.Linear(hidden1Size, hidden2Size) ) -- 10 input, 20 hidden units
mlp:add( nn.Tanh() )

mlp:add( nn.NeuronPruning('sigmoid') )         -- Input layer pruning
mlp:add( nn.ActivationModifier() )          -- Used to perform activations discretization/clusterization
mlp:add( nn.ActivationRecorder(false) )     -- Used to record activation values
mlp:add( nn.Linear(hidden2Size, outputsSize) ) -- 1 output
mlp:add( nn.SoftMax() )
--mlp:add( nn.Tanh() )
mlp:add( nn.ActivationModifier() )          -- Used to perform activations discretization/clusterization
mlp:add( nn.ActivationRecorder(false) )     -- Used to record activation values

--local criterion = nn.MSECriterion()  
--local criterion = nn.CrossEntropyCriterion()
local criterion = nn.ClassNLLCriterion()
local trainer = nn.StochasticGradient(mlp, criterion)
trainer.learningRate = .025
trainer.learningRateDecay = 0.0025
trainer.maxIteration = 10  --500
trainer.verbose = true

local verbose = true 
local saveModels = false
local i = 1
local results = {}

-- log results to files
--  trainLogger = optim.Logger(paths.concat(opt.save, 'train.log'))
--  testLogger = optim.Logger(paths.concat(opt.save, 'test.log'))


local trainLogger = optim.Logger(paths.concat('logs', dataSetName..'_'..timestamp..'train.log'))
local testLogger = optim.Logger(paths.concat('logs', dataSetName..'_'..timestamp..'test.log'))
local prunningTrainLogger = optim.Logger(paths.concat('logs', dataSetName..'_'..timestamp..'prunedTrain.log'))
local prunningTestLogger = optim.Logger(paths.concat('logs', dataSetName..'_'..timestamp..'prunedTest.log'))
local prunedNeuronsCount = optim.Logger(paths.concat('logs', dataSetName..'_'..timestamp..'prunedNeuronsCount.log'))
local rulesTrainLogger = optim.Logger(paths.concat('logs', dataSetName..'_'..timestamp..'rulesTrain.log'))
local rulesTestLogger = optim.Logger(paths.concat('logs', dataSetName..'_'..timestamp..'rulesTest.log'))
local rulesTestDepthLogger = optim.Logger(paths.concat('logs', dataSetName..'_'..timestamp..'rulesTestDepth.log'))
local rulesTestLeafsLogger = optim.Logger(paths.concat('logs', dataSetName..'_'..timestamp..'rulesTestLeafs.log'))

local xValTrainer = {
  criterion = criterion, 
  classNames = classNames, 
  mlp = mlp, 
  trainer = trainer, 
  saveModels = false, -- if true all trained models will be saved 
  verbose = false     -- will print accuracy if true
}

local pruningConfig = {}
pruningConfig.afterPruningRetrainEpochs = 2
pruningConfig.maxFallbacks = 5
pruningConfig.errorWorsenForFallback = 0.05
pruningConfig.numberOfNeuronsToPrune = 1 -- Number of neurons to prune at single step  
pruningConfig.maxNodesToPrune = hidden1Size + inputsSize
pruningConfig.maxPruningIterations = inputsSize + hidden1Size + hidden2Size + pruningConfig.maxFallbacks * 3 --20
pruningConfig.classNames = classNames
pruningConfig.retrainIterations = 1 --10

local xvalIteration = 1
local xValLogs = {}
xValLogs.trainConfusion = {}
xValLogs.testConfusion = {}
xValLogs.pruningTrainConfusion = {}
xValLogs.pruiningTestConfusion = {}
xValLogs.rulesTrainConfusion = {}
xValLogs.rulesTestConfusion = {}
xValLogs.pruningLogs = {}

local function trainProcedure(model, x, y, criterion, learnRate)
  local prediction = model:forward(x)
  --print( "prediction: " .. tostring( prediction ) )
  --print( "class: " .. tostring( class ) )
  local err = criterion:forward(prediction, y)
  model:zeroGradParameters()
  local gradOutputs = criterion:backward(prediction, y)
  model:backward(x, gradOutputs)
  model:updateParameters(learnRate)
end

xValTrainer.trainTest =
  function(thiz, _mlp, _trainer, _classNames, _trainData, _testData, _labels2Targets)
    -- train network
    _trainer:train(_trainData)
    -- manual training instead of gradient trainer
--    local _learnRate = _trainer.learningRate
--    --local Xtrain, yTrain = dataUtil.tensorsFromDataset(_trainData)
--    for epochs = 1, _trainer.maxIteration do
--      for _, row in pairs(_trainData) do
--        if ('function' ~= type(row)) then
--          local inpt = row[1]
--          local trgt = row[2]
--          trainProcedure(_mlp, inpt, trgt, criterion, _learnRate)
--          _learnRate = _learnRate * (1 - trainer.learningRateDecay)
--        end
--      end 
--      print(epochs) 
--    end
    
    -- save stats
    local trainConf = runningUtil.calculateConfusion2(_mlp, _trainData, classNames, labels2Targets, nil)
    local testConf = runningUtil.calculateConfusion2(_mlp, _testData, classNames, labels2Targets, nil)
    trainConf:updateValids()
    testConf:updateValids()
    xValLogs.trainConfusion[xvalIteration] = trainConf
    xValLogs.testConfusion[xvalIteration] = testConf
    
    print('trainConf '.. trainConf:__tostring__())
    print('testConf '.. testConf:__tostring__())
    
    -- prune network
--    local sensPruningLogic = nn.SensitivityPruningLogic()
--    local neuronPruning1 = nn.NeuronPruning('Tanh', sensPruningLogic)
--    local sensPruningLog = neuronPruning1:runNeuronsPruning(config, dataset, mlp:clone(), criterion, trainer, labels2Targets)
--    local confusionTrainAfterPruning = runningUtil.calculateConfusion2(_mlp, _trainData, classNames, labels2Targets, nil)
    
    -- save stats    
    local pruningTrainConf = runningUtil.calculateConfusion2(_mlp, _trainData, classNames, labels2Targets, nil)
    local pruningTestConf = runningUtil.calculateConfusion2(_mlp, _testData, classNames, labels2Targets, nil)
    pruningTrainConf:updateValids()
    pruningTestConf:updateValids()
    print('pruningTrainConf '.. pruningTrainConf:__tostring__())
    print('pruningTestConf '.. pruningTestConf:__tostring__())
    xValLogs.pruningTrainConfusion[xvalIteration] = pruningTrainConf
    xValLogs.pruiningTestConfusion[xvalIteration] = pruningTestConf
    xValLogs.pruningLogs[xvalIteration] = pruningLog
    
    -- clusterize activations
    activationModifierUtil.recordActivations(_mlp, _trainData)
    
--    local confusionCalcFunc = function() 
--      return runningUtil.calculateConfusion2(_mlp, _trainData, classNames, labels2Targets, nil) 
--    end
--    
--    local discretizer = nn.ActivationsDiscretizer()
--    local confusionBeforeClusterization = confusionCalcFunc()
--    discretizer:clusterize(_mlp, confusionCalcFunc, 1.95)
--    local confusionClustered = confusionCalcFunc()
--    
--    -- extract rules
--    local rulegen = nn.RulesGenerator()
--    rulegen:initialize(_mlp, _trainData)
--    local inputClustersWithBoundaries = _mlp:get(2).clustersTable -- collect clusters with their boundaries for input layer (usually it is layer #2)
--    local rootNode = rulegen:generateTree(inputClustersWithBoundaries)
--    print('rootNode=>\n')
--    print(rootNode:asString())
--    local treeStats = rootNode:treeStats()
--    
--    -- save extracted rules stats
--    local rulesTrainConf = runningUtil.calculateConfusion3(rulegen, rootNode, _trainData, classNames, labels2Targets, nil)
--    local rulesTestConf = runningUtil.calculateConfusion3(rulegen, rootNode, _testData, classNames, labels2Targets, nil)
--    rulesTrainConf:updateValids()
--    rulesTestConf:updateValids()
--    xValLogs.rulesTrainConfusion[xvalIteration] = rulesTrainConf
--    xValLogs.rulesTestConfusion[xvalIteration] = rulesTestConf
--    
--    xvalIteration = xvalIteration + 1
--    
--    trainLogger:add{['% mean class accuracy (train set)'] = trainConf.totalValid * 100}
--    testLogger:add{['% mean class accuracy (test set)'] = testConf.totalValid * 100}
--    prunningTrainLogger:add{['% mean class accuracy (pruned train set)'] = pruningTrainConf.totalValid * 100}
--    prunningTestLogger:add{['% mean class accuracy (pruned test set)'] = pruningTestConf.totalValid * 100}
--    prunedNeuronsCount:add{['% pruning Log (pruned neurons counts)'] = pruningLog.prunedNeurons[#pruningLog.prunedNeurons].total}
--    rulesTrainLogger:add{['% mean class accuracy (rules train set)'] = rulesTrainConf.totalValid * 100}
--    rulesTestLogger:add{['% mean class accuracy (rules test set)'] = rulesTestConf.totalValid * 100}
--    rulesTestDepthLogger:add{['% rules maxDepth'] = treeStats.maxDepth}
--    rulesTestLeafsLogger:add{['% rules count'] = treeStats.maxDepth}
--    
--    if true then
--      trainLogger:style{['% mean class accuracy (train set)'] = '-'}
--      testLogger:style{['% mean class accuracy (test set)'] = '+'}
--      prunningTrainLogger:style{['% mean class accuracy (pruned train set)'] = '-'}
--      prunningTestLogger:style{['% mean class accuracy (pruned test set)'] = '+'}
--      prunedNeuronsCount:style{['% pruning Log (pruned neurons counts)'] = '-' }
--      rulesTrainLogger:style{['% mean class accuracy (rules train set)'] = '-'}
--      rulesTestLogger:style{['% mean class accuracy (rules test set)'] = '+'}
--      rulesTestDepthLogger:style{['% mean class accuracy (rules test set)'] = '-'}
--      rulesTestLeafsLogger:style{['% mean class accuracy (rules test set)'] = '-'}
--      
--      trainLogger:plot()
--      testLogger:plot()
--      prunningTrainLogger:plot()
--      prunningTestLogger:plot()
--      prunedNeuronsCount:plot()
--      rulesTrainLogger:plot()
--      rulesTestLogger:plot()
--      rulesTestDepthLogger:plot()
--      rulesTestLeafsLogger:plot()
--    end
  end 

--profiler.start('expiris-profile.out')

--runningUtil.runXVal(dataset, labels, xValTrainer, labels2Targets, 10, 3)
runningUtil.runXVal(dataset, labels, xValTrainer, labels2Targets, 5, 1)

local trainConfTensor = tensorUtil.buildTensorFromConfusionsTable(xValLogs.trainConfusion, function(confM) return confM.totalValid end)
local testConfTensor = tensorUtil.buildTensorFromConfusionsTable(xValLogs.testConfusion, function(confM) return confM.totalValid end)
local pruningTrainConfTensor = tensorUtil.buildTensorFromConfusionsTable(xValLogs.pruningTrainConfusion, function(confM) return confM.totalValid end)
local pruningTestConfTensor = tensorUtil.buildTensorFromConfusionsTable(xValLogs.pruiningTestConfusion, function(confM) return confM.totalValid end)

local function printTensor(t, str) 
  print(str..' mean='..t:mean()..'; std='..t:std()..';')
end

printTensor(trainConfTensor, 'train')
printTensor(testConfTensor, 'test')
printTensor(pruningTrainConfTensor, 'train after pruning')
printTensor(pruningTestConfTensor, 'test after pruning' )
--print(plPretty.write(results, ''))

timeExec = sys.clock() - timeExec
print("Time to execute = " .. (timeExec*1000) .. 'ms')


--profiler:stop()
--local outfile = io.open( "expiris-profile.txt", "w+" )
--profiler:report( outfile )
--outfile:close()
