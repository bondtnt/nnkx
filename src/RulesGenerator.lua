-- ProtoRules is a class which holds Clusters - they are centers of intervals that will be used for rules generation
torch = require('torch')
typeUtil = require('typeUtil')
plPretty = require('pl.pretty')
runningUtil = require('runningUtil')
require('ProtoRule')
require('DecisionTreeLeaf')
require('DecisionTreeNode')
require('DecisionTreeStats')

--local RulesGenerator = torch.class('RulesGenerator')
local RulesGenerator = torch.class('nn.RulesGenerator')


function RulesGenerator:__init()
  self.currentLayerToSave = 1
  self.modifiedActivations = {}
  self.modifiedActivationsCoveredMarkers = {}
  self.protoRulesForLayers = {}
  self.protoRuleNodesForLayers = {}
end

function RulesGenerator:addModifiedActivationsForLayer(modifiedActivationsForLayer)
  typeUtil.assert(modifiedActivationsForLayer, 'Tensor')
  self.modifiedActivations[self.currentLayerToSave] = modifiedActivationsForLayer:clone() -- this is what ActivationsRecorder have recorded after ActivationsModiefier clustered modification has taken place
  self.modifiedActivationsCoveredMarkers[self.currentLayerToSave] = torch.Tensor(modifiedActivationsForLayer:size()[1]):fill(0) -- this tensor will serve as flags
  self.currentLayerToSave = 1 + self.currentLayerToSave
end

function RulesGenerator:scaleTree(root, oldMin, oldMax, newMin, newMax)
--  typeUtil.assert(modifiedActivationsForLayer, 'Tensor')
  if (torch.type(root) == 'DecisionTreeNode') then
    local leftScaledChild = RulesGenerator:scaleTree(root.leftChild, oldMin, oldMax, newMin, newMax)
    local rightScaledChild = RulesGenerator:scaleTree(root.rightChild, oldMin, oldMax, newMin, newMax)
    local colNum = root.columnNumber
    local splitVal = root.splitValue
    local scaledSplitVal = mathUtil.scaleValue(splitVal, oldMin, oldMax, newMin, newMax) -- we will be using tanh function
    local node = DecisionTreeNode(colNum, scaledSplitVal)
    node:setLeft(leftScaledChild)
    node:setRight(rightScaledChild)
    return node
  elseif (torch.type(root) == 'DecisionTreeLeaf') then
    local rootClassTensor = root.classTensor:clone()
    return DecisionTreeLeaf(rootClassTensor)
  end
end

-- @param tableOfNodes is regular table containing graph.Node()'s with node.data == ProtoRule. Each ProtoRule have field 'protoRule' 
-- @param protoRule is tensor containing clusters - we will search for it
local function findNode(tableOfNodes, protoRuleTensorToFind)
  if (tableOfNodes ~= nil) then 
    for _, node in pairs(tableOfNodes) do
      local protoRuleFromNode = node.data
      if (tensorUtil.tensorsEqual(protoRuleFromNode.protoRule, protoRuleTensorToFind)) then
        return node
      end
    end
  end
  return nil
end

-- Function returns tensor (which denotes class) that holds most frequent class
-- as well it returns count of entries of that class and total entries count 
local function getMajorityClass(tClasses)
  local uniqueRowsTensor, frequencies = tensorUtil.rowsFrequencies(tClasses)
  local mostFrequentClassCount = 0
  local mostFrequentClass = nil
  local totalClassesCount = 0
  for k,v in pairs(frequencies) do
    if v > mostFrequentClassCount then
      mostFrequentClassCount = v
      mostFrequentClass = tClasses[k]
    end 
    totalClassesCount = totalClassesCount + v 
  end 
  
  return mostFrequentClass, mostFrequentClassCount, totalClassesCount
end

local function constructTree(t, tClasses, inputClustersWithBoundaries, currentDepth, maxDepth)
  local mostFrequentClass, mostFrequentClassCount, totalCount = getMajorityClass(tClasses)
  if (mostFrequentClassCount == totalCount) then
    -- we have single class - let's create node with it
    return DecisionTreeLeaf(mostFrequentClass)
  else
    if (currentDepth == maxDepth) then
      -- generate LeafNode with most frequent class
      return DecisionTreeLeaf(mostFrequentClass)
    else
      local colNum, splitValue, calculationsPerformed = mathUtil.findBestSplit(t, tClasses, inputClustersWithBoundaries)
      local node = DecisionTreeNode(colNum, splitValue)
      
      local leftT, leftTClasses, rightT, rightTClasses = mathUtil.splitTables(t, tClasses, colNum, splitValue)
      if (leftTClasses:size():size() == 0) then
        return DecisionTreeLeaf(mostFrequentClass)
      end
      if (rightTClasses:size():size() == 0) then
        return DecisionTreeLeaf(mostFrequentClass)
      end
      local leftNode = constructTree(leftT, leftTClasses, inputClustersWithBoundaries, currentDepth + 1, maxDepth)
      local rightNode = constructTree(rightT, rightTClasses, inputClustersWithBoundaries, currentDepth + 1, maxDepth)
      
      node:setLeft(leftNode)
      node:setRight(rightNode)
    
      return node
    end
  end  
end

function RulesGenerator:runTree(rootNode, value)
  if (rootNode.isLeaf()) then -- not leaf - dig deeper
    return rootNode.classTensor  
  else 
    if (value[rootNode.columnNumber] < rootNode.splitValue) then
      return RulesGenerator:runTree(rootNode.leftChild, value)
    else
      return RulesGenerator:runTree(rootNode.rightChild, value)
    end
  end
end

function RulesGenerator:generateTree(inputClustersWithBoundaries, maxDepth)
  print('Generating tree')

  self.allLayersNodes = {}
  
  local g = graph.Graph()
  self.graph = g
  
  local nextLayerNodes = {}
  local layer = self.currentLayerToSave - 1
  local lastLayerIndx = layer 
  -- for all layers starting from last one
  while (layer > 0) do
    print('Processing layer = '.. layer)
    local coveredLayerActivations = self.modifiedActivationsCoveredMarkers[layer]:clone() -- tensor holding flags for each activations row
    local layerActivations = self.modifiedActivations[layer]:clone()  -- clustered neuron activations for given layer
    if (layer == lastLayerIndx) then
      -- as we are dealing with ClassNLLCriterion - let's convert last layer outputs to class labels
      _, layerActivations = layerActivations:max(2)
    end
    
    local layerNodes = {}
    
    -- This cycle picks current layer ( starting from very last one ) and leaves only unique clustered activation values
    -- It does so in a smart way - each new row (from activations table) - clustered activation values tuple is considered as a proto rule 
    -- and then any equal(same) activations combination (in subsequent rows) are marked (in a separate table - 
    -- -modifiedActivationsCoveredMarkersForLayer) as covered (data point covered by 'rule')
    -- such covered proto rules (clustered activation values tuples) are discared from subsequent processing
    -- We are creating protoRuleNode for graph with activation values - protoRule and covered points count coveredPoints fields of ProtoRule class.
    -- If Current layer is not last one, then corresponding  (same row) activations values from next layer is used
    -- to find already existing graph node (in nextLayerNodes table) and use it to build edge in the graph 
    -- continue while we have not covered activations
    local i = 1
    while (coveredLayerActivations:size()[1] > coveredLayerActivations:sum()) do
      if (coveredLayerActivations[i] == 0) then
        
        local activations = layerActivations[i]:clone()
        -- mark all similar activations and collect their count
        coveredLayerActivations[i] = 1
        local equalActivationsCount = 1
        if (i < layerActivations:size()[1]) then
          for j = i+1,layerActivations:size()[1] do
            if ((coveredLayerActivations[j] == 0) and (tensorUtil.tensorsEqual(activations, layerActivations[j]))) then
              equalActivationsCount = equalActivationsCount + 1
              coveredLayerActivations[j] = 1
            end
          end 
        end
        
        -- create node
        local premise = ProtoRule(activations, equalActivationsCount)
        local premiseNode = graph.Node(premise)
        layerNodes[tableUtil.getSize(layerNodes) + 1] = premiseNode
        
        if (tableUtil.getSize(nextLayerNodes) > 0) then
          local nextLayerActivations = self.modifiedActivations[layer+1][i]:clone()
          if (layer+1 == lastLayerIndx) then
            -- as we are dealing with ClassNLLCriterion - let's convert last layer outputs to class labels
            _, nextLayerActivations = nextLayerActivations:max(1)
          end
          local consequenceNode = findNode(nextLayerNodes, nextLayerActivations)
          g:add(graph.Edge(premiseNode, consequenceNode))
        end
      end  
      i = i + 1
    end
    self.allLayersNodes[layer] = layerNodes
    nextLayerNodes = layerNodes
    layer = layer - 1
  end
  
--  print('Done with clusterization network building');

  -- for now built graph is not used, what we reallly want is clusterd modifiedActivations themselves
  
  local t = self.modifiedActivations[1] -- first layer activations clustered values
  local tClasses = self.modifiedActivations[#self.modifiedActivations] -- corresponding last layer clusterd activation values - classes
  -- as we are using ClassNLLCriterion - we have several output neurons - the one with maximum value (all of them are < 0) is predicted class
  _, tClasses = tClasses:max(2) -- get indexes of max outputs (per each row - input vector)
  local rootNode = constructTree(t, tClasses, inputClustersWithBoundaries, 1, maxDepth)
--  print('Done with tree building');
  return rootNode
--  local y = RulesGenerator:runTree(rootNode, torch.Tensor({-0.8, -0.8}))
end

function RulesGenerator:initialize(mlp, dataset) 
  -- firstly let's save non modified activation values and reset recorder layers
  for i = 1,mlp:size() do
    if (torch.type(mlp:get(i)) == 'nn.ActivationRecorder') then
      local recorderLayer = mlp:get(i)
      recorderLayer:reset()
    end
  end

  -- BEGIN of activations collection for clusters
  -- enable activations collection
  runningUtil.applyToFilteredLayers(mlp, 'nn.ActivationRecorder', function(layer) layer.recordFlag = true end)
  -- record clustered activations
  for i = 1,#dataset do
    local example = dataset[i]
    local input = example[1]
    mlp:forward(input) -- now ActivationRecorder's will collect activations
  end
  -- disable activations collection
  runningUtil.applyToFilteredLayers(mlp, 'nn.ActivationRecorder', function(layer) layer.recordFlag = false end)
  -- END of activations collection
  
  for i = 1,mlp:size() do
    if (torch.type(mlp:get(i)) == 'nn.ActivationRecorder') then
      local recorderLayer = mlp:get(i)
      local acts = recorderLayer.activations
      self:addModifiedActivationsForLayer(acts)
    end
  end
end
