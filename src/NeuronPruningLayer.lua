
local NeuronPruningLayer, parent = torch.class('nn.NeuronPruningLayer', 'nn.Module')
local tableUtil = require('tableUtil')

-- Returns tre if input 
--------------------------------------------
-- @param pruningMask     - pruningMask. it is a Tensor, should contain only 0's and 1's. Here 1 means pruned neuron    
function tensor_is_binary(input)

  t = input:clone():apply(function(x)
    if (x == 1) then
      return 0
    else 
      return x
    end
  end)
  
  return t:sum() == 0
end

-- Class implementing NeuronPruningLayer module. 
-- It is basically a layer which nullifies (or set's to other mean value)
-- outputs from previous layer.
--------------------------------------------
-- @param zeroVal         - Value to which we should nullify pruned neurons (1's in prunning mask)
--                          It is 0 in case Tanh is used (as Tanh output belongs to [-1, +1]
--                          and it should be assigned to 0.5 in case     
-- @param layerSize       - Number. Length/size of pruned layer 
--                          (we support only 1D layers - hence it is single number)                          
function NeuronPruningLayer:__init(zeroVal, layerSize)
  parent.__init(self)
--  print('Initializing NeuronPruningLayer zVal = '.. zeroVal .. ', size='..layerSize)
  if (zeroVal == nil) then
    error('`zeroVal` parameter is nil')
  end 
  if (layerSize == nil) then
    error('`layerSize` parameter is nil')
  end  
  self.pruningMask = torch.Tensor(1, layerSize):fill(0):byte()
  if (type(zeroVal) == 'string') then
    zeroVal = zeroVal:lower()
    if ((zeroVal == 'hardtanh') or
        (zeroVal == 'hardshrink') or
        (zeroVal == 'softshrink') or
        (zeroVal == 'softmax') or
        (zeroVal == 'softmin') or
        (zeroVal == 'softplus') or
        (zeroVal == 'logsigmoid') or  -- ???
        (zeroVal == 'logsoftmax') or  -- ???
        (zeroVal == 'tanh') or
        (zeroVal == 'hardtanh') or
        (zeroVal == 'hardtanh')) then
        
        zeroVal = 0
        
    elseif ((zeroVal == 'softsign') or
        (zeroVal == 'sigmoid') or
        (zeroVal == 'relu') or      -- ???
        (zeroVal == 'prelu') or     -- ???
        (zeroVal == 'rrelu') or     
        (zeroVal == 'elu')) then    -- ???
        
        zeroVal = 0.5 
    else
      error('Unknown type of activation function is passed='..zeroVal)
    end
     
  end
  self:setZeroVal(zeroVal)
  return self
end

-- Setter for pruning mask
--------------------------------------------
-- @param pruningMask     - pruningMask. it is a Tensor, should contain only 0's and 1's. Here 1 means pruned neuron    
function NeuronPruningLayer:setPruningMask(pruningMask)
  assert(pruningMask ~= nil, '<NeuronPruningLayer> illegal prunningMask, must be not nil')
  assert(tensor_is_binary(pruningMask), '<NeuronPruningLayer> illegal prunningMask, must contain only 0s and/or 1s')
  assert(pruningMask:type() == torch.ByteTensor():type(), 'pruningMask param should be of type torch.ByteTensor')
  assert(pruningMask:size(1) == 1, 'We expecting mask to be single row (vector)')
  self.pruningMask = pruningMask:clone():byte()
end

-- Getter for pruning mask
--------------------------------------------
-- @return clone of pruningMask    
function NeuronPruningLayer:getPruningMask()
  return self.pruningMask:clone() -- return copy
end

-- Setter for zero val (output value for pruned neurons)
--------------------------------------------
-- @param zeroVal     - It is a number which will be setted as output for pruned neurons.    
function NeuronPruningLayer:setZeroVal(zeroVal)
  self.zeroVal = zeroVal
end

-- Getter for zero value
--------------------------------------------
-- @return zero value    
function NeuronPruningLayer:getZeroVal()
  return self.zeroVal
end

-- Getter for zero value
--------------------------------------------
-- @return 
local function assertSameSizes(ta, tb)
  if (ta:dim() ~= tb:dim()) then
    error('Tensor1 dim='..ta:dim()..' does not equal Tensor2 dim='..tb:dim())
  else
    local taSize = ta:size()
    local tbSize = tb:size()
    for i = 1, #taSize  do
      if (taSize[i] ~= tbSize[i]) then
        error('ta ='..tostring(ta)..' does not equal tb ='..tostring(tb))
      end
    end
  end
  return
end

function NeuronPruningLayer:updateOutput(input)
  self.output:resizeAs(input):copy(input)
  if (input:size():size() == 1) then  
    self.output:maskedFill(self.pruningMask, self.zeroVal)
  else -- we are dealing with batches
    local pruningMaskWidth = self.pruningMask:size(2)
    assert(input:size(2) == pruningMaskWidth)
    local inputBatchSize = input:size(1)
    local pruningMaskBatch = self.pruningMask:expand(inputBatchSize, pruningMaskWidth)
    assertSameSizes(input, pruningMaskBatch)
    self.output:maskedFill(pruningMaskBatch, self.zeroVal)    
  end  
  return self.output
end

function NeuronPruningLayer:updateGradInput(input, gradOutput)
  self.gradInput:resizeAs(gradOutput):copy(gradOutput) -- 0 - no error for pruned nodes
  if (input:size():size() == 1) then  
   self.gradInput:maskedFill(self.pruningMask, 0)
  else -- we are dealing with batches
    local pruningMaskWidth = self.pruningMask:size(2)
    assert(gradOutput:size(2) == pruningMaskWidth)
    local inputBatchSize = gradOutput:size(1)
    local pruningMaskBatch = self.pruningMask:expand(inputBatchSize, pruningMaskWidth)
    assertSameSizes(gradOutput, pruningMaskBatch)
    self.gradInput:maskedFill(pruningMaskBatch, 0)
  end  
  
  return self.gradInput
end

function NeuronPruningLayer:__tostring__()
  return torch.type(self) ..
    string.format('(zeroval=%s, maskSize=(%d,%d))', self.zeroVal, self.pruningMask:size(1), self.pruningMask:size(2))
end